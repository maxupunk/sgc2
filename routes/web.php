<?php
use Illuminate\Support\Facades\Route;

Route::get('{any?}',  function () {
    return view('admin');
})->where('any', '^(?!api|sanctum).*$')->middleware('throttle:60,1');
