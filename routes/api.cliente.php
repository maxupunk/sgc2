<?php

use Illuminate\Support\Facades\Route;

Route::post('cliente/login', 'Cliente\AutenticacaoController@login')->middleware('throttle:60,1')->name('cliente.login');

Route::group(['middleware' => ['auth:sanctum', 'throttle:120,1'], 'namespace' => 'Cliente', 'prefix' => 'cliente'], function () {
    Route::post('logout', 'AutenticacaoController@logout')->name('cliente.logout');

    Route::resource('ordens', 'OrdensController', ['only' => ['index','show']]);
    Route::resource('financeiros', 'FinanceirosController', ['only' => ['index','show']]);
});
