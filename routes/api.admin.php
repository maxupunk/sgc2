<?php

use Illuminate\Support\Facades\Route;

Route::post('admin/login', 'Admin\AutenticacaoController@login')->middleware('throttle:60,1')->name('login');

Route::group(['middleware' => ['auth:admin', 'throttle:120,1'], 'namespace' => 'Admin', 'prefix' => 'admin'], function () {

    // Notificações
    Route::get('notificacoes', 'NotificacoesController@index')->name('notificacoes.index');
    Route::put('notificacoes', 'NotificacoesController@marcaTodasComoLidas')->name('notificacoes.marcaTodasComoLidas');
    Route::put('notificacoes/{id}', 'NotificacoesController@marcaComoLida')->name('notificacoes.marcaComoLida');
    Route::delete('notificacoes', 'NotificacoesController@deleteTodas')->name('notificacoes.deleteTodas');
    Route::delete('notificacoes/{id}', 'NotificacoesController@delete')->name('notificacoes.delete');
    //

    Route::post('logout', 'AutenticacaoController@logout')->name('logout');

    Route::get('estados/listing', 'EstadosController@listing')->name('estados.listing');
    Route::get('cidades/listing/{cidade}', 'CidadesController@listing')->name('cidades.listing');
    Route::get('bairros/listing/{bairro}', 'BairrosController@listing')->name('bairros.listing');
    Route::get('ruas/listing/{rua}', 'RuasController@listing')->name('ruas.listing');
    Route::get('marcas/listing', 'MarcasController@listing')->name('marcas.listing');
    Route::get('categorias/listing', 'CategoriasController@listing')->name('categorias.listing');
    Route::get('medidas/listing', 'MedidasController@listing')->name('medidas.listing');
    Route::get('formapgs/listing', 'FormapgsController@listing')->name('formapgs.listing');
    Route::get('parcelas/get/{formapg}', 'ParcelasController@getParcelas')->name('parcelas.get')->where('formapg', '[0-9]+');
    Route::get('perfis/listing', 'PerfisController@listing')->name('perfis.listing');
    Route::get('acoes/listing', 'AcoesController@listing')->name('acoes.listing');

    Route::get('produtofiscal', 'ProdutoFiscalController@index')->name('produtofiscal.index');
    //
    Route::get('usuarios/listing', 'UsuariosController@listing')->name('usuarios.lista');
    // tags
    Route::get('tags/search', 'TagsController@search')->name('tags.search');
    Route::resource('tags', 'TagsController', ['except' => ['create', 'edit']]);

    // deletar o proprio token
    Route::delete('usuarios/token/{tokenId}', 'UsuariosController@deletaToken')->name('usuarios.deleteToken');
    // deleta outros token
    Route::delete('usuarios/{userId}/token/{tokenId}', 'UsuariosController@deletaTokenOutros')->name('usuarios.deleteTokenOutros');
});

Route::group(['middleware' => ['auth:admin', 'acl:admin', 'throttle:120,1'], 'namespace' => 'Admin', 'prefix' => 'admin'], function () {

    Route::resource('cidades', 'CidadesController', ['except' => ['create', 'edit', 'index']]);
    //
    Route::resource('bairros', 'BairrosController', ['except' => ['create', 'edit']]);
    //
    Route::resource('ruas', 'RuasController', ['except' => ['create', 'edit']]);
    //
    Route::resource('marcas', 'MarcasController', ['except' => ['create', 'edit']]);
    //
    Route::resource('categorias', 'CategoriasController', ['except' => ['create', 'edit']]);
    //
    Route::resource('medidas', 'MedidasController', ['except' => ['create', 'edit']]);
    //
    Route::resource('formapgs', 'FormapgsController', ['except' => ['create', 'edit']]);
    //
    Route::resource('parcelas', 'ParcelasController', ['except' => ['create', 'edit']]);
    //
    Route::get('pessoas/search/{search}', 'PessoasController@search')->where('search', '.*')->name('pessoas.search');
    Route::get('pessoas/link/{id}', 'PessoasController@link')->where('id', '[0-9]+')->name('pessoas.link');
    Route::resource('pessoas', 'PessoasController', ['except' => ['create', 'edit']]);
    Route::delete('pessoas/{id}/token/{tokenId}', 'PessoasController@deletaToken')->name('pessoas.deleteToken');
    //
    Route::get('produtos/search/{search}', 'ProdutosController@search')->where('search', '.*')->name('produtos.search');
    Route::resource('produtos', 'ProdutosController', ['except' => ['create', 'edit']]);
    //
    Route::resource('vendas', 'VendasController', ['except' => ['create', 'edit', 'show', 'update']]);
    //
    Route::resource('servicos', 'ServicosController', ['except' => ['create', 'show', 'edit']]);
    Route::post('servicos/faturar', 'ServicosController@faturar')->name('servicos.faturar');
    Route::post('servicos/estender', 'ServicosController@estender')->name('servicos.estender');
    //
    Route::get('serials/listing/{id}', 'SerialsController@listing')->where('id', '[0-9]+')->name('serials.listing');
    Route::resource('serials', 'SerialsController', ['except' => ['create', 'edit']]);
    //
    Route::resource('compras', 'ComprasController', ['except' => ['create', 'show', 'edit']]);
    Route::post('compras/faturar', 'ComprasController@faturar')->name('compras.faturar');
    //
    // Relatorio precisa esta em primeiro la lista
    Route::get('financeiros/relatorio', 'FinanceirosController@relatorio')->name('financeiros.relatorio');
    Route::resource('financeiros', 'FinanceirosController', ['except' => ['create', 'edit']]);
    Route::post('financeiros/baixa/pessoa', 'FinanceirosController@baixar_por_pessoa')->name('financeiros.pessoa.baixar');
    Route::post('financeiros/baixa', 'FinanceirosController@baixar')->name('financeiros.baixar');

    //
    Route::resource('estoques', 'EstoqueController', ['except' => ['create', 'edit', 'store','destroy']]);
    //
    Route::resource('avarias', 'AvariasController', ['except' => ['create', 'edit']]);
    //
    Route::resource('perfis', 'PerfisController', ['except' => ['create', 'edit']]);
    //
    Route::put('usuarios/perfil/{id}', 'UsuariosController@perfil')->where('id', '[0-9]+')->name('usuarios.perfil');
    Route::resource('usuarios', 'UsuariosController', ['except' => ['create', 'edit']]);
    //
    // Relatorio precisa esta em primeiro la lista
    Route::get('ordens/relatorio', 'OrdensController@relatorio')->name('ordens.relatorio');
    Route::get('ordens/reabrir/{id}', 'OrdensController@reabrir')->where('id', '[0-9]+')->name('ordens.reabrir');
    Route::get('ordens/{id}', 'OrdensController@show')->where('id', '[0-9]+')->name('ordens.show');
    //
    Route::resource('dashboard', 'DashboardController', ['only' => ['index']]);
    //
    Route::resource('backups', 'BackupsController', ['except' => ['create', 'edit']]);
    //
    Route::resource('prestacaocontas', 'PrestacaoContaController', ['except' => ['create', 'edit', 'update']]);
});
