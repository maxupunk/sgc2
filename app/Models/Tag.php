<?php

namespace App\Models;

class Tag extends BaseModel
{
    protected $table = 'tags';

    protected $fillable = ['tag', 'categoria'];

    public $timestamps = false;

    // em contrução
    /* public function setTagAttribute($hashtag)
    {
        // Casa tags como #dia #feliz #chateado
        // Não casa caracteres especias #so-pt
        $pattern = '/#(\w+)/';

        // Alternativa para incluir outros caracteres
        // Basta incluir entre os colchetes
        //$pattern = '/#([\w-]+)/';

        preg_match($pattern, $hashtag, $tag);

        Log::info('HASHTAG:' . $tag[0]);
        $this->attributes['tag'] = $tag[0];
    } */
}
