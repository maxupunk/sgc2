<?php

namespace App\Models;

class Marca extends BaseModel
{
    protected $table = 'marcas';

    protected $fillable = ['nome', 'descricao', 'image'];

    public static function listMarcas()
    {
        return static::orderBy('nome')
            ->select('id', 'nome')
            ->get();
    }
}
