<?php

namespace App\Models;

class Formapg extends BaseModel
{
    protected $table = 'formapgs';

    protected $fillable = ['descricao', 'custo', 'ativo'];

    public $timestamps = false;

    public static function listFormapgs()
    {
        return static::orderBy('descricao')
            ->select('id', 'descricao')
            ->get();
    }
}
