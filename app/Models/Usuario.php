<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;
use Laravel\Sanctum\HasApiTokens;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\Casts\Attribute;

class Usuario extends Authenticatable implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use Notifiable, HasApiTokens, HasFactory;

    protected $table = 'usuarios';

    protected $fillable = ['pessoa_id', 'email', 'nome', 'senha', 'perfil_id', 'empresa_id', 'imagem', 'config', 'ativo'];

    protected $hidden = ['senha', 'remember_token'];
    // não extende de BaseModel então precisa dos casts
    protected $casts = [
        'config' => 'json',
        'created_at' => 'datetime:d/m/Y - H:i:s',
        'updated_at' => 'datetime:d/m/Y - H:i:s',
    ];

    protected $auditExclude = [
        'imagens'
    ];

    public function perfil()
    {
        return $this->belongsTo('App\Models\Perfil', 'perfil_id');
    }

    public function Senha(): Attribute
    {
        return new Attribute(fn ($value) => Hash::needsRehash($value) ? Hash::make($value) : $value);
    }
}
