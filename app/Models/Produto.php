<?php

namespace App\Models;

class Produto extends BaseModel
{
    protected $table = 'produtos';

    protected $fillable = [
        'descricao', 'caracteristica', 'medida_id', 'marca_id', 'peso',
        'tipo', 'origem_id', 'tipo_id', 'ncm_id', 'cest_id', 'cfop_id', 'icms_id',
        'icms', 'ipi_id', 'ipi', 'pis_id', 'pis', 'cofins_id', 'cofins'
    ];

    public static function listProdutos()
    {
        return static::orderBy('descricao')->pluck('descricao', 'id');
    }

}
