<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProdutoNcm extends Model
{
    protected $table = 'produto_ncm';
    public $timestamps = false;
}
