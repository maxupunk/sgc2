<?php

namespace App\Models;


class AcoesPerfil extends BaseModel
{
    protected $table = 'acoes_perfis';

    protected $fillable = ['acao_id', 'perfil_id'];

    public static function GetID($id)
    {
        $ListaPerfil = static::select('acoes_perfis.acao_id')
            ->join('perfis', 'perfis.id', '=', 'acoes_perfis.perfil_id')
            ->join('acoes', 'acoes.id', '=', 'acoes_perfis.acao_id')
            ->where('acoes_perfis.perfil_id', '=', $id)
            ->pluck('acao_id');

        return $ListaPerfil;
    }

    // retona um array com toda a lista de metodos 
    public static function GetPermissoesByUserID($id)
    {
        return static::select('acoes.metodo')
            ->join('perfis', 'perfis.id', '=', 'acoes_perfis.perfil_id')
            ->join('acoes', 'acoes.id', '=', 'acoes_perfis.acao_id')
            ->join('usuarios', 'usuarios.perfil_id', '=', 'perfis.id')
            ->where('usuarios.id', '=', $id)->distinct()->pluck('metodo')->toArray();
    }

    public static function SaveLista($perfil_id, $LstAcoes)
    {
        $perfil = Perfil::find($perfil_id);
        $perfil->acoes()->sync($LstAcoes);
    }
}
