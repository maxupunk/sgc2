<?php

namespace App\Models;

class ContatoTag extends BaseModel
{
    protected $table = 'contato_tag';

    protected $fillable = ['tag_id', 'contato_id'];

}