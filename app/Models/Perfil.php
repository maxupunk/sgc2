<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Perfil extends BaseModel
{
    use HasFactory;

    protected $table = 'perfis';

    protected $fillable = ['descricao'];

    //protected $appends = ['acao_list'];

    public function acoes()
    {
        return $this->belongsToMany('App\Models\Acao', 'acoes_perfis', 'perfil_id', 'acao_id')->withTimestamps();
    }

    public static function listPerfis()
    {
        return static::orderBy('descricao')->select('descricao', 'id')->get();
    }

    public function Entregue(): Attribute
    {
        return new Attribute(
            get: fn ($date) => array_map('strval', $this->acoes->pluck('id')->toArray())
        );
    }

    protected static function boot()
    {
        parent::boot();

        static::deleting(function ($perfil) {
            $perfil->acoes()->detach();
        });
    }
}
