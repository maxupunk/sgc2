<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProdutoPisCofins extends Model
{
    protected $table = 'produto_pis_cofins';
    public $timestamps = false;
}
