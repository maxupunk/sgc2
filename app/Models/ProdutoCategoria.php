<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProdutoCategoria extends Model
{
    protected $table = 'produto_categoria';

    protected $fillable = ['produto_id', 'categoria_id'];

    public static function GetID($id)
    {
        return static::select('categorias.id', 'categorias.nome')
            ->join('produtos', 'produtos.id', '=', 'produto_categoria.produto_id')
            ->join('categorias', 'categorias.id', '=', 'produto_categoria.categoria_id')
            ->where('produto_id', $id)
            ->get();
    }

    public static function gravar($categorias, $produto_id)
    {
        static::where('produto_id', $produto_id)->delete();
        foreach ($categorias as $categoria) {
            if (!isset($categoria['id'])) {
                $categoria = Categoria::create(['nome' => $categoria]);
            }
            static::insert(['produto_id' => $produto_id, 'categoria_id' => $categoria['id']]);
        }
    }
}
