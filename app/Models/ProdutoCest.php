<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProdutoCest extends Model
{
    protected $table = 'produto_cest';
    public $timestamps = false;
}
