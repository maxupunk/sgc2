<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProdutoOrigem extends Model
{
    protected $table = 'produto_origens';
    public $timestamps = false;
}
