<?php

namespace App\Models;

class Parcela extends BaseModel
{
    protected $table = 'parcelas';

    protected $fillable = ['nparcela', 'juros', 'formapg_id'];

    public $timestamps = false;

    //public static function listParcelas($formapg_id)
    //{
    //    return static::orderBy('nparcela')->where('formapg_id', '=', $formapg_id)->pluck('nparcela', 'id');
    //}

    public static function listParcelas($formapg_id)
    {
        $parcelas = static::join('formapgs', 'formapgs.id', '=', 'parcelas.formapg_id')
            ->select('parcelas.id', 'parcelas.nparcela', 'parcelas.juros',
                'formapgs.custo')
            ->where('parcelas.formapg_id', '=', $formapg_id)->get();

        return $parcelas;
    }
}
