<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Casts\Attribute;

class PrestacaoConta extends BaseModel
{
    protected $table = 'prestacao_contas';

    protected $fillable = ['usuario_id', 'orden_id', 'financeiro_id', 'pagamento_id', 'comissao'];

    public function CreatedAt(): Attribute
    {
        return new Attribute(
            set: fn () => Carbon::now(),
        );
    }

    protected static function GetItens($usuarioId)
    {
        return Listaiten::join('usuarios', 'usuarios.id', '=', 'lista_itens.usuario_id')
            ->leftJoin('financeiros', 'financeiros.orden_id', '=', 'lista_itens.orden_id')
            ->whereNotIn('lista_itens.id', function ($query) {
                $query->from('prestacao_contas')
                    ->select(['lista_iten_id'])
                    ->where('lista_iten_id', '<>', 'lista_itens.id')
                    ->get();
            })
            //->where('financeiros.estatus', 'PG')
            ->where('lista_itens.usuario_id', $usuarioId);
    }

    public static function ListComissao($usuarioId)
    {
        $ListaItens = static::GetItens($usuarioId)
            ->leftJoin('estoques', 'estoques.id', '=', 'lista_itens.estoque_id')
            ->leftJoin('produtos', 'produtos.id', '=', 'estoques.produto_id')
            ->leftJoin('medidas', 'medidas.id', '=', 'produtos.medida_id')
            ->leftJoin('marcas', 'marcas.id', '=', 'produtos.marca_id')
            ->leftJoin('ordens', 'ordens.id', '=', 'lista_itens.orden_id')
            ->leftJoin('pessoas', 'pessoas.id', '=', 'ordens.pessoa_id')
            // não conta com EA = EM ABERTA, PD = PENDENTE, OR = ORÇAMENTO, AA = AGUARDANDO APROVACAO, CA = CANCELADA
            ->whereNotIn('ordens.status', ['EA', 'PD', 'OR', 'AA', 'CA'])
            // não conta o CN = cancelado, DV = devolução, DL = deletado	
            ->whereNotIn('financeiros.estatus', ['CN', 'DV', 'DL'])
            ->select(
                'pessoas.nome AS cliente',
                'produtos.descricao',
                'produtos.tipo',
                'estoques.variante',
                'estoques.custo',
                'lista_itens.orden_id',
                'lista_itens.valor',
                'lista_itens.quant',
                'lista_itens.comissao',
                'lista_itens.observacao',
                'marcas.nome AS marca',
                'medidas.sigla AS medida',
                'financeiros.estatus AS financeiro_estatus',
                'ordens.status AS ordens_estatus'
            )
            ->get();

        if (!count($ListaItens)) {
            return false;
        }

        $itensPagos = collect();
        $itensPendentes = collect();
        $total = 0;
        $custo = 0;
        $comissaoPago = 0;
        $comissaoPendente = 0;

        foreach ($ListaItens as $item) {
            if ($item->financeiro_estatus === "PG") {
                $itensPagos->push($item);
                $comissaoPago += $item->quant * (($item->valor / 100) * $item->comissao);
                $total += $item->quant * $item->valor;
                $custo += $item->quant * $item->custo;
            } else {
                $itensPendentes->push($item);
                $comissaoPendente += $item->quant * (($item->valor / 100) * $item->comissao);
            }
        }

        return [
            'total' => $total,
            'custo' => $custo,
            'ValorComissaoPago' => $comissaoPago,
            'ValorComissaoAberto' => $comissaoPendente,
            'ItensComissaoPago' => $itensPagos,
            'ItensComissaoAberto' => $itensPendentes
        ];
    }

    public static function criaPestacao($usuarioId, $pgstatus)
    {
        DB::beginTransaction();
        try {
            $ListaItens = static::GetItens($usuarioId)
                ->leftJoin('ordens', 'ordens.id', '=', 'lista_itens.orden_id')
                ->leftJoin('pessoas', 'pessoas.id', '=', 'ordens.pessoa_id')
                ->where('financeiros.estatus', 'PG')
                ->select(
                    'pessoas.nome AS cliente',
                    'usuarios.nome as usuario',
                    'usuarios.pessoa_id AS usuario_pessoa_id',
                    'lista_itens.usuario_id',
                    'financeiros.id AS financeiro_id',
                    'lista_itens.id AS lista_iten_id',
                    'lista_itens.quant',
                    'lista_itens.valor',
                    'lista_itens.comissao',
                )->get();

            if (!count($ListaItens)) {
                return null;
            }

            $comissaoTotal = $ListaItens->sum(function ($row) {
                return $row->quant * (($row->valor / 100) * $row->comissao);
            });

            $dadosFinanceiro['pessoa_id'] = $ListaItens[0]['usuario_pessoa_id'];
            $dadosFinanceiro['nature'] = 'D';
            $dadosFinanceiro['valor'] = $comissaoTotal;
            $dadosFinanceiro['pago'] = $pgstatus ? $comissaoTotal : 0;
            $dadosFinanceiro['descricao'] = "Prestação de conta (" . $ListaItens[0]['usuario'] . ")";

            $financeiro = Financeiro::create($dadosFinanceiro);

            $dadosPrestacao = [];

            foreach ($ListaItens as $item) {
                $comissao = $item->quant * (($item->valor / 100) * $item->comissao);

                $dadosPrestacao[] = [
                    'usuario_id' => $item->usuario_id,
                    'lista_iten_id' => $item->lista_iten_id,
                    'financeiro_id' => $item->financeiro_id,
                    'pagamento_id' => $financeiro->id,
                    'comissao' => $comissao
                ];
            }

            static::insert($dadosPrestacao);
            DB::commit();
            return true;
        } catch (QueryException $e) {
            DB::rollBack();
            return $e;
        }
    }
}
