<?php

namespace App\Models;

class Serial extends BaseModel
{
    protected $table = 'produto_serials';

    protected $fillable = ['produto_id', 'serial', 'ativo'];

    public static function listSerials($produto_id)
    {
        return static::orderBy('serial')
            ->where('produto_id', $produto_id)
            ->where('ativo', 1)
            ->select('produto_id', 'serial')
            ->get();
    }
}
