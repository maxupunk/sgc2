<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class Estado extends BaseModel
{
    use HasFactory;
    
    protected $table = 'estados';

    protected $fillable = ['nome', 'uf'];

    public $timestamps = false;

    public static function listEstados()
    {
        return static::orderBy('nome')
            ->select('id', 'nome')
            ->get();
    }

    public function setUfAttribute($uf)
    {
        $this->attributes['uf'] = ucwords($uf);
    }
}
