<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Helpers\Helpers;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\Casts\Attribute;

class Orden extends BaseModel implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'ordens';

    const tipo = ['venda' => 'v', 'compra' => 'c', 'servico' => 's', 'devolucao' => 'd', 'troca' => 't'];

    protected $fillable = [
        'pessoa_id', 'orden_id', 'usuario_id', 'tipo', 'descricao', 'defeito', 'acessorios', 'laudo', 'documento',
        'acrescimo', 'desconto', 'frete', 'imagens', 'anotacoes', 'transportadora_id', 'codigo_rastreio', 'parcela_id', 'total', 'status', 'entregue'
    ];

    protected $auditExclude = [
        'imagens',
        'anotacoes'
    ];

    public function transformAudit(array $data): array
    {

        $AnotacoesOriginal = $this->getOriginal('anotacoes');
        $AnotacoesNova = $this->getAttribute('anotacoes');

        $AnotacaoAdd = array_filter($AnotacoesNova, function ($element) use ($AnotacoesOriginal) {
            return !in_array($element, $AnotacoesOriginal);
        });

        $AnotacaoRm = array_filter($AnotacoesOriginal, function ($element) use ($AnotacoesNova) {
            return !in_array($element, $AnotacoesNova);
        });

        if (count($AnotacaoAdd)) {
            $data['new_values']['Adicionou anotação(s)'] = $AnotacaoAdd;
        }

        if (count($AnotacaoRm)) {
            $data['new_values']['Removeu anotação(s)'] = $AnotacaoRm;
        }

        return $data;
    }

    public function filho()
    {
        return $this->hasMany(Orden::class, 'orden_id');
    }

    public function pai()
    {
        return $this->belongsTo(Orden::class, 'orden_id');
    }

    public function pessoa()
    {
        return $this->belongsTo(Pessoa::class);
    }

    public function usuario()
    {
        return $this->belongsTo(Usuario::class);
    }

    public function Acrescimo(): Attribute
    {
        return new Attribute(
            set: fn ($valor) => Helpers::DataToSQL($valor),
            get: fn ($valor) => $valor !== null ? floatval($valor): 0
        );
    }

    public function Desconto(): Attribute
    {
        return new Attribute(
            set: fn ($desc) => Helpers::DataToSQL($desc),
            get: fn ($valor) => $valor !== null ? floatval($valor): 0
        );
    }

    public function Frete(): Attribute
    {
        return new Attribute(
            set: fn ($frete) => Helpers::DataToSQL($frete),
            get: fn ($valor) => $valor !== null ? floatval($valor): 0
        );
    }

    public function Entregue(): Attribute
    {
        return new Attribute(
            get: fn ($date) => $date !== null ? Carbon::parse($date)->format('d/m/Y') : null
        );
    }

    public function Imagens(): Attribute
    {
        return new Attribute(
            get: fn ($json) => $json == null ? array() : json_decode($json)
        );
    }

    public function Anotacoes(): Attribute
    {
        return new Attribute(
            get: fn ($json) => $json == null ? array() : json_decode($json)
        );
    }

    public static function Total($ordenID)
    {
        $totalOrden = static::selectRaw('sum((acrescimo + frete) - desconto) AS total')
            ->where('id', '=', $ordenID)->get()[0]['total'];

        $itensTotal = Listaiten::Total($ordenID);

        //atualiza o valor total da orden
        static::find($ordenID)->update(['total' => $totalOrden + $itensTotal]);

        return floatval($totalOrden + $itensTotal);
    }

    public static function getByID($id)
    {
        return static::select(
            'ordens.*',
            'pessoas.nome',
            'pessoas.id AS pessoa_id',
            'formapgs.id AS formapg_id',
            'parcelas.juros',
            'parcelas.nparcela',
            'financeiros.datapg'
        )
            ->leftJoin('pessoas', 'pessoas.id', '=', 'ordens.pessoa_id')
            ->leftJoin('parcelas', 'parcelas.id', '=', 'ordens.parcela_id')
            ->leftJoin('formapgs', 'formapgs.id', '=', 'parcelas.formapg_id')
            ->leftJoin('financeiros', 'financeiros.orden_id', '=', 'ordens.id')
            ->find($id);
    }

    public static function getLstByStatus($status, $tipo = 's')
    {
        return static::select(
            'ordens.id',
            'ordens.descricao',
            'ordens.defeito',
            'ordens.tipo',
            'ordens.total',
            'ordens.created_at',
            'pessoas.nome',
            'pessoas.id AS pessoa_id',
        )
            ->leftJoin('pessoas', 'pessoas.id', '=', 'ordens.pessoa_id')
            ->leftJoin('parcelas', 'parcelas.id', '=', 'ordens.parcela_id')
            ->leftJoin('formapgs', 'formapgs.id', '=', 'parcelas.formapg_id')
            ->leftJoin('financeiros', 'financeiros.orden_id', '=', 'ordens.id')
            ->where('ordens.status', $status)
            ->where('ordens.tipo', $tipo);
    }

    // retorna true se for tudo bem
    public static function reabrir($orden_id)
    {
        $ordenStatus = static::select('status')
            ->where('ordens.id', $orden_id)
            ->where('ordens.status', 'RE')->first();

        //verifica de a ordem foi recebebida/entregue
        if ($ordenStatus) {

            $ListaItensPrestacao = static::select(
                'ordens.id AS orden_id',
                'ordens.status',
                'usuarios.nome AS usuario',
                'estoques.variante',
                'produtos.descricao',
                'lista_itens.comissao',
                'financeiros.valor',
                'prestacao_contas.id AS pestacao_conta_id',
            )
                ->leftJoin('lista_itens', 'lista_itens.orden_id', '=', 'ordens.id')
                ->leftJoin('usuarios', 'usuarios.id', '=', 'lista_itens.usuario_id')
                ->leftJoin('estoques', 'estoques.id', '=', 'lista_itens.estoque_id')
                ->leftJoin('produtos', 'produtos.id', '=', 'estoques.produto_id')
                ->leftJoin('prestacao_contas', 'prestacao_contas.lista_iten_id', '=', 'lista_itens.id')
                ->leftJoin('financeiros', 'financeiros.id', '=', 'prestacao_contas.pagamento_id')
                ->where('ordens.id', $orden_id)
                ->whereNotNull('prestacao_contas.id')->get();

            DB::beginTransaction();

            if (count($ListaItensPrestacao)) {
                return ['prestacao' => $ListaItensPrestacao];
            }

            $rollbackLst = Listaiten::RollbackByOrdenID($orden_id);
            if ($rollbackLst === true) {
                $ordemRetorno = static::where('id', '=', $orden_id)
                    ->update([
                        'status' => 'CC',
                        'entregue' => null,
                        'total' => 0
                    ]);

                if ($ordemRetorno) {
                    Financeiro::where('orden_id', '=', $orden_id)->delete();
                    DB::commit();
                    return true;
                } else {
                    DB::rollBack();
                    return false;
                }
            } else {
                DB::rollBack();
                return false;
            }
        } else {
            return true;
        }
    }
}
