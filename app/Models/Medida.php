<?php

namespace App\Models;

class Medida extends BaseModel
{
    protected $table = 'medidas';

    protected $fillable = ['nome', 'sigla'];

    public $timestamps = false;

    public static function listMedidas()
    {
        return static::orderBy('nome')
            ->select('id', 'nome')
            ->get();
    }
}
