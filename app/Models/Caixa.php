<?php

namespace App\Models;

class Caixa extends BaseModel
{
    protected $table = 'caixa';

    protected $fillable = ['usuario_id', 'abertura', 'fechamento', 'descricao', 'aberto'];

}
