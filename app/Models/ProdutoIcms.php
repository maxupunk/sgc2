<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProdutoIcms extends Model
{
    protected $table = 'produto_icms';
    public $timestamps = false;
}
