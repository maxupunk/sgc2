<?php

namespace App\Models;

use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\Casts\Attribute;

class Estoque extends BaseModel implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'estoques';

    protected $fillable = ['produto_id', 'empresa_id', 'codigo', 'variante', 'quant', 'custo', 'imagens', 'comissao', 'valor', 'local', 'alerta', 'tasklist', 'ativo'];

    protected $auditExclude = [
        'imagens',
        'tasklist'
    ];

    public function Imagens(): Attribute
    {
        return new Attribute(
            get: function ($json) {
                if ($json == null) {
                    return array();
                } else {
                    return json_decode($json);
                }
            },
        );
    }

    public static function Saida($estoque_id, $quant)
    {
        $produtoTipo = static::getProdutoTipoByID($estoque_id);

        $where = static::find($estoque_id);
        if ($produtoTipo === 'p') {
            $where->decrement('quant', $quant);
        }

        return $where->save();
    }

    public static function StoreEstoque($estoques, $produto_id)
    {
        $produto = Produto::find($produto_id);

        if ($produto->id) {
            foreach ($estoques as $estoque) {
                $EstoqueQuery = [
                    'produto_id' => $produto->id,
                    'empresa_id' => null,
                    'codigo' => isset($estoque['codigo']) ? $estoque['codigo'] : null,
                    'variante' => $estoque['variante'],
                    'quant' => isset($estoque['quant']) ? $estoque['quant'] : 0,
                    'custo' => isset($estoque['custo']) ? $estoque['custo'] : 0,
                    'valor' => $estoque['valor'],
                    'imagens' => isset($estoque['imagens']) ? json_encode($estoque['imagens']) : null,
                    'comissao' => isset($estoque['comissao']) ? $estoque['comissao'] : 0,
                    'encontra_se' => isset($estoque['encontra_se']) ? $estoque['encontra_se'] : null,
                    'tasklist' => isset($estoque['tasklist']) ? $estoque['tasklist'] : null,
                    'alerta' => isset($estoque['alerta']) ? @$estoque['alerta'] : null,
                    'ativo' => $estoque['ativo'],
                ];

                if (isset($estoque['id']) and isset($estoque['delete']) and $estoque['delete'] === true) {
                    static::find($estoque['id'])->delete();
                } elseif ($estoque['id']) {
                    static::find($estoque['id'])->update($EstoqueQuery);
                } else {
                    static::create($EstoqueQuery);
                }
            }
        } else {
            return false;
        }
    }

    public static function SaidaByBatch($lstItens)
    {
        foreach ($lstItens as $item) {
            $saide = self::Saida($item['id'], $item['quant']);
            if ($saide)
                return $saide;
        }
    }

    public static function RollBackSaida($estoque_id, $quant)
    {
        $produtoTipo = static::getProdutoTipoByID($estoque_id);

        $where = static::find($estoque_id);
        if ($produtoTipo === 'p') {
            $where->increment('quant', $quant);
        }

        return $where->save();
    }

    public static function Entrada($estoque_id, $quant)
    {
        $produtoTipo = static::getProdutoTipoByID($estoque_id);
        if ($produtoTipo === 'p') {
            return static::find($estoque_id)->increment('quant', $quant);
        } else {
            return true;
        }
    }

    public static function RollBackEntrada($estoque_id, $quant)
    {
        $produtoTipo = static::getProdutoTipoByID($estoque_id);
        if ($produtoTipo === 'p') {
            return static::find($estoque_id)->decrement('quant', $quant);;
        } else {
            return true;
        }
    }

    public static function getProdutoTipoByID($estoque_id)
    {
        $Produto = static::join('produtos', 'produtos.id', '=', 'estoques.produto_id')
            ->select('produtos.id', 'produtos.tipo')
            ->find($estoque_id);

        return $Produto['tipo'];
    }


    public static function getWithProdutoByID($estoque_id)
    {
        $Produto = static::join('produtos', 'produtos.id', '=', 'estoques.produto_id')
            ->select('produtos.id', 'produtos.tipo', 'estoques.quant', 'estoques.custo', 'estoques.valor')
            ->find($estoque_id);

        return $Produto;
    }

    public static function alerta()
    {
        $estoque = static::select('produtos.id AS produto_id', 'produtos.descricao', 'estoques.id', 'estoques.quant', 'estoques.variante', 'marcas.nome AS marca')
            ->leftjoin('produtos', 'produtos.id', '=', 'estoques.produto_id')
            ->LeftJoin('marcas', 'marcas.id', '=', 'produtos.marca_id')
            ->whereColumn('estoques.quant', '<=', 'estoques.alerta')
            ->where('estoques.ativo', '=', true)
            ->where('estoques.alerta', '>', 0)
            ->orderBy('estoques.updated_at', 'desc')
            ->get();

        return $estoque;
    }
}
