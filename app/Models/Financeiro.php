<?php

namespace App\Models;

use Carbon\Carbon;
use App\Helpers\Helpers;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\Casts\Attribute;

class Financeiro extends BaseModel implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'financeiros';

    protected $fillable = [
        'orden_id', 'pessoa_id', 'nature', 'descricao', 'observacao', 'valor', 'pago', 'vencimento', 'datapg', 'arquivos', 'fechado', 'estatus'
    ];

    protected $auditExclude = [
        'arquivos'
    ];

    public function Vencimento(): Attribute
    {
        return new Attribute(
            get: fn ($v) => $v ? Carbon::parse($v)->format('d/m/Y') : null,
            set: fn ($v) => Helpers::DataToSQL($v),
        );
    }

    public function Datapg(): Attribute
    {
        return new Attribute(
            set: fn ($datapg) => Helpers::DataToSQL($datapg),
        );
    }

    public function Pago(): Attribute
    {
        return new Attribute(
            set: function ($pago, $attributes) {
                if ($pago >= $attributes['valor'] and !isset($attributes['datapg'])) {
                    return [
                        'pago' => $pago,
                        'datapg' => Carbon::now()->format('Y-m-d'),
                        'estatus' => 'PG'
                    ];
                }

                if ($pago > 0 and $pago < $attributes['valor']) {
                    return [
                        'pago' => $pago,
                        'estatus' => 'PP'
                    ];
                }
                return $pago;
            },
        );
    }

    public function Valor(): Attribute
    {
        return new Attribute(
            set: fn ($valor) => Helpers::RealToSQL($valor),
        );
    }

    public function Arquivos(): Attribute
    {
        return new Attribute(
            set: fn ($json) => json_encode($json),
            get: fn ($json) => $json == null ? array() : json_decode($json)
        );
    }

    public static function AdicionaByOrdenID($DadosFinanceiro)
    {
        $FinanceiroQuery = Financeiro::where('financeiros.orden_id', '=', $DadosFinanceiro['orden_id'])->first();
        if ($FinanceiroQuery) {
            $FinanceiroQuery->update($DadosFinanceiro);
        } else {
            return Financeiro::create($DadosFinanceiro);
        }
    }

    public static function DaBaixa($request)
    {
        $financeiro = Financeiro::find($request->id);
        if ($financeiro) {
            $financeiro_rest = $financeiro['valor'] - $financeiro['pago'];
            if ($request->valorpago >= $financeiro_rest) {
                $financeiroBaixa['pago'] = $financeiro['valor'];
            } else {
                $financeiroBaixa['pago'] = $financeiro['pago'] + Helpers::RealToSQL($request->valorpago);
            }

            $financeiroBaixa['observacao'] = isset($request->observacao) ? $request->observacao : null;
            $financeiroBaixa['arquivos'] = isset($request->arquivos) ? $request->arquivos : null;

            return $financeiro->update($financeiroBaixa);
        } else {
            return false;
        }
    }

    public static function Hoje($nature = 'D')
    {
        $retorno = Financeiro::where('financeiros.nature', '=', $nature)
            ->where('financeiros.estatus', '=', 'AB')
            ->where('financeiros.vencimento', '=', date('Y-m-d'));

        return $retorno;
    }
}
