<?php

namespace App\Models;

class Categoria extends BaseModel
{
    protected $table = 'categorias';

    protected $fillable = ['nome', 'descricao', 'image'];

    public static function listCategorias()
    {
        return static::orderBy('nome')
            ->select('id', 'nome')
            ->get();
    }
}
