<?php

namespace App\Models;
use OwenIt\Auditing\Contracts\Auditable;

class Avaria extends BaseModel implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    
    protected $table = 'avarias';

    protected $fillable = ['usuario_id', 'estoque_id', 'quant', 'valor', 'motivo'];

}
