<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{

    protected $casts = [
        'created_at' => 'datetime:d/m/Y',
        'updated_at' => 'datetime:d/m/Y',
        'datapg' => 'datetime:d/m/Y',
        'pago' => 'float',
        'preco' => 'float',
        'valor' => 'float',
        'ativo' => 'boolean',
        'anotacoes' => 'json',
        'tasklist' => 'json',
    ];
}
