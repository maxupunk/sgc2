<?php

namespace App\Models;

class Contato extends BaseModel
{
    protected $table = 'contatos';

    protected $fillable = ['pessoa_id', 'contato'];

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public static function GetID($id)
    {
        $contatos = Contato::select('contatos.id', 'contatos.contato', 'contatos.created_at', 'contatos.updated_at')
            ->where('contatos.pessoa_id', '=', $id)
            ->get();

        $retorno = [];
        foreach ($contatos as $contato) {
            $tags = ContatoTag::select('tags.tag')
                ->join('tags', 'tags.id', '=', 'contato_tag.tag_id')
                ->where('contato_tag.contato_id', '=', $contato->id)
                ->pluck('tag');
            $retorno[] = [
                'id' => $contato->id,
                'contato' => $contato->contato,
                'created_at' => $contato->created_at,
                'tags' => $tags
            ];
        }

        return $retorno;
    }

    public static function gravar($contatos, $pessoa_id)
    {
        if (!is_array($contatos)) return null;

        $listContatoDB = static::where('pessoa_id', '=', $pessoa_id)->get();
        foreach ($listContatoDB as $ContatoDB) {
            // busca se o produto do DB ainda existe na lista enviada
            $lista = array_search($ContatoDB->id, array_column($contatos, 'id'));
            if ($lista === false) {
                static::where('id', '=', $ContatoDB->id)->delete();
            }
        }

        foreach ($contatos as $contato) {

            if (isset($contato['id'])) {
                $contatoQuery = static::find($contato['id']);
                $contatoQuery->update([
                    'pessoa_id' => $pessoa_id,
                    'contato' => $contato['contato']
                ]);
                //ContatoTag::where('contato_id', $contato['id'])->delete();
            } else {
                $contatoQuery = static::create([
                    'pessoa_id' => $pessoa_id,
                    'contato' => $contato['contato']
                ]);
            }
            $TagsListArray = [];
            foreach ($contato['tags'] as $tag) {
                $tagAtual = Tag::where('tag', '=', $tag)->first();
                if (!$tagAtual) {
                    $tagAtual = Tag::create([
                        'tag' => $tag,
                        'categoria' => 'contato'
                    ]);
                }

                $TagsListArray[] = $tagAtual['id'];
            }
            $contatoQuery->tags()->sync($TagsListArray);
        }
    }
}
