<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProdutoCfop extends Model
{
    protected $table = 'produto_cfop';
    public $timestamps = false;
}
