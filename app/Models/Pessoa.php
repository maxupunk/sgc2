<?php

namespace App\Models;

use App\Helpers\Helpers;
use Carbon\Carbon;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Facades\Hash;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Pessoa extends Authenticatable
{
    use Notifiable, HasApiTokens, HasFactory;

    protected $table = 'pessoas';

    protected $fillable = ['nome', 'fantasia', 'documento', 'mae', 'pai', 'dtnasc', 'tipo', 'rua_id', 'numero', 'cep', 'complemento', 'senha', 'imagem', 'config', 'ativo'];

    protected $hidden = ['senha'];

    protected $casts = [
        'config' => 'json',
        'created_at' => 'datetime:d/m/Y - H:i:s',
        'updated_at' => 'datetime:d/m/Y - H:i:s',
        'dtnasc' => 'date:d/m/Y',
    ];

    public function Senha(): Attribute
    {
        return new Attribute(
            set: fn ($value) => Hash::make($value)
        );
    }

    public function Dtnasc(): Attribute
    {
        return new Attribute(
            get: fn ($v) => $v ? Carbon::parse($v)->format('d/m/Y') : null,
            set: fn ($data) => Helpers::DataToSQL($data),
        );
    }

    public function Nome(): Attribute
    {
        return new Attribute(
            set: fn ($nome) => ucwords($nome)
        );
    }

    public function Mae(): Attribute
    {
        return new Attribute(
            set: fn ($nome) => ucwords($nome)
        );
    }

    public function Pai(): Attribute
    {
        return new Attribute(
            set: fn ($nome) => ucwords($nome)
        );
    }
}
