<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class Bairro extends BaseModel
{
    use HasFactory;

    protected $table = 'bairros';

    protected $fillable = ['nome', 'cidade_id'];

    //public $timestamps = false;

    public static function listBairros($cidade_id)
    {
        return static::orderBy('nome')
            ->where('cidade_id', '=', $cidade_id)
            ->select('id', 'nome')
            ->get();
    }

}
