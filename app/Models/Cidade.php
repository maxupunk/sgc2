<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class Cidade extends BaseModel
{
    use HasFactory;
    
    protected $table = 'cidades';

    protected $fillable = ['nome', 'estado_id'];

    public $timestamps = false;

    public static function listCidades($estado_id)
    {
        return static::orderBy('nome')
            ->where('estado_id', '=', $estado_id)
            ->select('id', 'nome')
            ->get();
    }

}
