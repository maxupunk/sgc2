<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProdutoIpi extends Model
{
    protected $table = 'produto_ipi';
    public $timestamps = false;
}
