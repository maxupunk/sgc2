<?php

namespace App\Models;

class Configuracao extends BaseModel
{
    //
    protected $table = 'configuracoes';

    protected $fillable = ['usuario_id', 'dados'];

    public $timestamps = false;

}
