<?php

namespace App\Models;

class Acao extends BaseModel
{
    protected $table = 'acoes';

    protected $fillable = ['nome', 'metodo', 'titulo'];

    public $timestamps = false;

    public static function listAcoes()
    {
        return static::orderBy('id')
            ->select('id AS acao_id', 'nome', 'titulo', 'metodo')
            ->get();
    }

    public static function listMetodo()
    {
        $query = static::select('metodo')->get();

        $str_metodos = null;
        foreach ($query as $acao) {
            $separador = empty($str_metodos) ? null : '|';
            $str_metodos .= $separador . $acao->metodo;
        }
        $metodo_array = explode('|', $str_metodos);

        return array_values(array_unique($metodo_array));
    }
}
