<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Casts\Attribute;
use App\Helpers\Helpers;

class Listaiten extends BaseModel
{
    protected $table = 'lista_itens';

    protected $fillable = ['orden_id', 'produto_id', 'usuario_id', 'observacao', 'quant', 'valor', 'desconto', 'acrescimo', 'tasklist', 'estatus'];

    public $timestamps = false;

    public function Valor(): Attribute
    {
        return new Attribute(
            set: fn ($valor) => Helpers::RealToSQL($valor),
        );
    }

    public function Quant(): Attribute
    {
        return new Attribute(
            get: fn ($q) => floatval($q),
        );
    }

    public static function GetByOrdenID($orden_id)
    {
        return static::leftJoin('estoques', 'estoques.id', '=', 'lista_itens.estoque_id')
            ->leftJoin('produtos', 'produtos.id', '=', 'estoques.produto_id')
            ->leftJoin('medidas', 'medidas.id', '=', 'produtos.medida_id')
            ->leftJoin('marcas', 'marcas.id', '=', 'produtos.marca_id')
            ->leftJoin('usuarios', 'usuarios.id', '=', 'lista_itens.usuario_id')
            ->select(
                'estoques.id',
                'estoques.codigo',
                'estoques.quant AS estoque',
                'estoques.encontra_se',
                // nescessario para calcular o lucro tabela no front
                'estoques.custo',
                //
                'produtos.descricao',
                'produtos.caracteristica',
                'produtos.tipo',
                'estoques.variante',
                'lista_itens.id AS lista_item_id',
                'lista_itens.observacao',
                'lista_itens.usuario_id',
                'lista_itens.valor',
                'lista_itens.quant',
                'lista_itens.comissao',
                'lista_itens.tasklist',
                'usuarios.nome AS usuario',
                'marcas.nome AS marca',
                'medidas.nome AS medida'
            )
            ->where('orden_id', '=', $orden_id);
    }

    public static function SaveLista($LstProdutos, $orden_id = null, $estatusAll = null)
    {
        $listProdutosDB = static::where('orden_id', '=', $orden_id)->get();
        foreach ($listProdutosDB as $ProdutoDB) {
            // busca se o produto do DB ainda existe na lista enviada
            $lista = array_search($ProdutoDB->id, array_column($LstProdutos, 'lista_item_id'));
            if ($lista === false) {
                static::where('id', '=', $ProdutoDB->id)->delete();
            }
        }

        foreach ($LstProdutos as $Produto) {

            $usuario_id = isset($Produto['usuario_id']) ? $Produto['usuario_id'] : null;
            $observacao = isset($Produto['observacao']) ? $Produto['observacao'] : null;
            $produto_id = isset($Produto['id']) ? $Produto['id'] : null;
            $estatus = isset($Produto['estatus']) ? $Produto['estatus'] : 1;
            $estatus = $estatusAll ? $estatusAll : $estatus;

            if ($Produto['quant'] >= 1) {
                $EstoqueQuery = [
                    'orden_id' => $orden_id,
                    'estoque_id' => $produto_id,
                    'usuario_id' => $usuario_id,
                    'observacao' => $observacao,
                    'quant' => $Produto['quant'],
                    'valor' => $Produto['valor'],
                    'comissao' => isset($Produto['comissao']) ? $Produto['comissao'] : 0,
                    'desconto' => isset($Produto['desconto']) ? $Produto['desconto'] : 0,
                    'tasklist' => isset($Produto['tasklist']) ? json_encode($Produto['tasklist']) : null,
                    'estatus' => $estatus
                ];
                if (isset($Produto['lista_item_id'])) {
                    static::where('id', '=', $Produto['lista_item_id'])->update($EstoqueQuery);
                } else {
                    static::insert($EstoqueQuery);
                }
            }
        }
        return true;
    }

    public static function SaveListaEntrada($LstProdutos, $orden_id = null, $estatusAll = null)
    {
        $ProdutosQuery = [];
        foreach ($LstProdutos as $produto) {
            $estatus = isset($produto['estatus']) ? $produto['estatus'] : 1;
            $ProdutosQuery[] = [
                'orden_id' => $orden_id,
                'estoque_id' => $produto['id'],
                'usuario_id' => isset($produto['usuario_id']) ? $produto['usuario_id'] : null,
                'quant' => $produto['quant'],
                'valor' => $produto['valor'],
                //'desconto' => isset($produto['desconto']) ? $produto['desconto'] : 0,
                'estatus' => $estatusAll ? $estatusAll : $estatus
            ];
        }

        static::where('orden_id', '=', $orden_id)->delete();
        return static::insert($ProdutosQuery);
    }

    // não retorna nada se tudo ocorer bem
    public static function RollbackByOrdenID($orden_id)
    {
        $LstProdutos = static::select('lista_itens.estatus', 'lista_itens.quant', 'lista_itens.estoque_id', 'ordens.tipo')
            ->leftJoin('ordens', 'ordens.id', '=', 'lista_itens.orden_id')
            ->where('lista_itens.orden_id', '=', $orden_id)
            ->get();

        DB::beginTransaction();

        $erro = "";
        foreach ($LstProdutos as $produto) {
            if ($produto['estatus'] === 2) {
                if ($produto['tipo'] == 'v' or $produto['tipo'] == 's') {
                    $where = Estoque::RollBackSaida($produto['estoque_id'], $produto['quant']);
                } elseif ($produto['tipo'] == 'c') {
                    $where = Estoque::RollBackEntrada($produto['estoque_id'], $produto['quant']);
                }
                if ($where) {
                    static::where('orden_id', '=', $produto['orden_id'])
                        ->where('estoque_id', '=', $produto['estoque_id'])
                        ->update(['estatus' => 1]);
                } else {
                    $erro = $where;
                    break;
                }
            }
        }

        if ($erro) {
            DB::rollBack();
            return $erro;
        } else {
            DB::commit();
            return true;
        }
    }

    public static function SaidaByOrdenID($orden_id)
    {
        $LstItens = static::where('orden_id', '=', $orden_id)->get();

        DB::beginTransaction();

        foreach ($LstItens as $item) {
            if ($item['estatus'] <= 1 and $item['estoque_id'] !== null) {
                $saida = Estoque::Saida($item['estoque_id'], $item['quant']);
                if ($saida) {
                    // confirma a saida na lista de item
                    static::where('estoque_id', '=', $item['estoque_id'])
                        ->where('orden_id', '=', $orden_id)
                        ->update(['estatus' => 2]);
                } else {
                    DB::rollBack();
                    return $saida;
                }
            }
        }

        DB::commit();
        return true;
    }

    public static function EntradaByOrdenID($orden_id)
    {
        $LstProdutos = static::where('orden_id', '=', $orden_id)->get();

        DB::beginTransaction();

        foreach ($LstProdutos as $produto) {
            $produtoTipo = Estoque::getProdutoTipoByID($produto['estoque_id']);

            if ($produto['estatus'] <= 1 and $produtoTipo == "p") {
                $entrada = Estoque::Entrada($produto['estoque_id'], $produto['quant']);
                if ($entrada) {
                    // confirma a saida na lista de item
                    static::where('estoque_id', '=', $produto['estoque_id'])
                        ->where('orden_id', '=', $orden_id)
                        ->update(['estatus' => 2]);
                } else {
                    DB::rollBack();
                    return $entrada;
                }
            }
        }

        DB::commit();
        return true;
    }

    public static function Total($orden_id)
    {
        $total = static::selectRaw('sum((quant * valor) - desconto) AS total')
            ->where('orden_id', '=', $orden_id)->get();
        return $total[0]['total'];
    }
}
