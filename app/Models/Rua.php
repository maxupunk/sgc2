<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class Rua extends BaseModel
{
    use HasFactory;

    protected $table = 'ruas';

    protected $fillable = ['nome', 'bairro_id'];

    public $timestamps = false;

    public static function listRuas($bairro_id)
    {
        return static::orderBy('nome')
            ->where('bairro_id', '=', $bairro_id)
            ->select('id', 'nome')
            ->get();
    }

    public function bairro()
    {
        return $this->belongsTo('App\Models\Bairro');
    }
}
