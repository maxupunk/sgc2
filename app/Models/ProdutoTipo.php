<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProdutoTipo extends Model
{
    protected $table = 'produto_tipos';
    public $timestamps = false;
}
