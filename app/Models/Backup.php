<?php

namespace App\Models;

use App\Helpers\Helpers;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Artisan;

class Backup
{
    private static $TableIgnora = ["migrations"];
    private static $FolderBackup = "backup";

    public static function criar($FileName = null)
    {
        $bkpFileName = ($FileName === null) ? date('YmdHis') : $FileName;

        $dumpDB = static::dump_db();
        $filecontent['migrate'] = self::getLastMigrationName();
        $filecontent['versao_estrutura'] = md5(json_encode($dumpDB['estrutura']));
        $filecontent['md5_daods'] = md5(json_encode($dumpDB['dados']));
        $filecontent['data'] = $dumpDB['dados'];

        $contentGzip = gzencode(json_encode($filecontent), 9);

        if (Storage::disk(self::$FolderBackup)->put("$bkpFileName", $contentGzip)) {
            return $bkpFileName;
        } else {
            return false;
        }
    }

    public static function truncate()
    {
        $tables = self::tables_db();

        try {
            DB::beginTransaction();
            DB::getSchemaBuilder()->disableForeignKeyConstraints();

            foreach ($tables as $table) {
                DB::table($table)->truncate();
            }

            DB::getSchemaBuilder()->enableForeignKeyConstraints();

            return true;
        } catch (QueryException $e) {
            return false;
        }
    }

    private static function DeleteAllTabs()
    {
        $tables = self::tables_db();

        try {
            DB::beginTransaction();
            DB::getSchemaBuilder()->disableForeignKeyConstraints();

            foreach ($tables as $table) {
                DB::statement("DROP TABLE IF EXISTS $table");
            }

            DB::getSchemaBuilder()->enableForeignKeyConstraints();

            return true;
        } catch (QueryException $e) {
            return false;
        }
    }

    public static function restoreMigrateTo($migrate)
    {
        if (self::DeleteAllTabs()) {
            Artisan::call('migrate');
        }
    }

    public static function restaure($id)
    {
        $existe = Storage::disk(self::$FolderBackup)->exists($id);
        if ($existe) {
            //static::criar("$id.PR");

            $mime_type = Storage::disk(self::$FolderBackup)->mimeType($id);
            $is_gzip = ($mime_type == "application/gzip");

            $file_conteudo = Storage::disk(self::$FolderBackup)->get($id);

            if ($is_gzip) {
                $file_unzip = gzdecode($file_conteudo);
                if ($file_unzip == false) {
                    return false;
                }
            } else {
                $file_unzip = $file_conteudo;
            }

            self::restoure_db($file_unzip);
            return true;
        } else {
            return false;
        }
    }

    public static function lista()
    {
        $files = Storage::disk(self::$FolderBackup)->Files();
        $lstBackups = [];
        foreach ($files as $file) {
            $data = Carbon::parse(Storage::disk(self::$FolderBackup)->lastModified($file))->format('d/m/Y - H:m:s');
            $tamanho = Helpers::BitToHumano(Storage::disk(self::$FolderBackup)->size($file));
            $lstBackups[] = [
                'id' => $file,
                'modificacao' => $data,
                'tamanho' => $tamanho
            ];
        }
        return array_reverse($lstBackups);
    }

    public static function delete($id)
    {
        return Storage::disk(self::$FolderBackup)->delete($id);
    }

    public static function download($id)
    {
        $existe = Storage::disk(self::$FolderBackup)->exists($id);
        if ($existe) {
            return Storage::disk(self::$FolderBackup)->get($id);
        } else {
            return false;
        }
    }

    private static function tables_db()
    {

        $tables = Schema::getAllTables();

        $tablesLst = array_map('current', $tables);

        return array_diff($tablesLst, self::$TableIgnora);
    }

    private static function dump_db()
    {
        try {
            $tablesList = self::tables_db();
            $content = [];
            $estrutura = [];
            foreach ($tablesList as $table) {
                $content[$table] = DB::select("SELECT * FROM `$table`");
                $estrutura[$table] = Schema::getColumnListing($table);
            }
            $response['estrutura'] = $estrutura;
            $response['dados'] = $content;
            return $response;
        } catch (\Exception $e) {
            return $e;
        }
    }

    private static function restoure_db($file_conteudo)
    {
        DB::beginTransaction();
        try {
            Schema::disableForeignKeyConstraints();

            $BackupJson = json_decode($file_conteudo, true);
            set_time_limit(6000);

            self::restoreMigrateTo($BackupJson['migrate']);
            //self::truncate();

            foreach ($BackupJson['data'] as $tabela => $registros) {
                foreach ($registros as $registro) {
                    DB::table($tabela)->insert($registro);
                }
            }
            Schema::enableForeignKeyConstraints();
            return DB::commit();
        } catch (QueryException $e) {
            Log::info($e);
            return $e;
        }
    }

    private static function getLastMigrationName()
    {
        $lastMigration = DB::table('migrations')->orderBy('id', 'desc')->first();

        return $lastMigration ? $lastMigration->migration : null;
    }
}
