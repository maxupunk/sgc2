<?php

namespace App\Http\Controllers\Cliente;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\FinanceiroRequest;
use App\Models\Financeiro;
use App\Helpers\Helpers;
use Illuminate\Support\Facades\Auth;

class FinanceirosController extends Controller
{

    public function index(Request $request)
    {
        $financeiros = Financeiro::select(
            'financeiros.id',
            'financeiros.orden_id',
            'financeiros.nature',
            'financeiros.descricao',
            'financeiros.valor',
            'financeiros.pago',
            'financeiros.vencimento',
            'financeiros.datapg',
            'financeiros.created_at',
            'financeiros.estatus'
        )->where('pessoa_id', auth::user()->id);

        $financeiros = $financeiros->orderBy('financeiros.id', 'desc');

        if ($request->has('search') and $request->input('search') != '') {
            $search = $request->input('search');
            $financeiros = $financeiros->where('descricao', 'LIKE', "%$search%");
        }

        if ($request->has('nature') and $request->input('nature') != '') {
            $financeiros = $financeiros->where('financeiros.nature',  $request->input('nature'));
        }

        if ($request->has('estatus') and $request->input('estatus') != '') {
            $financeiros = $financeiros->where('financeiros.estatus', $request->input('estatus'));
        }

        $ValorTotal = $financeiros->sum('financeiros.valor');
        $ValorPago = $financeiros->sum('financeiros.pago');

        $rowsperpage = $request->input('itemsPerPage') > 0 ? $request->input('itemsPerPage') : 30;
        $page = $request->input('page');

        $pagination = $financeiros->paginate($rowsperpage, ['*'], 'page', $page);

        return response()->json([
            'total' => $pagination->total(),
            'data' => $pagination->items(),
            'perPage' => $pagination->perPage(),
            'lastpage' => $pagination->lastPage(),
            'valorTotal' => isset($ValorTotal) ? $ValorTotal : null,
            'valorPago' => isset($ValorPago) ? $ValorPago : null,
        ]);
    }

    public function show($id)
    {
        $financeiro = Financeiro::select('financeiros.*', 'pessoas.nome')
            ->leftJoin('pessoas', 'pessoas.id', '=', 'financeiros.pessoa_id')
            ->where('financeiros.pessoa_id', auth::user()->id)
            ->find($id);

        if ($financeiro) {
            $financeiro['historico'] = $financeiro->audits()->with('user')->get();
            return response()->json($financeiro);
        } else {
            return response()->json([
                'type' => 'warning',
                'message' => Helpers::message('MSG000')
            ], 404);
        }
    }
}
