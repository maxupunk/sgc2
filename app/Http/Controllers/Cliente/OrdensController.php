<?php

namespace App\Http\Controllers\Cliente;

use App\Http\Controllers\Controller;
use App\Helpers\Helpers;
use App\Models\Financeiro;
use App\Models\Orden;
use App\Models\Listaiten;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrdensController extends Controller
{
    public function index(Request $request)
    {
        $ordens = Orden::select(
            'ordens.id',
            'ordens.orden_id',
            'ordens.descricao',
            'ordens.tipo',
            'ordens.status',
            'ordens.created_at',
            'financeiros.estatus AS financeiro_status'
        )
            ->leftJoin('pessoas', 'pessoas.id', '=', 'ordens.pessoa_id')
            ->leftJoin('financeiros', 'financeiros.orden_id', '=', 'ordens.id')
            ->where('ordens.pessoa_id', '=', auth::user()->id);

        if ($request->has('search') and $request->input('search') != '') {
            $search = $request->input('search');
            $ordens = $ordens->where('ordens.descricao', 'LIKE', "%$search%");
        }

        if ($request->has('status') and $request->input('status') != '') {
            $status = $request->input('status');
            $ordens = $ordens->where('status', $status);
        }

        $ordens = $ordens->orderBy('ordens.id', 'desc');

        $rowsperpage = $request->input('itemsPerPage') > 0 ? $request->input('itemsPerPage') : 30;
        $page = $request->input('page');

        $pagination = $ordens->Paginate($rowsperpage, ['*'], 'page', $page);

        return response()->json([
            'total' => $pagination->total(),
            'data' => $pagination->items(),
            'perPage' => $pagination->perPage(),
            'lastpage' => $pagination->lastPage()
        ]);
    }

    public function show($id)
    {

        $orden = Orden::where('id', $id)
            ->where('ordens.pessoa_id', auth::user()->id)
            ->first();

        if ($orden) {
            $orden['itens'] = Listaiten::GetByOrdenID($orden->id)->get();

            $orden['financeiro'] = Financeiro::where('orden_id', $orden->id)->get();

            $ordenPai = [];
            if ($orden['orden_id'] !== null) {
                $ordenPaiQuery = Orden::find($orden->id)->pai()->first();
                $ordenPaiQuery['itens'] = Listaiten::GetByOrdenID($ordenPaiQuery['id'])->get();
                $ordenPai[] = $ordenPaiQuery;
            }

            $OrdenPaiId = ($orden['orden_id'] !== null) ? $ordenPaiQuery['id'] : $orden->id;
            $ordenFilhos = Orden::where('orden_id', $OrdenPaiId)->where('id', '<>', $orden->id)->get();
            $ordenFilho = [];
            if (count($ordenFilhos)) {
                foreach ($ordenFilhos as $filho) {
                    $filho['itens'] = Listaiten::GetByOrdenID($filho['id'])->get();
                    $ordenFilho[] = $filho;
                }
            }

            $orden['estendido'] = array_merge($ordenPai, $ordenFilho);

            return response()->json($orden);
        }

        return response()->json([
            'type' => 'warning',
            'message' => Helpers::message('MSG000')
        ], 404);
    }
}
