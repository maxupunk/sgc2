<?php

namespace App\Http\Controllers\Cliente;

use App\Helpers\Helpers;
use App\Http\Controllers\Controller;
use App\Models\Pessoa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Cache;

class AutenticacaoController extends Controller
{
    public function login(Request $request)
    {
        if ($request->chave) {
            $pessoa_id = Cache::pull($request->chave);
            if ($pessoa_id) {
                $pessoa = Pessoa::select('id', 'nome', 'fantasia', 'imagem', 'config', 'senha', 'ativo')->where('id', $pessoa_id)->first();
            } else {
                return response()->json([
                    'success' => false,
                    'type' => 'warning',
                    'message' => Helpers::message('MSG061'),
                ], 401);
            }
        } else {
            $pessoa = Pessoa::select('id', 'nome', 'fantasia', 'imagem', 'config', 'senha', 'ativo')->where('documento', $request->usuario)->first();
            if (!$pessoa || !Hash::check($request->senha, $pessoa->senha) || !$pessoa->ativo) {
                return response()->json([
                    'success' => false,
                    'type' => 'warning',
                    'message' => Helpers::message('MSG055'),
                ], 401);
            }
        }

        if ($pessoa) {
            $response['usuario'] = $pessoa;
            $response['token'] = $pessoa->createToken($request->device_name)->plainTextToken;
            return $response;
        } else {
            return response()->json([
                'success' => false,
                'type' => 'warning',
                'message' => Helpers::message('MSG055'),
            ], 401);
        }
    }

    public function logout(Request $request)
    {
        $request->user()->currentAccessToken()->delete();
        return response()->json([
            'success' => true,
            'type' => 'success',
            'message' => Helpers::message('MSG007')
        ]);
    }

    public function link(Request $request)
    {
        return $request;
    }
}
