<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AcoesPerfil;
use App\Http\Requests\PerfilRequest;
use App\Models\Perfil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Helpers\Helpers;

class PerfisController extends Controller
{
    public function index(Request $request)
    {
        $perfis = Perfil::select('*');

        if ($request->has('sortBy') and $request->input('sortBy') != null) {
            $sortBy = $request->input('sortBy');
            $sortDesc = $request->input('sortDesc');
            $ArraySort = array_combine($sortBy, $sortDesc);
            foreach ($ArraySort as $By => $Desc) {
                $perfis = $perfis->orderBy($By, $Desc == 'false' ? 'desc' : 'asc');
            }
        } else {
            $perfis = $perfis->orderBy('id', 'desc');
        }

        if ($request->has('search') and $request->input('search') != '') {
            $search = $request->input('search');
            $perfis = $perfis->where('descricao', 'LIKE', "%$search%");
        }

        $rowsperpage = $request->input('itemsPerPage') > 0 ? $request->input('itemsPerPage') : 30;
        $page = $request->input('page');

        $pagination = $perfis->paginate($rowsperpage, ['*'], 'page', $page);

        return response()->json([
            'total' => $pagination->total(),
            'data' => $pagination->items(),
            'perPage' => $pagination->perPage(),
            'lastpage' => $pagination->lastPage()
        ]);;
    }

    public function store(PerfilRequest $request)
    {
        $input = $request->all();
        DB::beginTransaction();
        try {
            $perfil = Perfil::create($input);
            $perfil->acoes()->sync($request->input('acoes_perfis'));
            DB::commit();
            return response()->json([
                'success' => true,
                'type' => 'success',
                'message' => Helpers::message('MSG001'),
                'id' => $perfil['id']
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);
            return response()->json([
                'type' => 'warning',
                'message' => Helpers::message('MSG012'),
                'errors' => $e
            ], 500);
        }
    }

    public function show($id)
    {
        $perfil = Perfil::find($id);
        $perfil['acoes_perfis'] = AcoesPerfil::GetID($id);

        if ($perfil->count())
            return response()->json($perfil);

        return response()->json(['error' => Helpers::message('MSG000')], 404);
    }

    public function update(PerfilRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $perfil = Perfil::find($id);
            $perfil->update($request->all());
            $perfil->acoes()->sync($request->input('acoes_perfis'));
            DB::commit();
            return response()->json([
                'success' => true,
                'type' => 'success',
                'message' => Helpers::message('MSG002')
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);
            return response()->json([
                'success' => false,
                'type' => 'warning',
                'message' => Helpers::message('MSG012'),
                'errors' => $e
            ], 500);
        }
    }

    public function destroy($id)
    {
        try {
            Perfil::destroy($id);

            return response()->json([
                'success' => true,
                'type' => 'success',
                'message' => Helpers::message('MSG003')
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'type' => 'warning',
                'message' => Helpers::message('MSG005'),
                'errors' => $e
            ], 500);
        }
    }

    public function listing()
    {
        return response()->json(Perfil::listPerfis());
    }
}
