<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\SerialRequest;
use App\Models\Serial;
use App\Helpers\Helpers;

class SerialsController extends Controller
{

    public function index(Request $request)
    {
    }

    public function store(SerialRequest $request)
    {
        $input = $request->all();
        Serial::create($input);
        return response()->json([
            'success' => true,
            'type' => 'success',
            'message' => Helpers::message('MSG001')
        ]);
    }

    public function show($id)
    {
        $serial = $this->serial
            ->select('produto_serials.produto_id', 'produto_serials.serial')
            ->where('serial', $id)
            ->get();

        if ($serial->count())
            return response()->json($serial);

        return response()->json(['error' =>  Helpers::message('MSG000')], 404);
    }

    public function update(SerialRequest $request, $id)
    {
        $serial = Serial::find($id);
        $serial->update($request->all());
        return response()->json([
            'success' => true,
            'type' => 'success',
            'message' => Helpers::message('MSG002')
        ]);
    }

    public function destroy($id)
    {
        try {
            Serial::destroy($id);

            return response()->json([
                'success' => true,
                'type' => 'success',
                'message' => Helpers::message('MSG003')
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'type' => 'warning',
                'message' => Helpers::message('MSG005'),
                'errors' => $e
            ], 500);
        }
    }

    public function listing($produto_id)
    {
        return response()->json([
            'list' => Serial::listSerials($produto_id)
        ]);
    }
}
