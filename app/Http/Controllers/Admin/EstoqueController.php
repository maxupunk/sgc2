<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\EstoqueResquest;
use Illuminate\Http\Request;
use App\Models\Produto;
use App\Models\Estoque;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Helpers\Helpers;

class EstoqueController extends Controller
{

    public function index(Request $request)
    {
        $produtos = Produto::leftjoin('medidas', 'medidas.id', '=', 'produtos.medida_id')
            ->leftjoin('marcas', 'marcas.id', '=', 'produtos.marca_id')
            ->leftjoin('estoques', 'estoques.produto_id', '=', 'produtos.id')
            ->leftjoin('produto_categoria', 'produto_categoria.produto_id', '=', 'produtos.id')
            ->leftjoin('categorias', 'categorias.id', '=', 'produto_categoria.categoria_id')
            ->select(
                'produtos.descricao',
                'estoques.id',
                'estoques.variante',
                'estoques.codigo',
                'estoques.ativo',
                'estoques.quant',
                'custo',
                'valor',
                'produtos.id AS produto_id',
                'medidas.id AS medida_id',
                'medidas.nome AS medida',
                'marcas.id AS marca_id',
                'marcas.nome AS marca'
            )
            ->where('produtos.tipo', '=', 'p')
            ->distinct();

        if ($request->has('search') and $request->input('search') != '') {
            $search = $request->input('search');
            $produtos = $produtos->where('estoques.codigo', '=', $search);
            $produtos = $produtos->orWhere('produtos.descricao', 'LIKE', "%$search%");
            $produtos = $produtos->orWhere('categorias.nome', '=', $search);
        }

        if ($request->has('sortBy') and $request->input('sortBy') != null) {
            $sortBy = $request->input('sortBy');
            $sortDesc = $request->input('sortDesc');
            $ArraySort = array_combine($sortBy, $sortDesc);
            foreach ($ArraySort as $By => $Desc) {
                $produtos = $produtos->orderBy($By, $Desc == 'false' ? 'desc' : 'asc');
            }
        } else {
            $produtos = $produtos->orderBy('produtos.id', 'desc');
        }

        $rowsperpage = $request->input('itemsPerPage') > 0 ? $request->input('itemsPerPage') : 30;
        $page = $request->input('page');

        $pagination = $produtos->Paginate($rowsperpage, ['*'], 'page', $page);

        return response()->json([
            'total' => $pagination->total(),
            'data' => $pagination->items(),
            'perPage' => $pagination->perPage(),
            'lastpage' => $pagination->lastPage()
        ]);
    }

    public function update(EstoqueResquest $request, $id)
    {
        DB::beginTransaction();
        try {
            $estoque = Estoque::find($id);

            if ($estoque->quant != $request['quant']) {
                $estoque->quant = $request['quant'];
            }

            if ($estoque->custo != Helpers::RealToSQL($request['custo'])) {
                $estoque->custo = Helpers::RealToSQL($request['custo']);
            }
            if ($estoque->valor != Helpers::RealToSQL($request['valor'])) {
                $estoque->valor = Helpers::RealToSQL($request['valor']);
            }

            $estoque->save();

            DB::commit();
            return response()->json([
                'success' => true,
                'type' => 'success',
                'message' => Helpers::message('MSG002')
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);
            return response()->json([
                'success' => false,
                'type' => 'warning',
                'message' => Helpers::message('MSG012'),
                'errors' => $e
            ], 500);
        }
    }
}
