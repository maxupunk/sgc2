<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Helpers;
use App\Http\Controllers\Controller;
use App\Http\Requests\AutenticacaoRequest;
use App\Models\AcoesPerfil;
use App\Models\Usuario;
use App\Notifications\LoginNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Laravel\Sanctum\PersonalAccessToken;

class AutenticacaoController extends Controller
{
    public function login(AutenticacaoRequest $request)
    {
        $user = Usuario::where('email', $request->email)->first();

        if (!$user || !Hash::check($request->senha, $user->senha) || !$user->ativo) {
            return response()->json([
                'success' => false,
                'type' => 'warning',
                'message' => Helpers::message('MSG055'),
            ]);
        }

        $permissoes = AcoesPerfil::GetPermissoesByUserID($user->id);

        $str_metodos = "";
        foreach ($permissoes as $acao) {
            $separador = empty($str_metodos) ? null : '|';
            $str_metodos .= $separador . $acao;
            //$array_metodos += explode("|", $acao);
        }
        $array_metodos = explode("|", $str_metodos);
        
        //dd($array_metodos);

        $tokenobj  = $user->createToken($request->device_name, $array_metodos);

        if (!$request->lembrar) {
            $personalAccessToken = PersonalAccessToken::findToken($tokenobj->plainTextToken);
            $personalAccessToken->expires_at = now()->addHours(1);
            $personalAccessToken->save();
        }

        $response['usuario'] = $user;
        $response['token'] = $tokenobj->plainTextToken;
        $response['acl'] = $permissoes;
        $response['config'] = $user->config;

        $user->notify(new LoginNotification($response['token']));

        return response()->json($response);
    }

    public function logout(Request $request)
    {
        $request->user()->currentAccessToken()->delete();
        return response()->json([
            'success' => true,
            'type' => 'success',
            'message' => Helpers::message('MSG007')
        ]);
    }
}
