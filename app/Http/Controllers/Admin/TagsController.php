<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\TagRequest;
use App\Models\Tag;
use App\Helpers\Helpers;

class TagsController extends Controller
{

    public function index(Request $request)
    {
        $tags = Tag::select('*');

        if ($request->has('search') and $request->input('search') != '') {
            $search = $request->input('search');
            $tags = $tags->where('tag', 'LIKE', "%$search%");
        }

        if ($request->has('sortBy') and $request->input('sortBy') != null) {
            $sortBy = $request->input('sortBy');
            $sortDesc = $request->input('sortDesc');
            $ArraySort = array_combine($sortBy, $sortDesc);
            foreach ($ArraySort as $By => $Desc) {
                $tags = $tags->orderBy($By, $Desc == 'false' ? 'desc' : 'asc');
            }
        } else {
            $tags = $tags->orderBy('id', 'desc');
        }

        $rowsperpage = $request->input('itemsPerPage') > 0 ? $request->input('itemsPerPage') : $tags->count();
        $page = $request->input('page');

        $pagination = $tags->paginate($rowsperpage, ['*'], 'page', $page);

        return response()->json([
            'total' => $pagination->total(),
            'data' => $pagination->items(),
            'perPage' => $pagination->perPage()
        ]);;
    }

    public function store(TagRequest $request)
    {
        $input = $request->all();
        $medida = Tag::create($input);
        return response()->json([
            'success' => true,
            'type' => 'success',
            'message' => Helpers::message('MSG001'),
            'id' => $medida['id']
        ]);
    }

    public function show($id)
    {
        $medida = Tag::find($id);

        if ($medida->count()) {
            return response()->json($medida);
        }

        return response()->json(['error' => Helpers::message('MSG000')], 404);
    }

    public function update(TagRequest $request, $id)
    {
        $medida = Tag::find($id);
        $medida->update($request->all());
        return response()->json([
            'success' => true,
            'type' => 'success',
            'message' => Helpers::message('MSG002')
        ]);
    }

    public function destroy($id)
    {
        try {
            Tag::destroy($id);

            return response()->json([
                'success' => true,
                'type' => 'success',
                'message' => Helpers::message('MSG003')
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'type' => 'warning',
                'message' => Helpers::message('MSG005'),
                'errors' => $e
            ], 500);
        }
    }

    public function search(Request $request)
    {
        $search = $request->input('tag');

        $tags = Tag::select('tags.tag')
            ->where('tags.tag', 'like', "%{$search}%")
            ->take(10)
            ->pluck('tag');

        return response()->json($tags);
    }
}
