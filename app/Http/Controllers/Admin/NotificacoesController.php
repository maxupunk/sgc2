<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NotificacoesController extends Controller
{

    public function index(Request $request)
    {
        $notificacaos = $request->user()->Notifications()->orderBy('created_at', 'desc');

        $rowsperpage = $request->input('itemsPerPage') > 0 ? $request->input('itemsPerPage') : $notificacaos->count();
        $page = $request->input('page');

        $pagination = $notificacaos->Paginate($rowsperpage, ['*'], 'page', $page);

        return response()->json([
            'total' => $pagination->total(),
            'data' => $pagination->items(),
            'perPage' => $pagination->perPage(),
        ]);
    }

    public function marcaComoLida($id)
    {
        auth::user()->Notifications->find($id)->markAsRead();
    }

    public function marcaTodasComoLidas()
    {
        auth::user()->Notifications->markAsRead();
    }

    public function delete($id)
    {
        auth::user()->Notifications->find($id)->delete();
    }

    public function deleteTodas(Request $request)
    {
        $request->user()->readNotifications()->delete();
    }
}
