<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests\FinanceiroRequest;
use App\Models\Financeiro;
use Illuminate\Support\Facades\DB;
use App\Helpers\Helpers;

class FinanceirosController extends Controller
{

    public function index(Request $request)
    {
        $financeiros = Financeiro::leftJoin('pessoas', 'pessoas.id', '=', 'financeiros.pessoa_id')
            ->select(
                'pessoas.id AS pessoa_id',
                'pessoas.nome',
                'financeiros.id',
                'financeiros.orden_id',
                'financeiros.nature',
                'financeiros.descricao',
                'financeiros.valor',
                'financeiros.pago',
                'financeiros.vencimento',
                'financeiros.datapg',
                'financeiros.created_at',
                'financeiros.estatus'
            );

        if ($request->has('sortBy') and $request->input('sortBy') != null) {
            $sortBy = $request->input('sortBy');
            $sortDesc = $request->input('sortDesc');
            $ArraySort = array_combine($sortBy, $sortDesc);
            foreach ($ArraySort as $By => $Desc) {
                $financeiros = $financeiros->orderBy($By, $Desc == 'false' ? 'desc' : 'asc');
            }
        } else {
            $financeiros = $financeiros->orderBy('financeiros.id', 'desc');
        }

        if ($request->has('search') and $request->input('search') != '') {
            $search = $request->input('search');
            $financeiros = $financeiros->where('descricao', 'LIKE', "%$search%");
        }

        if ($request->has('nature') and $request->input('nature') != '') {
            $financeiros = $financeiros->where('financeiros.nature',  $request->input('nature'));
        }

        if ($request->has('estatus') and $request->input('estatus') != '') {
            $financeiros = $financeiros->where('financeiros.estatus', $request->input('estatus'));
        }

        if ($request->has('pessoaId') and $request->input('pessoaId') != '') {
            $financeiros = $financeiros->where('financeiros.pessoa_id', $request->input('pessoaId'));
            $ValorTotal = $financeiros->sum('financeiros.valor');
            $ValorPago = $financeiros->sum('financeiros.pago');
        }

        $rowsperpage = $request->input('itemsPerPage') > 0 ? $request->input('itemsPerPage') : 30;
        $page = $request->input('page');

        $pagination = $financeiros->paginate($rowsperpage, ['*'], 'page', $page);

        return response()->json([
            'total' => $pagination->total(),
            'data' => $pagination->items(),
            'perPage' => $pagination->perPage(),
            'lastpage' => $pagination->lastPage(),
            'valorTotal' => isset($ValorTotal) ? $ValorTotal : null,
            'valorPago' => isset($ValorPago) ? $ValorPago : null,
        ]);
    }

    public function store(FinanceiroRequest $request)
    {
        $input = $request->all();
        $financeiro = Financeiro::create($input);
        return response()->json([
            'success' => true,
            'type' => 'success',
            'message' => Helpers::message('MSG001'),
            'id' => $financeiro['id']
        ]);
    }

    public function show($id)
    {
        $financeiro = Financeiro::select('financeiros.*', 'pessoas.nome')
            ->leftJoin('pessoas', 'pessoas.id', '=', 'financeiros.pessoa_id')
            ->find($id);

        if ($financeiro) {
            $financeiro['historico'] = $financeiro->audits()->with('user')->get();
            return response()->json($financeiro);
        } else {
            return response()->json(['error' => Helpers::message('MSG000')], 404);
        }
    }

    public function update(FinanceiroRequest $request, $id)
    {
        $financeiro = Financeiro::find($id);

        $input = $request->all();
        if ($input['estatus'] == 'pg' || $input['pago'] >= $input['valor']) {
            $input['pago'] = $input['valor'];
            $input['datapg'] = Carbon::now();
            $input['estatus'] = 'PG';
        }

        $financeiro->update($input);
        return response()->json([
            'success' => true,
            'type' => 'success',
            'message' => Helpers::message('MSG002')
        ]);
    }

    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $financeiro = Financeiro::find($id);
            if ($financeiro->estatus == 'CN') {
                Financeiro::destroy($id);
            } else {
                $financeiro->estatus = 'CN';
                $financeiro->save();
            }

            DB::commit();
            return response()->json([
                'success' => true,
                'type' => 'success',
                'message' => Helpers::message('MSG003')
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'success' => false,
                'type' => 'warning',
                'message' => Helpers::message('MSG005'),
                'errors' => $e
            ], 500);
        }
    }

    public function baixar(FinanceiroRequest $request)
    {
        $financeiro = Financeiro::DaBaixa($request);
        if ($financeiro) {
            return response()->json([
                'success' => true,
                'type' => 'success',
                'message' => Helpers::message('MSG050')
            ]);
        } else {
            return response()->json([
                'success' => false,
                'type' => 'warning',
                'message' => Helpers::message('MSG020')
            ]);
        }
    }

    public function baixar_por_pessoa(FinanceiroRequest $request)
    {
        $financeiros = Financeiro::where('pessoa_id', '=', $request->pessoa_id)
            ->where('nature', '=', $request->nature)
            ->where('estatus', '=', $request->estatus)
            ->whereRaw('financeiros.valor > financeiros.pago')
            ->orderBy('financeiros.vencimento', 'asc')
            ->get();

        if (count($financeiros) > 0) {
            $valorPago = Helpers::RealToSQL($request->valorpago);

            DB::beginTransaction();
            try {
                foreach ($financeiros as $financeiro) {
                    if ($valorPago > 0) {
                        $restante = $financeiro['valor'] - $financeiro['pago'];
                        if ($valorPago > $restante) {
                            $DeuBaixa = Financeiro::DaBaixa((object) [
                                'id' => $financeiro['id'],
                                'valorpago' => $restante
                            ]);
                            if ($DeuBaixa) {
                                $valorPago -= $restante;
                            } else {
                                DB::rollBack();
                            }
                        } else {
                            $DeuBaixa = Financeiro::DaBaixa((object) [
                                'id' => $financeiro['id'],
                                'valorpago' => $valorPago
                            ]);
                            if ($DeuBaixa) {
                                break;
                            }
                        }
                    } else {
                        break;
                    }
                }
                DB::commit();
                return response()->json([
                    'success' => true,
                    'type' => 'success',
                    'message' => Helpers::message('MSG050')
                ]);
            } catch (\Exception $e) {
                DB::rollBack();
                return response()->json([
                    'success' => false,
                    'type' => 'warning',
                    'message' => Helpers::message('MSG020'),
                    'errors' => $e
                ], 500);
            }
        } else {
            return response()->json([
                'success' => false,
                'type' => 'warning',
                'message' => Helpers::message('MSG047')
            ]);
        }
    }
    //
    public function relatorio(Request $request)
    {
        $relatorio = Financeiro::select('financeiros.*', 'pessoas.nome', 'pessoas.tipo')
            ->leftJoin('pessoas', 'pessoas.id', '=', 'financeiros.pessoa_id')
            ->leftJoin('financeiro_historico', 'financeiro_historico.financ_id', '=', 'financeiros.id')
            ->distinct();

        if ($request->has('nature') and $request->input('nature') != '') {
            $nature = $request->input('nature');
            $relatorio = $relatorio->where('financeiros.nature', '=', $nature);
        }

        if ($request->has('datapg_inicial') and $request->input('datapg_inicial') != '') {
            $data_inicial = Carbon::parse($request->input('datapg_inicial'));
            $relatorio = $relatorio->where('financeiros.datapg', '>=', $data_inicial);
        }

        if ($request->has('datapg_final') and $request->input('datapg_final') != '') {
            $data_final = Carbon::parse($request->input('datapg_final'));
            $relatorio = $relatorio->where('financeiros.datapg', '<=', $data_final);
        }

        if ($request->has('vencimento_inicial') and $request->input('vencimento_inicial') != '') {
            $data_inicial = Carbon::parse($request->input('vencimento_inicial'));
            $relatorio = $relatorio->where('financeiros.vencimento', '>=', $data_inicial);
        }

        if ($request->has('vencimento_final') and $request->input('vencimento_final') != '') {
            $data_final = Carbon::parse($request->input('vencimento_final'));
            $relatorio = $relatorio->where('financeiros.vencimento', '<=', $data_final);
        }

        //////////////// PAGO MINIMO MAXIMO //////////////////
        if ($request->has('pago_min') and $request->input('pago_min') != '') {
            $pago_min = Helpers::RealToSQL($request->input('pago_min'));
            $relatorio = $relatorio->where('financeiros.pago', '>=', $pago_min);
        }

        if ($request->has('pago_max') and $request->input('pago_max') != '' and $request->input('pago_min') != 0) {
            $pago_max = Helpers::RealToSQL($request->input('pago_max'));
            $relatorio = $relatorio->where('financeiros.pago', '<=', $pago_max);
        }

        //////////////// VALOR MINIMO MAXIMO //////////////////
        if ($request->has('valor_min') and $request->input('valor_min') != '') {
            $valor_min = Helpers::RealToSQL($request->input('valor_min'));
            $relatorio = $relatorio->where('financeiros.valor', '>=', $valor_min);
        }

        if ($request->has('valor_max') and $request->input('valor_max') != '' and $request->input('valor_max') != 0) {
            $valor_max = Helpers::RealToSQL($request->input('valor_max'));
            $relatorio = $relatorio->where('financeiros.valor', '<=', $valor_max);
        }

        if ($request->has('estatus') and $request->input('estatus') != '') {
            $status = $request->input('estatus');
            $relatorio = $relatorio->where('financeiros.estatus', '=', $status);
        }

        if ($request->has('pessoa_id') and $request->input('pessoa_id') != '') {
            $pessoa_id = $request->input('pessoa_id');
            $relatorio = $relatorio->where('financeiros.pessoa_id', '=', $pessoa_id);
        }

        if ($request->has('usuario_id') and $request->input('usuario_id') != '') {
            $usuario_id = $request->input('usuario_id');
            $relatorio = $relatorio->where('financeiro_historico.usuario_id', '=', $usuario_id);
        }

        if ($request->has('pessoa_tipo') and $request->input('pessoa_tipo') != '') {
            $usuario_id = $request->input('pessoa_tipo');
            $relatorio = $relatorio->where('pessoas.tipo', '=', $usuario_id);
        }

        return response()->json($relatorio->get());
    }
}
