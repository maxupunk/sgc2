<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Usuario;
use App\Http\Requests\UsuarioRequest;
use Illuminate\Support\Facades\DB;
use App\Helpers\Helpers;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;

class UsuariosController extends Controller
{
    public function index(Request $request)
    {
        $usuarios = Usuario::join('perfis', 'perfis.id', '=', 'usuarios.perfil_id')
            ->select(
                'usuarios.id',
                'usuarios.email',
                'perfis.descricao AS perfil',
                'usuarios.ativo',
                'usuarios.created_at',
                'usuarios.updated_at'
            );

        if ($request->has('sortBy') and $request->input('sortBy') != null) {
            $sortBy = $request->input('sortBy');
            $sortDesc = $request->input('sortDesc');
            $ArraySort = array_combine($sortBy, $sortDesc);
            foreach ($ArraySort as $By => $Desc) {
                $usuarios = $usuarios->orderBy($By, $Desc == 'false' ? 'desc' : 'asc');
            }
        } else {
            $usuarios = $usuarios->orderBy('usuarios.id', 'desc');
        }

        if ($request->has('search') and $request->input('search') != '') {
            $search = $request->input('search');
            $usuarios = $usuarios->where('perfis.descricao', 'LIKE', "%$search%")
                ->OrWhere('usuarios.email', 'LIKE', "%$search%");
        }

        $rowsperpage = $request->input('itemsPerPage') > 0 ? $request->input('itemsPerPage') : 30;
        $page = $request->input('page');

        $pagination = $usuarios->paginate($rowsperpage, ['*'], 'page', $page);

        return response()->json([
            'total' => $pagination->total(),
            'data' => $pagination->items(),
            'perPage' => $pagination->perPage(),
            'lastpage' => $pagination->lastPage()
        ]);
    }

    public function store(UsuarioRequest $request)
    {
        $inputs = $request->all();
        DB::beginTransaction();
        try {
            $usuario = Usuario::create($inputs);

            DB::commit();
            return response()->json([
                'success' => true,
                'type' => 'success',
                'message' => Helpers::message('MSG001'),
                'id' => $usuario->id
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);
            return response()->json([
                'type' => 'warning',
                'message' => Helpers::message('MSG012'),
                'errors' => $e
            ], 500);
        }
    }

    public function show($id)
    {
        $usuario = Usuario::select(
            'usuarios.id',
            'usuarios.nome',
            'usuarios.ativo',
            'usuarios.email',
            'usuarios.config',
            'perfil_id',
            'usuarios.imagem',
            'pessoa_id',
            'pessoas.nome AS pessoa',
        )
            ->leftJoin('pessoas', 'pessoas.id', '=', 'usuarios.pessoa_id')
            ->find($id);

        if ($usuario->count()) {
            $usuario['tokens'] = $usuario->tokens()->get();
            $usuario['historico'] = $usuario->audits()->get();
            return response()->json($usuario);
        }

        return response()->json(['error' => Helpers::message('MSG000')], 404);
    }

    public function update(UsuarioRequest $request, $id)
    {
        $inputs = $request->only('email', 'senha', 'imagem', 'config');
        try {
            Usuario::find($id)->update($inputs);
            return response()->json([
                'success' => true,
                'type' => 'success',
                'message' => Helpers::message('MSG002')
            ]);
        } catch (QueryException $e) {
            return response()->json([
                'success' => false,
                'type' => 'warning',
                'message' => Helpers::message('MSG005'),
                'errors' => $e
            ], 500);
        }
    }

    public function perfil(UsuarioRequest $request, $id)
    {
        $inputs = $request->all();
        try {
            Usuario::find($id)->update($inputs);

            return response()->json([
                'success' => true,
                'type' => 'success',
                'message' => Helpers::message('MSG002')
            ]);
        } catch (QueryException $e) {
            return response()->json([
                'success' => false,
                'type' => 'warning',
                'message' => Helpers::message('MSG005'),
                'errors' => $e
            ], 500);
        }
    }

    public function destroy($id)
    {
        try {
            $usuario = Usuario::find($id);
            $usuario->delete();

            return response()->json([
                'success' => true,
                'type' => 'success',
                'message' => Helpers::message('MSG003')
            ]);
        } catch (QueryException $e) {
            return response()->json([
                'success' => false,
                'type' => 'warning',
                'message' => Helpers::message('MSG005'),
                'errors' => $e
            ], 500);
        }
    }

    public function deletaToken($id)
    {
        $userAuth = auth::user();
        $user = Usuario::find($userAuth->id)->tokens()->where('id', $id)->delete();
        if ($user) {
            return response()->json([
                'success' => true,
                'type' => 'success',
                'message' => Helpers::message('MSG060')
            ]);
        } else {
            return response()->json([
                'success' => false,
                'type' => 'warning',
                'message' => Helpers::message('MSG020'),
            ], 500);
        }
    }

    public function deletaTokenOutros($userId, $tokenId)
    {
        $user = Usuario::find($userId)->tokens()->where('id', $tokenId)->delete();
        if ($user) {
            return response()->json([
                'success' => true,
                'type' => 'success',
                'message' => Helpers::message('MSG060')
            ]);
        } else {
            return response()->json([
                'success' => false,
                'type' => 'warning',
                'message' => Helpers::message('MSG020'),
            ], 500);
        }
    }

    public function listing()
    {
        return response()->json(
            Usuario::select('id', 'nome', 'email')
                ->where('ativo', '=', 1)->get()
        );
    }
}
