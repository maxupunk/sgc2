<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Marca;
use App\Http\Requests\MarcaRequest;
use App\Helpers\Helpers;

class MarcasController extends Controller
{
    public function index(Request $request)
    {
        $marcas = Marca::select('*');

        if ($request->has('sortBy') and $request->input('sortBy') != null) {
            $sortBy = $request->input('sortBy');
            $sortDesc = $request->input('sortDesc');
            $ArraySort = array_combine($sortBy, $sortDesc);
            foreach ($ArraySort as $By => $Desc) {
                $marcas = $marcas->orderBy($By, $Desc == 'false' ? 'desc' : 'asc');
            }
        } else {
            $marcas = $marcas->orderBy('id', 'desc');
        }

        if ($request->has('search') and $request->input('search') != '') {
            $search = $request->input('search');
            $marcas = $marcas->where('nome', 'LIKE', "%$search%");
        }

        $rowsperpage = $request->input('itemsPerPage') > 0 ? $request->input('itemsPerPage') : 30;
        $page = $request->input('page');

        $pagination = $marcas->paginate($rowsperpage, ['*'], 'page', $page);

        return response()->json([
            'total' => $pagination->total(),
            'data' => $pagination->items(),
            'perPage' => $pagination->perPage(),
            'lastpage' => $pagination->lastPage()
        ]);;
    }

    public function store(MarcaRequest $request)
    {
        $input = $request->all();
        $marca = Marca::create($input);
        return response()->json([
            'success' => true,
            'type' => 'success',
            'message' => Helpers::message('MSG001'),
            'id' => $marca['id']
        ]);
    }

    public function show($id)
    {
        $marca = Marca::find($id);

        if ($marca->count())
            return response()->json($marca);

        return response()->json(['error' =>  Helpers::message('MSG000')], 404);
    }

    public function update(MarcaRequest $request, $id)
    {
        $marca = Marca::find($id);
        $marca->update($request->all());
        return response()->json([
            'success' => true,
            'type' => 'success',
            'message' => Helpers::message('MSG002')
        ]);
    }

    public function destroy($id)
    {
        try {
            Marca::destroy($id);

            return response()->json([
                'success' => true,
                'type' => 'success',
                'message' => Helpers::message('MSG003')
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'type' => 'warning',
                'message' => Helpers::message('MSG005'),
                'errors' => $e
            ], 500);
        }
    }

    public function listing()
    {
        return response()->json(Marca::listMarcas());
    }
}
