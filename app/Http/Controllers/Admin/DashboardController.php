<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Estoque;
use App\Models\Financeiro;
use App\Models\Orden;

class DashboardController extends Controller
{

    public function index()
    {
        $retorno['servicoAberto'] = Orden::where('ordens.tipo', '=', 's')
            ->where('ordens.status', '=', 'EA')->count();

        $retorno['vendaAberto'] = Orden::where('ordens.tipo', '=', 'v')
            ->where('ordens.status', '=', 'EA')->count();

        $retorno['compraAberto'] = Orden::where('ordens.tipo', '=', 'c')
            ->where('ordens.status', '=', 'EA')->count();

        $retorno['receitas'] = Financeiro::Hoje('R')->count();

        $retorno['despesas'] = Financeiro::Hoje('D')->count();

        $retorno['servicoAbertoLst'] = Orden::getLstByStatus('EA', 's')->orderBy('ordens.created_at', 'asc')->get();

        $retorno['estoqueAlerta'] = Estoque::alerta();

        return response()->json($retorno);
    }


}
