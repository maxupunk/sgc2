<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ProdutoCest;
use App\Models\ProdutoCfop;
use App\Models\ProdutoIcms;
use App\Models\ProdutoIpi;
use App\Models\ProdutoNcm;
use App\Models\ProdutoOrigem;
use App\Models\ProdutoPisCofins;
use App\Models\ProdutoTipo;
use Illuminate\Http\Request;

class ProdutoFiscalController extends Controller
{

    public function index()
    {
        //$ListaFical = [];
        $ListaFical['cest'] = ProdutoCest::get();
        $ListaFical['cfop'] = ProdutoCfop::get();
        $ListaFical['icms'] = ProdutoIcms::get();
        $ListaFical['ipi'] = ProdutoIpi::get();
        $ListaFical['ncm'] = ProdutoNcm::get();
        $ListaFical['origem'] = ProdutoOrigem::get();
        $ListaFical['piscofins'] = ProdutoPisCofins::get();
        $ListaFical['tipo'] = ProdutoTipo::get();

        return response()->json($ListaFical);
    }

    public function cest($search)
    {
        if ($search !== null) {
            $searchValues = preg_split('/\s+/', $search, -1, PREG_SPLIT_NO_EMPTY);

            $cest = ProdutoCest::where('codigo', '=', $searchValues)
                ->OrWhere(
                    function ($q) use ($searchValues) {
                        foreach ($searchValues as $value) {
                            $q->Where('descricao', 'like', "%{$value}%");
                        }
                    })
                ->get();

            return response()->json($cest);
        }
    }

    public function cfop($search)
    {
        if ($search !== null) {
            $searchValues = preg_split('/\s+/', $search, -1, PREG_SPLIT_NO_EMPTY);

            $cest = ProdutoCfop::where(
                function ($q) use ($searchValues) {
                    foreach ($searchValues as $value) {
                        $q->Where('descricao', 'like', "%{$value}%");
                    }
                })
                ->get();

            return response()->json($cest);
        }
    }

    public function icms($search)
    {
        if ($search !== null) {
            $searchValues = preg_split('/\s+/', $search, -1, PREG_SPLIT_NO_EMPTY);

            $cest = ProdutoIcms::where('codigo', '=', $searchValues)
                ->OrWhere(
                    function ($q) use ($searchValues) {
                        foreach ($searchValues as $value) {
                            $q->Where('descricao', 'like', "%{$value}%");
                        }
                    })
                ->get();

            return response()->json($cest);
        }
    }

    public function ipi($search)
    {
        if ($search !== null) {
            $searchValues = preg_split('/\s+/', $search, -1, PREG_SPLIT_NO_EMPTY);

            $cest = ProdutoIpi::where('codigo', '=', $searchValues)
                ->OrWhere(
                    function ($q) use ($searchValues) {
                        foreach ($searchValues as $value) {
                            $q->Where('descricao', 'like', "%{$value}%");
                        }
                    })
                ->get();

            return response()->json($cest);
        }
    }

    public function ncm($search)
    {
        if ($search !== null) {
            $searchValues = preg_split('/\s+/', $search, -1, PREG_SPLIT_NO_EMPTY);

            $cest = ProdutoNcm::where('codigo', '=', $searchValues)
                ->OrWhere(
                    function ($q) use ($searchValues) {
                        foreach ($searchValues as $value) {
                            $q->Where('descricao', 'like', "%{$value}%");
                        }
                    })
                ->get();

            return response()->json($cest);
        }
    }

    public function origem($search)
    {
        if ($search !== null) {
            $searchValues = preg_split('/\s+/', $search, -1, PREG_SPLIT_NO_EMPTY);

            $cest = ProdutoOrigem::where(
                function ($q) use ($searchValues) {
                    foreach ($searchValues as $value) {
                        $q->Where('descricao', 'like', "%{$value}%");
                    }
                })
                ->get();

            return response()->json($cest);
        }
    }

    public function piscofins($search)
    {
        if ($search !== null) {
            $searchValues = preg_split('/\s+/', $search, -1, PREG_SPLIT_NO_EMPTY);

            $cest = ProdutoPisCofins::where('codigo', '=', $searchValues)
                ->OrWhere(
                    function ($q) use ($searchValues) {
                        foreach ($searchValues as $value) {
                            $q->Where('descricao', 'like', "%{$value}%");
                        }
                    })
                ->get();

            return response()->json($cest);
        }
    }

    public function tipo($search)
    {
        if ($search !== null) {
            $searchValues = preg_split('/\s+/', $search, -1, PREG_SPLIT_NO_EMPTY);

            $cest = ProdutoOrigem::where(
                function ($q) use ($searchValues) {
                    foreach ($searchValues as $value) {
                        $q->Where('descricao', 'like', "%{$value}%");
                    }
                })
                ->get();

            return response()->json($cest);
        }
    }
}
