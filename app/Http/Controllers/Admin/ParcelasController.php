<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\ParcelaRequest;
use App\Models\Parcela;
use App\Helpers\Helpers;

class ParcelasController extends Controller
{
    private $parcela;

    public function __construct(Parcela $parcela)
    {
        $this->parcela = $parcela;
    }

    public function index(Request $request)
    {
        $parcelas = $this->parcela->orderBy('nparcela', 'asc')
            ->join('formapgs', 'formapgs.id', '=', 'parcelas.formapg_id')
            ->select('parcelas.id', 'parcelas.nparcela', 'parcelas.juros', 'formapgs.id AS formapg_id')
            ->where('formapg_id', '=', $request->input('id'))
            ->get();

        return response()->json($parcelas);

    }

    public function store(ParcelaRequest $request)
    {
        $input = $request->all();
        $this->parcela->create($input);
        return response()->json([
            'success' => true
        ]);
    }

    public function show($id)
    {
        $parcela = $this->parcela
            ->join('formapgs', 'formapgs.id', '=', 'parcelas.formapg_id')
            ->select('parcelas.id', 'parcelas.nparcela', 'parcelas.juros',
                'formapgs.descricao', 'formapgs.custo', 'formapgs.id AS formapg_id')
            ->find($id);

        if ($parcela->count())
            return response()->json($parcela);

        return response()->json(['error' => Helpers::message('MSG000')], 404);
    }

    public function update(ParcelaRequest $request, $id)
    {
        $parcela = $this->parcela->find($id);
        $parcela->update($request->all());
        return response()->json([
            'success' => true
        ]);
    }

    public function destroy($id)
    {
        try {
            $this->parcela->destroy($id);

            return response()->json([
                'success' => true,
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'type' => 'warning',
                'message' => Helpers::message('MSG005'),
                'errors' => $e
            ], 500);
        }
    }

    public function getParcelas($formapg_id)
    {
        return response()->json($this->parcela->listParcelas($formapg_id));
    }
}
