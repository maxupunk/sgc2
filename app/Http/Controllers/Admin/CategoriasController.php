<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\CategoriaRequest;
use App\Models\Categoria;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Helpers\Helpers;

class CategoriasController extends Controller
{

    public function index(Request $request)
    {
        $categorias = Categoria::select('*');

        if ($request->has('sortBy') and $request->input('sortBy') != null) {
            $sortBy = $request->input('sortBy');
            $sortDesc = $request->input('sortDesc');
            $ArraySort = array_combine($sortBy, $sortDesc);
            foreach ($ArraySort as $By => $Desc) {
                $categorias = $categorias->orderBy($By, $Desc == 'false' ? 'desc' : 'asc');
            }
        } else {
            $categorias = $categorias->orderBy('id', 'desc');
        }

        if ($request->has('search') and $request->input('search') != '') {
            $search = $request->input('search');
            $categorias = $categorias->where('nome', 'LIKE', "%$search%");
        }

        $rowsperpage = $request->input('itemsPerPage') > 0 ? $request->input('itemsPerPage') : 30;
        $page = $request->input('page');

        $pagination = $categorias->paginate($rowsperpage, ['*'], 'page', $page);

        return response()->json([
            'total' => $pagination->total(),
            'data' => $pagination->items(),
            'perPage' => $pagination->perPage(),
            'lastpage' => $pagination->lastPage()
        ]);
    }

    public function store(CategoriaRequest $request)
    {
        $input = $request->all();
        DB::beginTransaction();
        try {
            $categoria = Categoria::create($input);
            DB::commit();
            return response()->json([
                'success' => true,
                'type' => 'success',
                'message' => Helpers::message('MSG001'),
                'id' => $categoria['id']
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);
            return response()->json([
                'type' => 'warning',
                'message' => Helpers::message('MSG012'),
                'errors' => $e
            ], 500);
        }
    }

    public function show($id)
    {
        $categoria = Categoria::find($id);

        if ($categoria->count())
            return response()->json($categoria);

        return response()->json(['error' => Helpers::message('MSG000')], 404);
    }

    public function update(CategoriaRequest $request, $id)
    {
        try {
            $categoria = Categoria::find($id);
            $categoria->update($request->all());
            return response()->json([
                'success' => true,
                'type' => 'success',
                'message' => Helpers::message('MSG002')
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'type' => 'warning',
                'message' => Helpers::message('MSG005'),
                'errors' => $e
            ], 500);
        }
    }

    public function destroy($id)
    {
        try {
            Categoria::destroy($id);

            return response()->json([
                'success' => true,
                'type' => 'success',
                'message' => Helpers::message('MSG003')
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'type' => 'warning',
                'message' => Helpers::message('MSG005'),
                'errors' => $e
            ], 500);
        }
    }

    public function listing()
    {
        return response()->json(Categoria::listCategorias());
    }
}
