<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\Helpers;
use App\Http\Requests\PrestacaoContaRequest;
use App\Models\Financeiro;
use App\Models\PrestacaoConta;
use Exception;

class PrestacaoContaController extends Controller
{

    public function index(Request $request)
    {
        $PrestacaoConta = PrestacaoConta::leftJoin('lista_itens', 'lista_itens.id', '=', 'prestacao_contas.lista_iten_id')
            ->leftJoin('estoques', 'estoques.id', '=', 'lista_itens.estoque_id')
            ->leftJoin('produtos', 'produtos.id', '=', 'estoques.produto_id')
            ->leftJoin('medidas', 'medidas.id', '=', 'produtos.medida_id')
            ->leftJoin('marcas', 'marcas.id', '=', 'produtos.marca_id')
            ->leftJoin('usuarios', 'usuarios.id', '=', 'prestacao_contas.usuario_id')
            ->leftJoin('ordens', 'ordens.id', '=', 'lista_itens.orden_id')
            ->leftJoin('pessoas', 'pessoas.id', '=', 'ordens.pessoa_id')
            ->select(
                'prestacao_contas.id',
                'prestacao_contas.usuario_id',
                'prestacao_contas.financeiro_id',
                'prestacao_contas.pagamento_id',
                'prestacao_contas.comissao',
                'prestacao_contas.created_at',
                'produtos.descricao',
                'estoques.variante',
                'lista_itens.orden_id',
                'lista_itens.observacao',
                'lista_itens.valor',
                'lista_itens.quant',
                'usuarios.nome AS usuario',
                'pessoas.nome AS cliente',
                'marcas.nome AS marca',
                'medidas.nome AS medida',
            );

        if ($request->has('search') and $request->input('search') != '') {
            $search = $request->input('search');
            $PrestacaoConta = $PrestacaoConta->OrWhere('pessoas.nome', 'LIKE', "%$search%")
                ->OrWhere('produtos.descricao', 'LIKE', "%$search%")
                ->OrWhere('lista_itens.observacao', 'LIKE', "%$search%");
        }

        if ($request->has('sortBy') and $request->input('sortBy') != null) {
            $sortBy = $request->input('sortBy');
            $sortDesc = $request->input('sortDesc');
            $ArraySort = array_combine($sortBy, $sortDesc);
            foreach ($ArraySort as $By => $Desc) {
                $PrestacaoConta = $PrestacaoConta->orderBy($By, $Desc == 'false' ? 'desc' : 'asc');
            }
        } else {
            $PrestacaoConta = $PrestacaoConta->orderBy('prestacao_contas.id', 'desc');
        }

        if ($request->has('dataInicial') and $request->input('dataInicial') != null) {
            $data_inicial = Helpers::DataToSQL($request->input('dataInicial'));
            $PrestacaoConta = $PrestacaoConta->where('prestacao_contas.created_at', '>=', $data_inicial);
        }

        if ($request->has('dataFinal') and $request->input('dataFinal') != null) {
            $data_final = Helpers::DataToSQL($request->input('dataFinal'));
            $PrestacaoConta = $PrestacaoConta->where('prestacao_contas.created_at', '<=', $data_final);
        }

        if ($request->has('usuarioId') and $request->input('usuarioId') != null) {
            $usuario_id = $request->input('usuarioId');
            $PrestacaoConta = $PrestacaoConta->where('prestacao_contas.usuario_id', $usuario_id);
        }

        $rowsperpage = $request->input('itemsPerPage') > 0 ? $request->input('itemsPerPage') : 30;
        $page = $request->input('page');

        $pagination = $PrestacaoConta->paginate($rowsperpage, ['*'], 'page', $page);

        return response()->json([
            'total' => $pagination->total(),
            'data' => $pagination->items(),
            'perPage' => $pagination->perPage(),
            'lastpage' => $pagination->lastPage()
        ]);
    }

    public function show($usuarioId)
    {
        $listaPrestacao = PrestacaoConta::ListComissao($usuarioId);

        if ($listaPrestacao) {
            return response()->json($listaPrestacao);
        } else {
            return response()->json([
                'success' => false,
                'type' => 'success',
                'message' => Helpers::message("MSG052"),
            ]);
        }
    }

    public function store(PrestacaoContaRequest $request)
    {
        $pgstatus = $request->has('pgstatus') ? $request->input('pgstatus') : null;
        $usuarioId = $request->input('usuarioId');

        $PrestacaoConta = PrestacaoConta::criaPestacao($usuarioId, $pgstatus);
        if ($PrestacaoConta === true) {
            return response()->json([
                'success' => true,
                'type' => 'success',
                'message' => Helpers::message('MSG001')
            ]);
        } else if ($PrestacaoConta === null) {
            return response()->json([
                'success' => false,
                'type' => 'warning',
                'message' => Helpers::message('MSG052')
            ]);
        } else {
            return response()->json([
                'success' => false,
                'type' => 'warning',
                'message' => Helpers::message('MSG020')
            ]);
        }
    }

    public function destroy($id)
    {
        try {
            $PrestacaoConta = PrestacaoConta::find($id);
            PrestacaoConta::where('pagamento_id', $PrestacaoConta->pagamento_id)->delete();
            Financeiro::destroy($PrestacaoConta->pagamento_id);
            return response()->json([
                'success' => true,
                'type' => 'success',
                'message' => Helpers::message('MSG003')
            ]);
        } catch (Exception $e) {
            return response()->json([
                'success' => false,
                'type' => 'warning',
                'message' => Helpers::message('MSG005'),
                'errors' => $e
            ], 500);
        }
    }
}
