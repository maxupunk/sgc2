<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Avaria;
use App\Models\Estoque;
use Illuminate\Http\Request;
use App\Http\Requests\AvariaRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Helpers\Helpers;
use Illuminate\Database\QueryException;

class AvariasController extends Controller
{
    public function index(Request $request)
    {

        $avarias = Avaria::join('estoques', 'estoques.id', '=', 'avarias.estoque_id')
            ->join('produtos', 'produtos.id', '=', 'estoques.produto_id')
            ->leftJoin('usuarios', 'usuarios.id', '=', 'avarias.usuario_id')
            ->leftJoin('medidas', 'medidas.id', '=', 'produtos.medida_id')
            ->leftJoin('marcas', 'marcas.id', '=', 'produtos.marca_id')
            ->select(
                'avarias.id',
                'avarias.motivo',
                'avarias.quant',
                'avarias.valor',
                'estoques.codigo',
                'estoques.variante',
                'produtos.descricao',
                'medidas.sigla AS medida',
                'marcas.nome AS marca',
                'usuarios.nome AS usuario'
            );


        if ($request->has('sortBy') and $request->input('sortBy') != null) {
            $sortBy = $request->input('sortBy');
            $sortDesc = $request->input('sortDesc');
            $ArraySort = array_combine($sortBy, $sortDesc);
            foreach ($ArraySort as $By => $Desc) {
                $avarias = $avarias->orderBy($By, $Desc == 'false' ? 'desc' : 'asc');;
            }
        } else {
            $avarias = $avarias->orderBy('avarias.id', 'desc');
        }

        if ($request->has('search') and $request->input('search') != '') {
            $search = $request->input('search');
            $searchValues = preg_split('/\s+/', $search, -1, PREG_SPLIT_NO_EMPTY);

            $avarias = $avarias->where('avarias.motivo', 'LIKE', "%$search%")
                ->OrWhere(
                    function ($q) use ($searchValues) {
                        foreach ($searchValues as $value) {
                            $q->Where('produtos.descricao', 'like', "%{$value}%");
                        }
                    }
                );
        }

        $rowsperpage = $request->input('itemsPerPage') > 0 ? $request->input('itemsPerPage') : 30;
        $page = $request->input('page');

        $pagination = $avarias->paginate($rowsperpage, ['*'], 'page', $page);

        return response()->json([
            'total' => $pagination->total(),
            'data' => $pagination->items(),
            'perPage' => $pagination->perPage(),
            'lastpage' => $pagination->lastPage()
        ]);
    }

    public function store(AvariaRequest $request)
    {
        try {
            $estoque = Estoque::find($request->estoque_id);
            $AvariaDados['usuario_id'] = auth()->id();
            $AvariaDados['estoque_id'] = $request->estoque_id;
            $AvariaDados['quant'] = $request->quant;
            $AvariaDados['valor'] = $estoque->custo;
            $AvariaDados['motivo'] = $request->motivo;

            $avaria = Avaria::create($AvariaDados);

            Estoque::Saida($request->estoque_id, $request->quant);

            return response()->json([
                'success' => true,
                'type' => 'success',
                'message' => Helpers::message('MSG001'),
                'id' => $avaria['id']
            ]);
        } catch (QueryException $e) {
            DB::rollBack();
            Log::error($e);
            return response()->json([
                'type' => 'warning',
                'message' => Helpers::message('MSG012'),
                'errors' => $e
            ], 500);
        }
    }

    public function show($id)
    {
        $avaria = Avaria::join('estoques', 'estoques.id', '=', 'avarias.estoque_id')
            ->join('produtos', 'produtos.id', '=', 'estoques.produto_id')
            ->leftJoin('marcas', 'marcas.id', '=', 'produtos.marca_id')
            ->select(
                'avarias.id',
                'avarias.estoque_id',
                'avarias.motivo',
                'avarias.quant',
                'produtos.descricao',
                'estoques.variante',
                'marcas.nome AS marca'
            )
            ->where('avarias.id', '=', $id)
            ->first();

        if ($avaria->count())
            return response()->json($avaria);

        return response()->json(['error' => Helpers::message('MSG000')], 404);
    }

    public function update(AvariaRequest $request, $id)
    {
        try {
            // Desfaz a avaria antes de atualizar
            $avaria = Avaria::find($request->id);
            Estoque::RollBackSaida($avaria->estoque_id, $avaria->quant);

            // atualiza os dados
            $estoque = Estoque::find($request->estoque_id);
            $AvariaDados['estoque_id'] = $request->estoque_id;
            $AvariaDados['quant'] = $request->quant;
            $AvariaDados['valor'] = $estoque->custo;
            $AvariaDados['motivo'] = $request->motivo;

            Avaria::find($request->id)->update($AvariaDados);

            // Baixa o produto
            Estoque::Saida($request->estoque_id, $request->quant);
            return response()->json([
                'success' => true,
                'type' => 'success',
                'message' => Helpers::message('MSG002'),
                'id' => $request->id
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'type' => 'warning',
                'message' => Helpers::message('MSG005'),
                'errors' => $e
            ], 500);
        }
    }

    public function destroy($id)
    {
        try {
            //Desfaz a avaria antes de deletar
            $avaria = Avaria::find($id);
            Estoque::RollBackSaida($avaria->estoque_id, $avaria->quant);
            Avaria::destroy($id);

            return response()->json([
                'success' => true,
                'type' => 'success',
                'message' => Helpers::message('MSG003')
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'type' => 'warning',
                'message' => Helpers::message('MSG005'),
                'errors' => $e
            ], 500);
        }
    }

    public function listing()
    {
        return response()->json(Avaria::listCategorias());
    }
}
