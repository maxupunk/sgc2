<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Backup;
use Illuminate\Http\Request;
use App\Helpers\Helpers;

class BackupsController extends Controller
{
    public function index()
    {
        $listaArquivos = Backup::lista();
        return response()->json($listaArquivos);
    }

    public function store(Request $request)
    {
        $filename = $request->has('filename') ? $request->input('filename') : null;
        $backup = Backup::criar($filename);
        if ($backup) {
            return response()->json([
                'success' => true,
                'type' => 'success',
                'message' => Helpers::message('MSG042')
            ]);
        } else {
            return response()->json([
                'type' => 'warning',
                'message' => Helpers::message('MSG043'),
                'errors' => $backup
            ], 500);
        }
    }

    public function show($id)
    {
        ini_set('memory_limit', '1024M');
        $download = Backup::download($id);
        if ($download) {
            return $download;
        } else {
            return response()->json([
                'success' => false,
                'type' => 'warning',
                'message' => Helpers::message('MSG046')
            ], 500);
        }
    }

    public function update($id)
    {
        $backup = Backup::restaure($id);
        if ($backup) {
            return response()->json([
                'success' => true,
                'type' => 'success',
                'message' => Helpers::message('MSG048')
            ]);
        } else {
            return response()->json([
                'type' => 'warning',
                'message' => Helpers::message('MSG012'),
                'errors' => $backup
            ], 500);
        }
    }


    public function destroy($id)
    {
        if (Backup::delete($id)) {
            return response()->json([
                'success' => true,
                'type' => 'success',
                'message' => Helpers::message('MSG044')
            ]);
        } else {
            return response()->json([
                'success' => false,
                'type' => 'warning',
                'message' => Helpers::message('MSG045')
            ], 500);
        }
    }
}
