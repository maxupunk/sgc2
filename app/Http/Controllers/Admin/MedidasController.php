<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\MedidaRequest;
use App\Models\Medida;
use App\Helpers\Helpers;

class MedidasController extends Controller
{
    public function index(Request $request)
    {
        $medidas = Medida::select('*');

        if ($request->has('sortBy') and $request->input('sortBy') != null) {
            $sortBy = $request->input('sortBy');
            $sortDesc = $request->input('sortDesc');
            $ArraySort = array_combine($sortBy, $sortDesc);
            foreach ($ArraySort as $By => $Desc) {
                $medidas = $medidas->orderBy($By, $Desc == 'false' ? 'desc' : 'asc');
            }
        } else {
            $medidas = $medidas->orderBy('id', 'desc');
        }

        if ($request->has('search') and $request->input('search') != '') {
            $search = $request->input('search');
            $medidas = $medidas->where('nome', 'LIKE', "%$search%");
        }

        $rowsperpage = $request->input('itemsPerPage') > 0 ? $request->input('itemsPerPage') : 30;
        $page = $request->input('page');

        $pagination = $medidas->paginate($rowsperpage, ['*'], 'page', $page);

        return response()->json([
            'total' => $pagination->total(),
            'data' => $pagination->items(),
            'perPage' => $pagination->perPage(),
            'lastpage' => $pagination->lastPage()
        ]);;
    }

    public function store(MedidaRequest $request)
    {
        $input = $request->all();
        $medida = Medida::create($input);
        return response()->json([
            'success' => true,
            'type' => 'success',
            'message' => Helpers::message('MSG001'),
            'id' => $medida['id']
        ]);
    }

    public function show($id)
    {
        $medida = Medida::find($id);

        if ($medida->count())
            return response()->json($medida);

        return response()->json(['error' =>  Helpers::message('MSG000')], 404);
    }

    public function update(MedidaRequest $request, $id)
    {
        $medida = Medida::find($id);
        $medida->update($request->all());
        return response()->json([
            'success' => true,
            'type' => 'success',
            'message' => Helpers::message('MSG002')
        ]);
    }

    public function destroy($id)
    {
        try {
            Medida::destroy($id);

            return response()->json([
                'success' => true,
                'type' => 'success',
                'message' => Helpers::message('MSG003')
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'type' => 'warning',
                'message' => Helpers::message('MSG005'),
                'errors' => $e
            ], 500);
        }
    }

    public function listing()
    {
        return response()->json(Medida::listMedidas());
    }
}
