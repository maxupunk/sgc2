<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\RuaRequest;
use App\Models\Rua;
use App\Helpers\Helpers;

class RuasController extends Controller
{


    public function index(Request $request)
    {
        $ruas = Rua::select(
            'ruas.id',
            'ruas.nome',
            'estados.nome AS estado',
            'cidades.nome AS cidade',
            'bairros.nome AS bairro'
        )
            ->join('bairros', 'bairros.id', '=', 'ruas.bairro_id')
            ->join('cidades', 'cidades.id', '=', 'bairros.cidade_id')
            ->join('estados', 'estados.id', '=', 'cidades.estado_id');

        if ($request->has('sortBy') and $request->input('sortBy') != null) {
            $sortBy = $request->input('sortBy');
            $sortDesc = $request->input('sortDesc');
            $ArraySort = array_combine($sortBy, $sortDesc);
            foreach ($ArraySort as $By => $Desc) {
                $ruas = $ruas->orderBy($By, $Desc == 'false' ? 'desc' : 'asc');
            }
        } else {
            $ruas = $ruas->orderBy('ruas.id', 'desc');
        }

        if ($request->has('search') and $request->input('search') != '') {
            $search = $request->input('search');
            $ruas = $ruas->where('ruas.nome', 'LIKE', "%$search%");
        }

        $rowsperpage = $request->input('itemsPerPage') > 0 ? $request->input('itemsPerPage') : 30;
        $page = $request->input('page');

        $pagination = $ruas->paginate($rowsperpage, ['*'], 'page', $page);

        return response()->json([
            'total' => $pagination->total(),
            'data' => $pagination->items(),
            'perPage' => $pagination->perPage(),
            'lastpage' => $pagination->lastPage()
        ]);;
    }

    public function store(RuaRequest $request)
    {
        try {
            $input = $request->all();
            $rua = Rua::create($input);
            return response()->json([
                'success' => true,
                'type' => 'success',
                'message' => Helpers::message('MSG001'),
                'id' => $rua['id']
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'type' => 'warning',
                'message' => Helpers::message('MSG012'),
                'errors' => $e
            ], 500);
        }
    }

    public function show($id)
    {
        $rua = Rua::join('bairros', 'bairros.id', '=', 'ruas.bairro_id')
            ->join('cidades', 'cidades.id', '=', 'bairros.cidade_id')
            ->join('estados', 'estados.id', '=', 'cidades.estado_id')
            ->select(
                'ruas.id',
                'ruas.nome',
                'estados.id AS estado_id',
                'cidades.id AS cidade_id',
                'bairros.id AS bairro_id'
            )
            ->find($id);

        if ($rua->count()) {
            return response()->json($rua);
        }

        return response()->json(['error' => Helpers::message('MSG000')], 404);
    }

    public function update(RuaRequest $request, $id)
    {
        $rua = Rua::find($id);
        $rua->update($request->all());
        return response()->json([
            'success' => true,
            'type' => 'success',
            'message' => Helpers::message('MSG002')
        ]);
    }

    public function destroy($id)
    {
        try {
            Rua::destroy($id);

            return response()->json([
                'success' => true,
                'type' => 'success',
                'message' => Helpers::message('MSG003')
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'type' => 'warning',
                'message' => Helpers::message('MSG005'),
                'errors' => $e
            ], 500);
        }
    }

    public function listing($rua_id)
    {
        return response()->json(Rua::listruas($rua_id));
    }
}
