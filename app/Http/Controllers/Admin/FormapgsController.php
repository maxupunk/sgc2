<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\FormapgRequest;
use App\Models\Formapg;
use App\Helpers\Helpers;

class FormapgsController extends Controller
{

    public function index(Request $request)
    {
        $formapgs = Formapg::select('*');

        if ($request->has('sortBy') and $request->input('sortBy') != null) {
            $sortBy = $request->input('sortBy');
            $sortDesc = $request->input('sortDesc');
            $ArraySort = array_combine($sortBy, $sortDesc);
            foreach ($ArraySort as $By => $Desc) {
                $formapgs = $formapgs->orderBy($By, $Desc == 'false' ? 'desc' : 'asc');
            }
        } else {
            $formapgs = $formapgs->orderBy('id', 'desc');
        }

        if ($request->has('search') and $request->input('search') != '') {
            $search = $request->input('search');
            $formapgs = $formapgs->where('formapgs.descricao', 'LIKE', "%$search%");
        }

        $rowsperpage = $request->input('itemsPerPage') > 0 ? $request->input('itemsPerPage') : 30;
        $page = $request->input('page');

        $pagination = $formapgs->paginate($rowsperpage, ['*'], 'page', $page);

        return response()->json([
            'total' => $pagination->total(),
            'data' => $pagination->items(),
            'perPage' => $pagination->perPage(),
            'lastpage' => $pagination->lastPage()
        ]);
    }

    public function store(FormapgRequest $request)
    {
        $input = $request->all();
        Formapg::create($input);
        return response()->json([
            'success' => true,
            'type' => 'success',
            'message' => Helpers::message('MSG001')
        ]);
    }

    public function show($id)
    {
        $formapg = Formapg::find($id);

        if ($formapg->count())
            return response()->json($formapg);

        return response()->json(['error' => Helpers::message('MSG000')], 404);
    }

    public function update(FormapgRequest $request, $id)
    {
        $formapg = Formapg::find($id);
        $formapg->update($request->all());
        return response()->json([
            'success' => true,
            'type' => 'success',
            'message' => Helpers::message('MSG002')
        ]);
    }

    public function destroy($id)
    {
        try {
            Formapg::destroy($id);

            return response()->json([
                'success' => true,
                'type' => 'success',
                'message' => Helpers::message('MSG003')
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'type' => 'warning',
                'message' => Helpers::message('MSG005'),
                'errors' => $e
            ], 500);
        }
    }

    public function listing()
    {
        return response()->json(Formapg::listFormapgs());
    }
}
