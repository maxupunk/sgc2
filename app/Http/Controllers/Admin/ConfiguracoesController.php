<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Configuracao;
use App\Helpers\Helpers;

class ConfiguracoesController extends Controller
{
    private $usuario;

    public function __construct()
    {
        $this->usuario = auth()->user('id');
    }


    /*
        Exemplo de estrutura das configurações
        {"NotificacaoVia":["mail"],"Notificacoes":["OrdenNova"]}
    */
    public function index()
    {
        $Configuracao = Configuracao::where('usuario_id', '=', $this->usuario->id)->get();

        if ($Configuracao->count())
            return response()->json($Configuracao);

        return response()->json(['error' => Helpers::message('MSG000')], 404);
    }

    public function update(Request $request)
    {
        try {
            $Configuracao = Configuracao::where('usuario_id', '=', $this->usuario->id);
            $Configuracao->update($request->all());
            return response()->json([
                'success' => true,
                'type' => 'success',
                'message' => Helpers::message('MSG002')
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'type' => 'warning',
                'message' => Helpers::message('MSG005'),
                'errors' => $e
            ], 500);
        }
    }
}
