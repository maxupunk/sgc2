<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Orden;
use App\Helpers\Helpers;
use App\Models\Contato;
use App\Models\Financeiro;
use App\Models\Listaiten;

class OrdensController extends Controller
{

    public function show($id)
    {

        $orden = Orden::getByID($id);

        $orden['itens'] = Listaiten::GetByOrdenID($id)->get();

        $orden['historico'] = $orden->audits()->with('user')->get();

        $orden['contatos'] = Contato::GetID($orden->pessoa_id);

        $orden['financeiro'] = Financeiro::where('orden_id', $id)->get();

        $ordenPai = [];
        if ($orden['orden_id'] !== null) {
            $ordenPaiQuery = Orden::find($id)->pai()->first();
            $ordenPaiQuery['itens'] = Listaiten::GetByOrdenID($ordenPaiQuery['id'])->get();
            $ordenPai[] = $ordenPaiQuery;
        }

        $OrdenPaiId = ($orden['orden_id'] !== null) ? $ordenPaiQuery['id'] : $id;
        $ordenFilhos = Orden::where('orden_id', $OrdenPaiId)->where('id', '<>', $id)->get();
        $ordenFilho = [];
        if (count($ordenFilhos)) {
            foreach ($ordenFilhos as $filho) {
                $filho['itens'] = Listaiten::GetByOrdenID($filho['id'])->get();
                $ordenFilho[] = $filho;
            }
        }

        $orden['estendido'] = array_merge($ordenPai, $ordenFilho);

        if ($orden) {
            return response()->json($orden);
        }

        return response()->json(['error' => Helpers::message('MSG000')], 404);
    }

    public function reabrir($id)
    {

        $orden = Orden::reabrir($id);

        if ($orden and !isset($orden['prestacao'])) {
            return response()->json([
                'success' => true,
                'type' => 'success',
                'message' => Helpers::message('MSG021')
            ]);
        } else {
            if (isset($orden['prestacao'])) {
                return response()->json([
                    'success' => false,
                    'type' => 'warning',
                    'message' => Helpers::message('MSG057'),
                    'prestacao' => $orden['prestacao']
                ]);
            } else {
                return response()->json([
                    'errors' => $orden,
                    'success' => false,
                    'type' => 'warning',
                    'message' => Helpers::message('MSG020')
                ]);
            }
        }
    }

    public function relatorio(Request $request)
    {
        $relatorio = Orden::select(
            'ordens.id AS orden_id',
            'ordens.created_at',
            'ordens.entregue',
            'ordens.status',
            'ordens.tipo',
            'ordens.total',
            'pessoas.nome AS pessoa',
            'usuarios.nome AS usuario',
            'estoques.variante',
            'produtos.descricao',
            'lista_itens.observacao'
        )
            ->leftJoin('lista_itens', 'lista_itens.orden_id', '=', 'ordens.id')
            ->leftJoin('estoques', 'lista_itens.estoque_id', '=', 'estoques.id')
            ->leftJoin('produtos', 'estoques.produto_id', '=', 'produtos.id')
            ->leftJoin('usuarios', 'lista_itens.usuario_id', '=', 'usuarios.id')
            ->leftJoin('pessoas', 'pessoas.id', '=', 'ordens.pessoa_id')
            ->distinct();

        if ($request->has('tipo') and $request->input('tipo') != '') {
            $tipo = $request->input('tipo');
            $relatorio = $relatorio->where('ordens.tipo', '=', $tipo);
        }

        if ($request->has('data_inicial') and $request->input('data_inicial') != '') {
            $data_inicial = Helpers::DataToSQL($request->input('data_inicial'));
            $relatorio = $relatorio->where('ordens.created_at', '>=', $data_inicial);
        }

        if ($request->has('data_final') and $request->input('data_final') != '') {
            $data_final = Helpers::DataToSQL($request->input('data_final'));
            $relatorio = $relatorio->where('ordens.created_at', '<=', $data_final);
        }

        //////////////// TOTAL MINIMO MAXIMO //////////////////
        if ($request->has('total_min') and $request->input('total_min') != '') {
            $total_min = Helpers::RealToSQL($request->input('total_min'));
            $relatorio = $relatorio->where('ordens.total', '>=', $total_min);
        }

        if ($request->has('total_max') and $request->input('total_max') != '' and $request->input('total_max') != 0) {
            $total_max = Helpers::RealToSQL($request->input('total_max'));
            $relatorio = $relatorio->where('ordens.total', '<=', $total_max);
        }

        if ($request->has('defeito') and $request->input('defeito') != '') {
            $defeito = $request->input('defeito');
            $relatorio = $relatorio->where('ordens.defeito', 'LIKE', "%$defeito%");
        }

        if ($request->has('status') and $request->input('status') != '') {
            $status = $request->input('status');
            $relatorio = $relatorio->where('ordens.status', '=', $status);
        }

        if ($request->has('pessoa_id') and $request->input('pessoa_id') != '') {
            $pessoa_id = $request->input('pessoa_id');
            $relatorio = $relatorio->where('ordens.pessoa_id', '=', $pessoa_id);
        }

        if ($request->has('usuario_id') and $request->input('usuario_id') != '') {
            $usuario_id = $request->input('usuario_id');
            $relatorio = $relatorio->where('lista_itens.usuario_id', '=', $usuario_id);
        }

        return response()->json($relatorio->get());
    }
}
