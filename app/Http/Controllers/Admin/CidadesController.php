<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CidadeRequest;
use App\Models\Cidade;
use App\Helpers\Helpers;

class CidadesController extends Controller
{

    public function store(CidadeRequest $request)
    {
        $input = $request->all();
        $cidade = Cidade::create($input);
        return response()->json([
            'success' => true,
            'type' => 'success',
            'message' => Helpers::message('MSG001'),
            'id' => $cidade['id']
        ]);
    }

    public function show($id)
    {
        $cidade = Cidade::find($id);

        if($cidade->count())
            return response()->json($cidade);

        return response()->json(['error' =>  Helpers::message('MSG000')], 404);
    }

    public function update(CidadeRequest $request, $id)
    {
        $cidade = Cidade::find($id);
        $cidade->update($request->all());
        return response()->json([
            'success' => true,
            'type' => 'success',
            'message' => Helpers::message('MSG002')
        ]);
    }

    public function listing($estado_id)
    {
        return response()->json(Cidade::listCidades($estado_id));
    }
}
