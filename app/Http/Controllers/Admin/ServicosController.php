<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\ServicoRequest;
use Illuminate\Support\Facades\DB;
use App\Models\Orden;
use App\Models\Listaiten;
use App\Models\Financeiro;
use Illuminate\Support\Facades\Log;
use App\Models\Usuario;
use App\Notifications\OrdenNovaNotification;
use App\Notifications\OrdenUpdateNotification;
use Illuminate\Support\Facades\Notification;
use App\Helpers\Helpers;
use App\Notifications\OrdenFaturadaNotification;

class ServicosController extends Controller
{

    public function index(Request $request)
    {
        $ordens = Orden::select(
            'ordens.id',
            'ordens.orden_id',
            'ordens.descricao',
            'ordens.entregue',
            'ordens.status',
            'ordens.created_at',
            'pessoas.nome',
            'financeiros.estatus AS financeiro_status'
        )
            ->leftJoin('pessoas', 'pessoas.id', '=', 'ordens.pessoa_id')
            ->leftJoin('financeiros', 'financeiros.orden_id', '=', 'ordens.id')
            ->where('ordens.tipo', '=', 's');

        if ($request->has('search') and $request->input('search') != '') {
            $search = $request->input('search');
            $ordens = $ordens->where('ordens.id', "$search")
                ->orwhere('nome', 'LIKE', "%$search%")
                ->orWhere('ordens.descricao', 'LIKE', "%$search%");
        }

        if ($request->has('status') and $request->input('status') != '') {
            $status = $request->input('status');
            $ordens = $ordens->where('status', $status);
        }

        if ($request->has('pessoa_id') and $request->input('pessoa_id') != '') {
            $pessoa_id = $request->input('pessoa_id');
            $ordens = $ordens->where('ordens.pessoa_id', $pessoa_id);
        }

        if ($request->has('sortBy') and $request->input('sortBy') != null) {
            $sortBy = $request->input('sortBy');
            $sortDesc = $request->input('sortDesc');
            $ArraySort = array_combine($sortBy, $sortDesc);
            foreach ($ArraySort as $By => $Desc) {
                $ordens = $ordens->orderBy($By, $Desc == 'false' ? 'desc' : 'asc');
            }
        } else {
            $ordens = $ordens->orderBy('ordens.id', 'desc');
        }

        $rowsperpage = $request->input('itemsPerPage') > 0 ? $request->input('itemsPerPage') : 30;
        $page = $request->input('page');

        $pagination = $ordens->Paginate($rowsperpage, ['*'], 'page', $page);

        return response()->json([
            'total' => $pagination->total(),
            'data' => $pagination->items(),
            'perPage' => $pagination->perPage(),
            'lastpage' => $pagination->lastPage()
        ]);
    }

    public function store(ServicoRequest $request)
    {
        $input = $request->all();

        $orden = Orden::create($input);
        $usuarios = Usuario::where('ativo', true)->get();
        Notification::send($usuarios, new OrdenNovaNotification($orden));
        return response()->json([
            'success' => true,
            'type' => 'success',
            'message' => Helpers::message('MSG014'),
            'id' => $orden['id']
        ]);
    }

    public function update(ServicoRequest $request)
    {
        if ($request->status !== 'RE') {
            //pega os dados
            $orden = Orden::find($request->id);

            if ($orden->status !== 'RE') {

                DB::beginTransaction();

                // atualiza a os dados da ordem de serviço
                $UpdOrdenDados = $request->only('pessoa_id', 'descricao', 'defeito', 'laudo', 'obs', 'imagens', 'anotacoes', 'acessorios', 'status');
                $orden->update($UpdOrdenDados);

                // adiciona/atualiza os itens na lista da ordem
                $ListaItens = Listaiten::SaveLista($request->itens, $orden->id);
                if ($ListaItens) {
                    DB::commit();
                    $usuarios = Usuario::where('ativo', true)->get();
                    Notification::send($usuarios, new OrdenUpdateNotification($orden));
                    return response()->json([
                        'success' => true,
                        'type' => 'success',
                        'message' => Helpers::message('MSG002')
                    ]);
                } else {
                    DB::rollBack();
                    return response()->json([
                        'success' => false,
                        'type' => 'warning',
                        'message' => Helpers::message('MSG041')
                    ]);
                }
            } else {
                return response()->json([
                    'success' => false,
                    'type' => 'warning',
                    'message' => Helpers::message('MSG018')
                ]);
            }
        } else {
            return response()->json([
                'success' => false,
                'type' => 'warning',
                'message' => Helpers::message('MSG024')
            ]);
        }
    }

    public function faturar(ServicoRequest $request)
    {

        //pega os dados
        $orden = Orden::find($request->id);

        if ($orden->status !== 'RE') {

            $ReceitaTotal = Orden::Total($orden->id);

            // atualiza a os dados da ordem de serviço
            $UpdOrdenDados['status'] = 'RE';
            $UpdOrdenDados['acrescimo'] = Helpers::RealToSQL($request->acrescimo);
            $UpdOrdenDados['desconto'] = Helpers::RealToSQL($request->desconto);
            $UpdOrdenDados['frete'] = Helpers::RealToSQL($request->frete);
            $UpdOrdenDados['obs'] = $request->obs;
            $UpdOrdenDados['documento'] = $request->documento;
            $UpdOrdenDados['parcela_id'] = $request->parcela_id;
            $UpdOrdenDados['entregue'] = date("Y-m-d");
            $UpdOrdenDados['total'] = $ReceitaTotal;

            DB::beginTransaction();

            try {
                $orden->update($UpdOrdenDados);

                // dados para o financeiro
                $DadosFinanceiro['orden_id'] = $orden->id;
                $DadosFinanceiro['pessoa_id'] = $orden->pessoa_id;
                $DadosFinanceiro['nature'] = 'R';
                $DadosFinanceiro['valor'] = $ReceitaTotal;
                $DadosFinanceiro['vencimento'] = $request->vencimento;
                $DadosFinanceiro['datapg'] = null;
                $DadosFinanceiro['status'] = 'AB';

                // tranforma o $valorpg em float se vinher monetario.
                $valorpg = isset($request->valorpg) ? Helpers::RealToSQL($request->valorpg) : null;

                if (isset($request->valorpg)) {
                    $DadosFinanceiro['pago'] = ($valorpg > $ReceitaTotal) ? $ReceitaTotal : $valorpg;
                }

                if (isset($request->pgstatus) and $request->pgstatus) {
                    $DadosFinanceiro['pago'] = $ReceitaTotal;
                }

                Financeiro::AdicionaByOrdenID($DadosFinanceiro);

                // Baixa toda a lista de itens no estoque
                Listaiten::SaidaByOrdenID($orden->id);

                // notificação dos usuarios quer participaram da ordem
                $listaUser_id_Orden = Listaiten::where('orden_id', $orden->id)->pluck('usuario_id');
                $usuarios = Usuario::whereIn('id', $listaUser_id_Orden)->get();
                Notification::send($usuarios, new OrdenFaturadaNotification($orden));

                DB::commit();

                return response()->json([
                    'success' => true,
                    'type' => 'success',
                    'message' => Helpers::message('MSG022')
                ]);
            } catch (\Exception $e) {
                DB::rollBack();
                Log::error($e);
                return response()->json([
                    'type' => 'warning',
                    'message' => Helpers::message('MSG012'),
                    'errors' => $e
                ], 500);
            }
        } else {
            return response()->json([
                'success' => false,
                'type' => 'warning',
                'message' => Helpers::message('MSG023')
            ]);
        }
    }

    public function destroy($id)
    {
        $orden = Orden::find($id);
        if ($orden->status !== "RE" and $orden->status !== "EE") {
            if (Orden::destroy($id)) {
                return response()->json([
                    'success' => true,
                    'type' => 'success',
                    'message' => Helpers::message('MSG003')
                ]);
            } else {
                return response()->json([
                    'success' => false,
                    'type' => 'warning',
                    'message' => Helpers::message('MSG018')
                ]);
            }
        } else {
            return response()->json([
                'success' => false,
                'type' => 'warning',
                'message' => Helpers::message('MSG018')
            ]);
        }
    }

    public function estender(ServicoRequest $request)
    {

        $OrdenEstedida = Orden::where('orden_id', $request['id'])->first();

        if ($OrdenEstedida and $OrdenEstedida['status'] !== 'RE') {
            return response()->json([
                'success' => true,
                'type' => 'success',
                'message' => Helpers::message('MSG058'),
                'orden_id' => $OrdenEstedida['id']
            ]);
        } else {

            $Orden = Orden::find($request['id'])->first();

            if (!$Orden['orden_id']) {
                $OrdenDados['pessoa_id'] = $request['pessoa_id'];
                $OrdenDados['orden_id'] = $request['id'];
                $OrdenDados['tipo'] = "s";
                $OrdenDados['descricao'] = $request['descricao'];
                $OrdenDados['defeito'] = $request['defeito'];
                $OrdenDados['acessorios'] = $request['acessorios'];
                $OrdenDados['status'] = "EA";

                $orden = Orden::create($OrdenDados);
                $usuarios = Usuario::where('ativo', true)->get();
                Notification::send($usuarios, new OrdenNovaNotification($orden));

                return response()->json([
                    'success' => true,
                    'type' => 'success',
                    'message' => Helpers::message('MSG014'),
                    'orden_id' => $orden['id']
                ]);
            } else {
                return response()->json([
                    'success' => false,
                    'type' => 'warning',
                    'message' => Helpers::message('MSG059')
                ]);
            }
        }
    }
}
