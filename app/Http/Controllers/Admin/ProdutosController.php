<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Produto;
use App\Models\Estoque;
use App\Http\Requests\ProdutoRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Helpers\Helpers;
use App\Models\ProdutoCategoria;
use Hamcrest\Type\IsObject;
use Illuminate\Database\QueryException;

class ProdutosController extends Controller
{

    public function index(Request $request)
    {

        $produtos = Produto::leftJoin('medidas', 'medidas.id', '=', 'produtos.medida_id')
            ->leftJoin('marcas', 'marcas.id', '=', 'produtos.marca_id')
            ->leftJoin('estoques', 'estoques.produto_id', '=', 'produtos.id')
            ->select(
                'produtos.id',
                'produtos.descricao',
                'medidas.nome AS medida',
                'marcas.nome AS marca'
            )
            ->distinct();

        if ($request->has('sortBy') and $request->input('sortBy') != null) {
            $sortBy = $request->input('sortBy');
            $sortDesc = $request->input('sortDesc');
            $ArraySort = array_combine($sortBy, $sortDesc);
            foreach ($ArraySort as $By => $Desc) {
                $produtos = $produtos->orderBy($By, $Desc == 'false' ? 'desc' : 'asc');
            }
        } else {
            $produtos = $produtos->orderBy('produtos.id', 'desc');
        }

        if ($request->has('search') and $request->input('search') != '') {

            $search = $request->input('search');

            $searchValues = preg_split('/\s+/', $search, -1, PREG_SPLIT_NO_EMPTY);
            $produtos = $produtos->where('estoques.codigo', $search)
                ->OrWhere(
                    function ($q) use ($searchValues) {
                        foreach ($searchValues as $value) {
                            $q->Where('produtos.descricao', 'like', "%{$value}%")
                                ->orWhere('marcas.nome', $value);
                        }
                    }
                )
                ->OrWhere(
                    function ($q) use ($searchValues) {
                        foreach ($searchValues as $value) {
                            $q->Where('produtos.caracteristica', 'like', "%{$value}%");
                        }
                    }
                );
        }

        $rowsperpage = $request->input('itemsPerPage') > 0 ? $request->input('itemsPerPage') : 30;
        $page = $request->input('page');

        $pagination = $produtos->Paginate($rowsperpage, ['*'], 'page', $page);

        return response()->json([
            'total' => $pagination->total(),
            'data' => $pagination->items(),
            'perPage' => $pagination->perPage(),
            'lastpage' => $pagination->lastPage()
        ]);
    }

    public function store(ProdutoRequest $request)
    {
        $input = $request->all();
        DB::beginTransaction();
        try {

            $produto = Produto::create($input);

            Estoque::StoreEstoque($input['estoques'], $produto['id']);
            ProdutoCategoria::gravar($input['categorias'], $produto['id']);

            DB::commit();
            return response()->json([
                'success' => true,
                'type' => 'success',
                'message' => Helpers::message('MSG001')
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);
            return response()->json([
                'type' => 'warning',
                'message' => Helpers::message('MSG012'),
                'errors' => $e
            ], 500);
        }
    }

    public function show($id)
    {
        $produto = Produto::LeftJoin('medidas', 'medidas.id', '=', 'produtos.medida_id')
            ->LeftJoin('marcas', 'marcas.id', '=', 'produtos.marca_id')
            ->select(
                'produtos.*',
                'medidas.id AS medida_id',
                'medidas.nome AS medida',
                'marcas.id AS marca_id',
                'marcas.nome AS marca'
            )
            ->find($id);
        $produto['categorias'] = ProdutoCategoria::GetID($id);
        $produto['estoques'] = Estoque::where('produto_id', $id)->get();

        return response()->json($produto);
    }

    public function update(ProdutoRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $produto = Produto::find($id);
            $produto->update($request->all());

            Estoque::StoreEstoque($request['estoques'], $produto['id']);
            ProdutoCategoria::gravar($request['categorias'], $produto['id']);

            DB::commit();
            return response()->json([
                'success' => true,
                'type' => 'success',
                'message' => Helpers::message('MSG002')
            ]);
        } catch (QueryException $e) {
            DB::rollBack();
            return response()->json([
                'success' => false,
                'type' => 'warning',
                'message' => Helpers::message('MSG012'),
                'errors' => $e
            ], 500);
        }
    }

    public function destroy($id)
    {
        try {
            Produto::destroy($id);

            return response()->json([
                'success' => true,
                'type' => 'success',
                'message' => Helpers::message('MSG003')
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'type' => 'warning',
                'message' => Helpers::message('MSG005'),
                'errors' => $e
            ], 500);
        }
    }

    public function search($search)
    {
        $termos = preg_split('/\s+/', $search, -1, PREG_SPLIT_NO_EMPTY);

        $produtos = Produto::LeftJoin('medidas', 'medidas.id', '=', 'produtos.medida_id')
            ->LeftJoin('marcas', 'marcas.id', '=', 'produtos.marca_id')
            ->LeftJoin('estoques', 'estoques.produto_id', '=', 'produtos.id')
            ->select(
                'produtos.descricao',
                'produtos.tipo',
                //'produtos.caracteristica',
                'estoques.id',
                'estoques.valor',
                'estoques.codigo',
                'estoques.variante',
                'estoques.quant',
                'estoques.comissao',
                'estoques.tasklist',
                'medidas.sigla AS medida',
                'marcas.nome AS marca'
            )
            ->where('estoques.codigo', $search)
            ->OrWhere(
                function ($q) use ($termos) {
                    foreach ($termos as $termo) {
                        if (strlen($termo) >= 3) {
                            $q->where('marcas.nome', '=', $termo);
                            $q->OrWhere('produtos.descricao', 'like', "%{$termo}%");
                            $q->OrWhere('produtos.caracteristica', 'like', "%{$termo}%");
                            $q->OrWhere('estoques.variante', 'like', "{$termo}%");
                        }
                    }
                }
            )
            // estoque zero deve aparecer para hora da compra e aparecer serviços
            //->where('estoques.quant', '>', 0)
            ->where(function ($query) {
                $query->where('estoques.quant', '>', 0)
                    ->OrWhere('produtos.tipo', '=', 's');
            })
            ->where('estoques.ativo', true)
            ->limit(10)
            ->get();

        foreach ($produtos as $produto) {
            $relevancia = 0;

            foreach ($termos as $palavra) {
                $relevancia += stripos($produto->marca, $palavra) !== false;
                $relevancia += stripos($produto->descricao, $palavra) !== false;
                $relevancia += stripos($produto->variante, $palavra) !== false;
            }

            $produto->relevancia = $relevancia;
        }

        $produtosArray = $produtos->ToArray();
        usort(
            $produtosArray,
            function ($a, $b) {
                //if ($a['relevancia'] == $b['relevancia']) return 0;
                return (($a['relevancia'] > $b['relevancia']) ? -1 : 1);
            }
        );

        return response()->json($produtosArray);
    }
}
