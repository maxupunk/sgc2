<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Acao;

class AcoesController extends Controller
{
    public  function listing()
    {
        return response()->json(Acao::listAcoes());
    }

    public  function metodos()
    {
        return response()->json(Acao::listMetodo());
    }
}
