<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CompraRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Orden;
use App\Models\Listaiten;
use App\Models\Financeiro;
use Illuminate\Support\Facades\Log;
use App\Helpers\Helpers;

class ComprasController extends Controller
{
    public function index(Request $request)
    {
        $ordens = Orden::leftJoin('pessoas', 'pessoas.id', '=', 'ordens.pessoa_id')
            ->select(
                'ordens.id',
                'ordens.tipo',
                'ordens.status',
                'pessoas.nome',
                'ordens.created_at'
            )
            ->where('ordens.tipo', '=', 'c');

        if ($request->has('search') and $request->input('search') != '') {
            $search = $request->input('search');
            $ordens = $ordens->where('pessoas.nome', 'LIKE', "%" . $search . "%");
        }

        if ($request->has('sortBy') and $request->input('sortBy') != null) {
            $sortBy = $request->input('sortBy');
            $sortDesc = $request->input('sortDesc');
            $ArraySort = array_combine($sortBy, $sortDesc);
            foreach ($ArraySort as $By => $Desc) {
                $ordens = $ordens->orderBy($By, $Desc == 'false' ? 'desc' : 'asc');
            }
        } else {
            $ordens = $ordens->orderBy('ordens.id', 'desc');
        }

        $rowsperpage = $request->input('itemsPerPage') > 0 ? $request->input('itemsPerPage') : 30;
        $page = $request->input('page');

        $pagination = $ordens->Paginate($rowsperpage, ['*'], 'page', $page);

        return response()->json([
            'total' => $pagination->total(),
            'data' => $pagination->items(),
            'perPage' => $pagination->perPage(),
            'lastpage' => $pagination->lastPage()
        ]);
    }

    public function store(CompraRequest $request)
    {
        $input = $request->all();
        DB::beginTransaction();
        try {
            $orden = Orden::create($input);
            if ($orden['id']) {
                // verifica se existe item para gravar
                Listaiten::SaveLista($request->itens, $orden->id);

                DB::commit();
                return response()->json([
                    'success' => true,
                    'type' => 'success',
                    'message' => Helpers::message('MSG001'),
                    'id' => $orden['id']
                ]);
            }
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);
            return response()->json([
                'type' => 'warning',
                'message' => Helpers::message('MSG012'),
                'errors' => $e
            ], 500);
        }
    }

    public function update(Request $request, $id)
    {
        if ($request->status !== 'RE') {
            //pega os dados
            $orden = Orden::find($request->id);

            if ($orden->status !== 'RE') {

                DB::beginTransaction();

                try {
                    $UpdOrdenDados['pessoa_id'] = $request->pessoa_id;
                    $UpdOrdenDados['acrescimo'] = $request->acrescimo;
                    $UpdOrdenDados['desconto'] = $request->desconto;
                    $UpdOrdenDados['frete'] = $request->frete;
                    $UpdOrdenDados['documento'] = $request->documento;
                    $UpdOrdenDados['parcela_id'] = $request->parcela_id;
                    $UpdOrdenDados['anotacoes'] = $request->anotacoes;
                    $UpdOrdenDados['imagens'] = $request->imagens;

                    $UpdOrden = $orden->update($UpdOrdenDados);

                    // Se não tiver itens para gravar ele para aqui
                    if ($UpdOrden and count($request->itens) == 0) {
                        DB::commit();
                        return response()->json([
                            'success' => true,
                            'type' => 'success',
                            'message' => Helpers::message('MSG002')
                        ]);
                    }

                    Listaiten::SaveLista($request->itens, $orden->id);

                    DB::commit();
                    return response()->json([
                        'success' => true,
                        'type' => 'success',
                        'message' => Helpers::message('MSG002'),
                        'id' => $request->id
                    ]);
                } catch (\Exception $e) {
                    DB::rollBack();
                    Log::error($e);
                    return response()->json([
                        'type' => 'warning',
                        'message' => Helpers::message('MSG012'),
                        'errors' => $e
                    ], 500);
                }
            } else {
                return response()->json([
                    'success' => false,
                    'type' => 'warning',
                    'message' => Helpers::message('MSG018')
                ]);
            }
        } else {
            return response()->json([
                'success' => false,
                'type' => 'warning',
                'message' => Helpers::message('MSG024')
            ]);
        }
    }

    // PATCH leva a faturar
    public function faturar(CompraRequest $request)
    {

        DB::beginTransaction();

        $UpdOrdenDados['pessoa_id'] = $request->pessoa_id;
        $UpdOrdenDados['acrescimo'] = Helpers::RealToSQL($request->acrescimo);
        $UpdOrdenDados['desconto'] = Helpers::RealToSQL($request->desconto);
        $UpdOrdenDados['frete'] = Helpers::RealToSQL($request->frete);
        $UpdOrdenDados['documento'] = $request->documento;
        $UpdOrdenDados['parcela_id'] = $request->parcela_id;
        $UpdOrdenDados['anotacoes'] = $request->anotacoes;
        $UpdOrdenDados['imagens'] = $request->imagens;
        $UpdOrdenDados['tipo'] = 'c';
        $UpdOrdenDados['status'] = 'RE';

        try {

            if ($request->id) {
                $orden = Orden::find($request->id);

                if ($orden->status === 'RE') {
                    $ordenReabrir = Orden::reabrir($request->id);
                    if ($ordenReabrir !== true) {
                        return response()->json([
                            'errors' => $ordenReabrir,
                            'success' => false,
                            'type' => 'warning',
                            'message' => Helpers::message('MSG020')
                        ], 500);
                    }
                }
                // tem que existe esse find para haver um update apos reabrir
                Orden::find($request->id)->update($UpdOrdenDados);
            } else {
                $orden = Orden::create($UpdOrdenDados);
            }


            Listaiten::SaveLista($request->itens, $orden['id'], 1);
            Listaiten::EntradaByOrdenID($orden->id);

            $ordenTotal = Orden::Total($orden['id']);

            // tranforma o $valorpg em float se vinher monetario.
            $valorpg = isset($request->valorpg) ? Helpers::RealToSQL($request->valorpg) : null;

            $DadosFinanceiro['orden_id'] = $orden['id'];
            $DadosFinanceiro['nature'] = 'D';
            $DadosFinanceiro['valor'] = $ordenTotal;
            $DadosFinanceiro['vencimento'] = $request->vencimento;
            $DadosFinanceiro['datapg'] = null;
            $DadosFinanceiro['status'] = 'AB';

            if (isset($request->valorpg)) {
                $DadosFinanceiro['pago'] = ($valorpg > $ordenTotal) ? $ordenTotal : $valorpg;
            }

            if (isset($request->pessoa_id)) {
                $DadosFinanceiro['pessoa_id'] = $request->pessoa_id;
            }

            if (isset($request->pgstatus) and $request->pgstatus) {
                $DadosFinanceiro['pago'] = $ordenTotal;
            }

            Financeiro::AdicionaByOrdenID($DadosFinanceiro);

            DB::commit();
            return response()->json([
                'success' => true,
                'type' => 'success',
                'message' => Helpers::message('MSG011')
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);
            return response()->json([
                'type' => 'warning',
                'message' => Helpers::message('MSG012'),
                'errors' => $e
            ], 500);
        }
    }

    public function destroy($id)
    {
        $orden = Orden::find($id);
        if ($orden->status !== "RE" and $orden->status !== "EE") {
            if (Orden::destroy($id)) {
                return response()->json([
                    'success' => true,
                    'type' => 'success',
                    'message' => Helpers::message('MSG003')
                ]);
            } else {
                return response()->json([
                    'success' => false,
                    'type' => 'warning',
                    'message' => Helpers::message('MSG018')
                ]);
            }
        } else {
            return response()->json([
                'success' => false,
                'type' => 'warning',
                'message' => Helpers::message('MSG018')
            ]);
        }
    }
}
