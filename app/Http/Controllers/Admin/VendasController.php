<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\VendaRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Orden;
use App\Models\Listaiten;
use App\Models\Financeiro;
use Illuminate\Support\Facades\Log;
use App\Helpers\Helpers;
use Illuminate\Database\QueryException;

class VendasController extends Controller
{

    public function index(Request $request)
    {
        $ordens = Orden::leftJoin('pessoas', 'pessoas.id', '=', 'ordens.pessoa_id')
            ->select(
                'ordens.id',
                'ordens.tipo',
                'ordens.status',
                'pessoas.nome',
                'ordens.created_at'
            )
            ->where('ordens.tipo', '=', "v");

        if ($request->has('sortBy') and $request->input('sortBy') != null) {
            $sortBy = $request->input('sortBy');
            $sortDesc = $request->input('sortDesc');
            $ArraySort = array_combine($sortBy, $sortDesc);
            foreach ($ArraySort as $By => $Desc) {
                $ordens = $ordens->orderBy($By, $Desc == 'false' ? 'desc' : 'asc');
            }
        } else {
            $ordens = $ordens->orderBy('ordens.id', 'desc');
        }

        if ($request->has('search') and $request->input('search') != '') {
            $search = $request->input('search');
            $ordens = $ordens->where('pessoas.nome', 'LIKE', "%" . $search . "%");
        }

        $rowsperpage = $request->input('itemsPerPage') > 0 ? $request->input('itemsPerPage') : 30;
        $page = $request->input('page');

        $pagination = $ordens->Paginate($rowsperpage, ['*'], 'page', $page);

        return response()->json([
            'total' => $pagination->total(),
            'data' => $pagination->items(),
            'perPage' => $pagination->perPage(),
            'lastpage' => $pagination->lastPage()
        ]);;
    }

    public function store(VendaRequest $request)
    {

        $UpdOrdenDados['pessoa_id'] = $request->pessoa_id;
        $UpdOrdenDados['acrescimo'] = Helpers::RealToSQL($request->acrescimo);
        $UpdOrdenDados['desconto'] = Helpers::RealToSQL($request->desconto);
        $UpdOrdenDados['frete'] = Helpers::RealToSQL($request->frete);
        $UpdOrdenDados['documento'] = $request->documento;
        $UpdOrdenDados['parcela_id'] = $request->parcela_id;
        $UpdOrdenDados['anotacoes'] = $request->anotacoes;
        $UpdOrdenDados['imagens'] = $request->imagens;
        $UpdOrdenDados['tipo'] = Orden::tipo['venda'];
        $UpdOrdenDados['entregue'] = Carbon::now();
        $UpdOrdenDados['status'] = 'RE';

        //dd($UpdOrdenDados);

        DB::beginTransaction();

        try {

            if ($request->id) {
                $orden = Orden::find($request->id);
                if ($orden->status === 'RE') {
                    $ordenReabrir = Orden::reabrir($request->id);
                    if (!$ordenReabrir) {
                        return response()->json([
                            'errors' => $ordenReabrir,
                            'success' => false,
                            'type' => 'warning',
                            'message' => Helpers::message('MSG020')
                        ]);
                    } else if (isset($ordenReabrir['prestacao'])) {
                        return response()->json([
                            'success' => false,
                            'type' => 'warning',
                            'message' => Helpers::message('MSG057'),
                            'prestacao' => $ordenReabrir['prestacao']
                        ]);
                    }
                }
                Orden::find($request->id)->update($UpdOrdenDados);
            } else {
                $orden = Orden::create($UpdOrdenDados);
                if (!$orden) {
                    return response()->json([
                        'success' => false,
                        'type' => 'warning',
                        'message' => Helpers::message('MSG012')
                    ], 500);
                }
            }

            $ListaItens = Listaiten::SaveLista($request->itens, $orden['id']);

            if ($ListaItens) {

                Listaiten::SaidaByOrdenID($orden['id']);

                $ordenTotal = Orden::Total($orden['id']);

                // tranforma o $valorpg em float se vinher monetario.
                $valorpg = isset($request->valorpg) ? Helpers::RealToSQL($request->valorpg) : null;

                // dados para o financeiro
                $DadosFinanceiro['orden_id'] = $orden['id'];
                $DadosFinanceiro['nature'] = 'R';
                $DadosFinanceiro['valor'] = $ordenTotal;
                $DadosFinanceiro['vencimento'] = $request->vencimento;
                $DadosFinanceiro['datapg'] = null;
                $DadosFinanceiro['status'] = 'AB';

                if (isset($request->valorpg)) {
                    $DadosFinanceiro['pago'] = ($valorpg > $ordenTotal) ? $ordenTotal : $valorpg;
                }

                if (isset($request->pessoa_id)) {
                    $DadosFinanceiro['pessoa_id'] = $request->pessoa_id;
                }

                if (isset($request->pgstatus) and $request->pgstatus) {
                    $DadosFinanceiro['pago'] = $ordenTotal;
                }

                Financeiro::AdicionaByOrdenID($DadosFinanceiro);

                DB::commit();
                return response()->json([
                    'success' => true,
                    'type' => 'success',
                    'message' => Helpers::message('MSG011')
                ]);
            } else {
                DB::rollBack();
                return response()->json([
                    'errors' => $ListaItens,
                    'type' => 'warning',
                    'message' => Helpers::message('MSG020')
                ], 500);
            }
        } catch (QueryException $e) {
            DB::rollBack();
            Log::error($e);
            return response()->json([
                'type' => 'warning',
                'message' => Helpers::message('MSG012'),
                'errors' => $e
            ], 500);
        }
    }

    public function destroy($id)
    {
        $orden = Orden::find($id);
        if ($orden->status !== "RE" and $orden->status !== "EE") {
            if (Orden::destroy($id)) {
                return response()->json([
                    'success' => true,
                    'type' => 'success',
                    'message' => Helpers::message('MSG003')
                ]);
            } else {
                return response()->json([
                    'success' => false,
                    'type' => 'warning',
                    'message' => Helpers::message('MSG018')
                ]);
            }
        } else {
            return response()->json([
                'success' => false,
                'type' => 'warning',
                'message' => Helpers::message('MSG018')
            ]);
        }
    }
}
