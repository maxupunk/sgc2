<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Contato;
use App\Models\Pessoa;
use Illuminate\Http\Request;
use App\Http\Requests\PessoaRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Helpers\Helpers;
use Illuminate\Support\Facades\Cache;

class PessoasController extends Controller
{
    public function index(Request $request)
    {
        $pessoas = Pessoa::select('*');

        if ($request->has('sortBy') and $request->input('sortBy') != null) {
            $sortBy = $request->input('sortBy');
            $sortDesc = $request->input('sortDesc');
            $ArraySort = array_combine($sortBy, $sortDesc);
            foreach ($ArraySort as $By => $Desc) {
                $pessoas = $pessoas->orderBy($By, $Desc == 'false' ? 'desc' : 'asc');
            }
        } else {
            $pessoas = $pessoas->orderBy('id', 'desc');
        }

        if ($request->has('search') and $request->input('search') != '') {
            $search = $request->input('search');

            $searchValues = preg_split('/\s+/', $search, -1, PREG_SPLIT_NO_EMPTY);
            $pessoas = $pessoas->Where('documento', '=', $search)
                ->OrWhere(
                    function ($q) use ($searchValues) {
                        foreach ($searchValues as $value) {
                            $q->Where('nome', 'like', "%{$value}%");
                        }
                    }
                );
        }

        $rowsperpage = $request->input('itemsPerPage') > 0 ? $request->input('itemsPerPage') : 30;
        $page = $request->input('page');

        $pagination = $pessoas->Paginate($rowsperpage, ['*'], 'page', $page);

        return response()->json([
            'total' => $pagination->total(),
            'data' => $pagination->items(),
            'perPage' => $pagination->perPage(),
            'lastPage' => $pagination->lastPage()
        ]);
    }

    public function store(PessoaRequest $request)
    {
        $input = $request->all();
        DB::beginTransaction();
        try {
            $pessoa = Pessoa::create($input);
            Contato::gravar($request->contatos, $pessoa->id);
            DB::commit();
            return response()->json([
                'success' => true,
                'type' => 'success',
                'message' => Helpers::message('MSG001'),
                'data' => $pessoa
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);
            return response()->json([
                'type' => 'warning',
                'message' => Helpers::message('MSG012'),
                'errors' => $e
            ], 500);
        }
    }

    public function show($id)
    {
        $pessoa = Pessoa::join('ruas', 'ruas.id', '=', 'pessoas.rua_id')
            ->join('bairros', 'bairros.id', '=', 'ruas.bairro_id')
            ->join('cidades', 'cidades.id', '=', 'bairros.cidade_id')
            ->join('estados', 'estados.id', '=', 'cidades.estado_id')
            ->select(
                'pessoas.*',
                'estados.id AS estado_id',
                'cidades.id AS cidade_id',
                'bairros.id AS bairro_id',
                'ruas.id AS rua_id'
            )
            ->find($id);

        $pessoa['contatos'] = Contato::GetID($id);
        $pessoa['tokens'] = $pessoa->tokens()->get();

        if ($pessoa->count())
            return response()->json($pessoa);

        return response()->json(['error' => Helpers::message('MSG000')], 404);
    }

    public function update(PessoaRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $pessoa = Pessoa::find($id);
            if ($pessoa) {
                $pessoa->update($request->all());
                Contato::gravar($request->contatos, $id);
                DB::commit();
                return response()->json([
                    'success' => true,
                    'type' => 'success',
                    'message' => Helpers::message('MSG002')
                ]);
            }
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);
            return response()->json([
                'type' => 'warning',
                'message' => Helpers::message('MSG012'),
                'errors' => $e
            ], 500);
        }
    }

    public function destroy($id)
    {
        try {
            Pessoa::destroy($id);

            return response()->json([
                'success' => true,
                'type' => 'success',
                'message' => Helpers::message('MSG003')
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'type' => 'warning',
                'message' => Helpers::message('MSG005'),
                'errors' => $e
            ], 500);
        }
    }

    public function deletaToken($id, $tokenId)
    {
        $user = Pessoa::find($id);
        $user->tokens()->where('id', $tokenId)->delete();
        if ($user) {
            return response()->json([
                'success' => true,
                'type' => 'success',
                'message' => Helpers::message('MSG060')
            ]);
        } else {
            return response()->json([
                'success' => false,
                'type' => 'warning',
                'message' => Helpers::message('MSG020'),
            ], 500);
        }
    }

    public function link($id)
    {
        $chave = sha1(uniqid(time(), true));
        $cache = Cache::put($chave, $id, now()->addMinutes(60));
        if ($cache) {
            return response()->json([
                'success' => true,
                'chave' => $chave
            ]);
        } else {
            return response()->json([
                'success' => false,
                'type' => 'warning',
                'message' => Helpers::message('MSG020'),
            ], 500);
        }
    }

    public function search($search)
    {
        $searchValues = preg_split('/\s+/', $search, -1, PREG_SPLIT_NO_EMPTY);
        $pessoa = Pessoa::select('pessoas.id', 'pessoas.nome', 'pessoas.tipo')
            ->where('pessoas.ativo', true)
            ->where('documento', '=', $search)
            ->OrWhere(
                function ($q) use ($searchValues) {
                    foreach ($searchValues as $value) {
                        $q->Where('nome', 'like', "%{$value}%");
                    }
                }
            )
            ->get();

        return response()->json($pessoa);
    }
}
