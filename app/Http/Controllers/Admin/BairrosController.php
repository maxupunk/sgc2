<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\BairroRequest;
use Illuminate\Http\Request;
use App\Models\Bairro;
use Illuminate\Support\Facades\Log;
use App\Helpers\Helpers;

class BairrosController extends Controller
{

    public function index(Request $request)
    {
        $bairros = Bairro::select(
            'bairros.id',
            'bairros.nome',
            'estados.nome AS estado',
            'cidades.nome AS cidade'
        )
            ->join('cidades', 'cidades.id', '=', 'bairros.cidade_id')
            ->join('estados', 'estados.id', '=', 'cidades.estado_id');

        if ($request->has('sortBy') and $request->input('sortBy') != null) {
            $sortBy = $request->input('sortBy');
            $sortDesc = $request->input('sortDesc');
            $ArraySort = array_combine($sortBy, $sortDesc);
            foreach ($ArraySort as $By => $Desc) {
                $bairros = $bairros->orderBy($By, $Desc == 'false' ? 'desc' : 'asc');
            }
        } else {
            $bairros = $bairros->orderBy('bairros.id', 'desc');
        }

        if ($request->has('search') and $request->input('search') != '') {
            $search = $request->input('search');
            $bairros = $bairros->where('bairros.nome', 'LIKE', "%$search%");
        }

        $rowsperpage = $request->input('itemsPerPage') > 0 ? $request->input('itemsPerPage') : 30;
        $page = $request->input('page');

        $pagination = $bairros->paginate($rowsperpage, ['*'], 'page', $page);

        return response()->json([
            'total' => $pagination->total(),
            'data' => $pagination->items(),
            'perPage' => $pagination->perPage(),
            'lastpage' => $pagination->lastPage()
        ]);
    }

    public function store(BairroRequest $request)
    {
        try {
            $input = $request->all();
            $bairro = Bairro::create($input);
            return response()->json([
                'success' => true,
                'type' => 'success',
                'message' => Helpers::message('MSG001'),
                'id' => $bairro['id']
            ]);
        } catch (\Exception $e) {
            Log::error($e);
            return response()->json([
                'success' => false,
                'type' => 'warning',
                'message' => Helpers::message('MSG012'),
                'errors' => $e
            ], 500);
        }
    }

    public function show($id)
    {
        $bairro = Bairro::join('cidades', 'cidades.id', '=', 'bairros.cidade_id')
            ->join('estados', 'estados.id', '=', 'cidades.estado_id')
            ->select(
                'bairros.id',
                'bairros.nome',
                'estados.id AS estado_id',
                'cidades.id AS cidade_id'
            )
            ->find($id);

        if ($bairro->count())
            return response()->json($bairro);

        return response()->json(['error' => Helpers::message('MSG000')], 404);
    }

    public function update(BairroRequest $request, $id)
    {
        $bairro = Bairro::find($id);
        $bairro->update($request->all());
        return response()->json([
            'success' => true,
            'type' => 'success',
            'message' => Helpers::message('MSG002')
        ]);
    }

    public function destroy($id)
    {
        try {
            Bairro::destroy($id);

            return response()->json([
                'success' => true,
                'type' => 'success',
                'message' => Helpers::message('MSG003')
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'type' => 'warning',
                'message' => Helpers::message('MSG005')
            ], 500);
        }
    }

    public function listing($cidade_id)
    {
        return response()->json(Bairro::listBairros($cidade_id));
    }
}
