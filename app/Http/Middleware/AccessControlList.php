<?php

namespace App\Http\Middleware;

use Closure;
use App\Helpers\Helpers;

class AccessControlList
{
    public function handle($request, Closure $next)
    {
       
        $route = $request->route()->getName();
        if (!$request->user()->tokenCan($route)) {
            return response()->json([
                'success' => false,
                'type' => 'warning',
                'message' => Helpers::message('MSG009'),
            ], 401);
        }

        return $next($request);
    }
}
