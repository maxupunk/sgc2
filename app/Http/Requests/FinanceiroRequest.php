<?php

namespace App\Http\Requests;

use App\Helpers\Helpers;
use App\Models\Financeiro;

class FinanceiroRequest extends Request
{

    public function rules()
    {

        if ($this->route()->getName() == "financeiros.pessoa.baixar" || $this->route()->getName() == "financeiros.baixar") {
            $rules = [
                'valorpago' => "required|numeric|min:0.01",
                'nature' => 'required',
                'estatus' => 'required',
            ];
        } else {
            $rules = [
                'descricao' => 'max:150',
                'nature' => 'required',
                'valor' => 'required',
                'estatus' => 'required',
            ];

            if ($this['pessoa_id'] == null and $this['estatus'] !== 'pg') {
                $rules['pessoa_id'] = 'required';
                $rules['vencimento'] = 'required';
            }

            if ($this['pessoa_id'] == null) {
                $rules['descricao'] = 'required|max:150';
            }

            /* if ($this->isMethod('PUT')) {
                $financeiro = Financeiro::where('id', $this['id'])->first();
                if ($financeiro->valor != $this['valor'] && $financeiro->pago != $this['pago']) {
                    $rules['mensagem'] = 'required';
                }
            } */
        }

        return $rules;
    }

    public function messages()
    {
        $mensagem['pessoa_id.required'] = Helpers::message('MSG013');
        $mensagem['mensagem.required'] = Helpers::message('MSG051');
        return $mensagem;
    }

    public function attributes()
    {
        $atributo['nature'] = Helpers::message('MSG034');
        return $atributo;
    }
}
