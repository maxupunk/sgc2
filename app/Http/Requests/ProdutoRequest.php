<?php

namespace App\Http\Requests;

use App\Helpers\Helpers;
use App\Models\Produto;
use Illuminate\Validation\Rule;

class ProdutoRequest extends Request
{
    public function rules()
    {
        $descricao = $this->descricao;
        $marca_id = $this->marca_id;

        $rules = [
            'descricao' => [
                'required',
                Rule::unique('produtos')->where(function ($query) use ($descricao, $marca_id) {
                    return $query->where('descricao', $descricao)->where('marca_id', $marca_id);
                })->ignore($this->id)
            ],
            'tipo' => 'required',
            'categorias' => 'required',
            'ativo' => 'boolean',
            'estoques' => 'required|array|min:1',
            'peso' => 'nullable|numeric',
        ];

        if ($this->estoques !== null) {
            if ($this->tipo == 'p') {
                $rules['estoques.*.quant'] = 'required';
            }
            $rules['estoques.*.valor'] = 'required';
        }

        if ($this->icms_id !== null) {
            $rules['icms'] = 'required';
        }

        if ($this->ipi_id !== null) {
            $rules['ipi'] = 'required';
        }

        if ($this->pis_id !== null) {
            $rules['pis'] = 'required';
        }

        if ($this->cofins_id !== null) {
            $rules['cofins'] = 'required';
        }

        if ($this->tipo !== 's') {
            $rules['marca_id'] = 'required';
            $rules['medida_id'] = 'required';
        }

        return $rules;
    }

    public function attributes()
    {
        $atributo['categoria_id'] = Helpers::message('MSG036');
        $atributo['medida_id'] = Helpers::message('MSG037');
        $atributo['marca_id'] = Helpers::message('MSG038');
        $atributo['quant'] = Helpers::message('MSG039');
        return $atributo;
    }

    public function messages()
    {
        $mensagem['estoques.required'] = Helpers::message('MSG039');
        $mensagem['descricao.unique'] = Helpers::message('MSG056');
        return $mensagem;
    }
}
