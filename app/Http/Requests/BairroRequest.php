<?php

namespace App\Http\Requests;

class BairroRequest extends Request
{
    public function rules()
    {
        $rules = [
            'cidade_id' => 'required'
        ];

        return $rules;
    }
}
