<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;

class TagRequest extends Request
{
    public function rules()
    {
        $rules = [
            'nome' => [
                'required',
                'min:2',
                'max:60',
                Rule::unique('tags')->ignore($this->id),
            ]
        ];

        return $rules;
    }
}
