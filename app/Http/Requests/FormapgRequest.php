<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;

class FormapgRequest extends Request
{
    public function rules()
    {
        $rules = [
            'descricao' => [
                'required',
                'max:20',
                Rule::unique('formapgs')->ignore($this->id),
            ],
            'custo' => 'required|numeric',
            'ativo' => 'required|boolean'
        ];

        return $rules;
    }
}
