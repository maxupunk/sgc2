<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use App\Helpers\Helpers;

class RuaRequest extends Request
{
    public function rules()
    {
        $bairro_id = $this->bairro_id;
        $rules = [
            'nome' => [
                'required',
                'min:2',
                'max:60',
                Rule::unique('ruas')->where(function ($query) use ($bairro_id) {
                    return $query->where('bairro_id', $bairro_id);
                })
                //Rule::unique('ruas')->ignore($this->id),
            ],
            'bairro_id' => 'required'
        ];

        return $rules;
    }

    public function attributes()
    {
        $atributo['bairro_id'] = Helpers::message('MSG032');
        return $atributo;
    }
}
