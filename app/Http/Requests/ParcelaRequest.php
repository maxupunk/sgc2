<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use App\Helpers\Helpers;

class ParcelaRequest extends Request
{
    public function rules()
    {
        $rules = [
            'nparcela' => 'required|numeric',
            'juros' => 'required|numeric'
        ];
        return $rules;
    }

    public function attributes()
    {
        $atributo['nparcela'] = Helpers::message('MSG033');
        return $atributo;
    }
}
