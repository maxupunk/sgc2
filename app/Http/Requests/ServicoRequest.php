<?php

namespace App\Http\Requests;

use App\Helpers\Helpers;
use App\Models\Orden;

class ServicoRequest extends Request
{

    protected function prepareForValidation()
    {
        $this['tipo'] = Orden::tipo['servico'];
    }

    public function rules()
    {
        $rules = [];

        if ($this['status'] == 'RE') {
            $rules['status'] = 'required';
        }

        if ($this->method() == 'POST') {
            $rules = [
                'pessoa_id' => 'required',
                'descricao' => 'required',
                'defeito' => 'required'
            ];
        }

        if ($this->route()->getName() == "servicos.faturar") {
            $rules = [
                'formapg_id' => 'required',
                'parcela_id' => 'required'
            ];
        }

        return $rules;
    }

    public function messages()
    {
        $mensagem['status.required'] = Helpers::message('MSG035');
        $mensagem['descricao.required'] = Helpers::message('MSG025');
        $mensagem['pessoa_id.required'] = Helpers::message('MSG030');
        return $mensagem;
    }

    public function attributes()
    {
        $atributo['formapg_id'] = Helpers::message('MSG028');
        $atributo['parcela_id'] = Helpers::message('MSG029');
        return $atributo;
    }
}
