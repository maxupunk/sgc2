<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;

class MedidaRequest extends Request
{
    public function rules()
    {
        $rules = [
            'nome' => [
                'required',
                'min:2',
                'max:60',
                Rule::unique('medidas')->ignore($this->id),
            ],
            'sigla' => 'required|min:2|max:2'
        ];

        return $rules;
    }
}
