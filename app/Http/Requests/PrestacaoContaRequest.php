<?php

namespace App\Http\Requests;

use App\Helpers\Helpers;

class PrestacaoContaRequest extends Request
{
    public function rules()
    {
        $rules = [
            'usuarioId' => 'required',
        ];

        return $rules;
    }

    public function messages()
    {
        $mensagem['usuarioId.required'] = Helpers::message('MSG053');
        return $mensagem;
    }
}
