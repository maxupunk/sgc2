<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;

class PessoaRequest extends Request
{
    public function rules()
    {
        $rules['nome'] = [
            'required',
            'min:6',
            'max:60',
            Rule::unique('pessoas')->ignore($this->id),
        ];
        $rules['contatos'] = 'required|array|min:1';
        $rules['contatos.*.contato'] = 'required|min:4';
        $rules['tipo'] = 'required';
        $rules['rua_id'] = 'required';
        $rules['numero'] = 'numeric|nullable';
        $rules['ativo'] = ['boolean', 'nullable', Rule::in([0, 1])];

        if ($this->tipo === 'f') {
            //$rules['mae'] = 'required|min:6|max:60';
            //$rules['dtnasc'] = 'required';
        }

        return $rules;
    }
}
