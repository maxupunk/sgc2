<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use App\Helpers\Helpers;

abstract class Request extends FormRequest
{
    protected function failedValidation(Validator $validator)
    {

        $response = response()->json([
            'errors' => $validator->errors(),
            'success' => false,
            'type' => 'warning',
            'message' => Helpers::message('MSG004')
        ], 200);

        throw new ValidationException($validator, $response);
    }
}
