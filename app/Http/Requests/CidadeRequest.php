<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use App\Helpers\Helpers;

class CidadeRequest extends Request
{
    public function rules()
    {
        $rules = [
            'nome' => [
                'required',
                'min:2',
                'max:60',
                Rule::unique('cidades')->ignore($this->id),
            ],
            'estado_id' => 'required'
        ];

        if($this->isMethod('PATCH') || $this->isMethod('PUT')){
            $rules['nome'] .= ",$this->id";
        }

        return $rules;
    }

    public function attributes()
    {
        $atributo['estado_id'] = Helpers::message('MSG028');
        return $atributo;
    }
}
