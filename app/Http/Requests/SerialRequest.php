<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;

class SerialRequest extends Request
{
    public function rules()
    {
        $rules = [
            'serial' => [
                'required',
                'min:2',
                'max:45',
                Rule::unique('serials')->ignore($this->id),
            ],
            'produto_id' => 'required'
        ];

        return $rules;
    }
}
