<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UsuarioRequest extends FormRequest
{
    public function rules()
    {
        $rules = [
            'nome' => [
                'required',
                'min:5',
                'max:100',
                Rule::unique('usuarios')->ignore($this->id),
            ],
            'email' => [
                'required',
                'email',
                'max:100',
                Rule::unique('usuarios')->ignore($this->id),
            ],
            'senha' => 'required|min:6|max:60',
            'imagem' => 'max:65000',
            'perfil_id' => 'required|integer',
            'ativo' => 'boolean'
        ];

        if($this->isMethod('PATCH') || $this->isMethod('PUT')){
            if($this->senha === NULL) {
                unset($rules['senha']);
            }
        }

        return $rules;
    }
}
