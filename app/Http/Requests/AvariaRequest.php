<?php

namespace App\Http\Requests;

class AvariaRequest extends Request
{
    public function rules()
    {
        $rules = [
            'estoque_id' => 'required',
            'motivo' => 'required|min:6',
            'quant' => 'required|min:1'
        ];

        return $rules;
    }
}
