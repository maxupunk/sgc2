<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;

class MarcaRequest extends Request
{
    public function rules()
    {
        $rules = [
            'nome' => [
                'required',
                'min:2',
                'max:60',
                Rule::unique('marcas')->ignore($this->id),
            ]
        ];

        return $rules;
    }
}
