<?php

namespace App\Http\Requests;

use App\Helpers\Helpers;
use App\Models\Orden;

class CompraRequest extends Request
{

    protected function prepareForValidation()
    {
        $this['tipo'] = Orden::tipo['compra'];
    }

    public function rules()
    {

        if ($this->isMethod('PUT')) {
            $rules['formapg_id'] = 'required';
            $rules['parcela_id'] = 'required';
            if ($this['pessoa_id'] == null and $this['pgstatus'] == false) {
                $rules['pessoa_id'] = 'required';
            }
        }

        $rules['itens'] = 'required';

        return $rules;
    }

    public function messages()
    {
        $mensagem['itens.required'] = Helpers::message('MSG026');
        $mensagem['pessoa_id.required'] = Helpers::message('MSG027');
        return $mensagem;
    }

    public function attributes()
    {
        $atributo['formapg_id'] = Helpers::message('MSG028');
        $atributo['parcela_id'] = Helpers::message('MSG029');
        return $atributo;
    }
}
