<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AutenticacaoRequest extends FormRequest
{

    public function rules()
    {
        $rules = [
            'email' => 'required|email',
            'senha' => 'required|min:6',
            'device_name' => 'required'
        ];

        return $rules;
    }
}
