<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;

class PerfilRequest extends Request
{
    public function rules()
    {
        $rules = [
            'descricao' => [
                'required',
                'min:3',
                'max:60',
                Rule::unique('perfis')->ignore($this->id),
            ],
            'acoes_perfis' => 'required|array'
        ];

        return $rules;
    }
}
