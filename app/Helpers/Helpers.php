<?php

namespace App\Helpers;

use Carbon\Carbon;

class Helpers
{
    public static function message($string)
    {
        $json = json_decode(file_get_contents(app_path() . '/Helpers/message.json'));
        return $json->$string;
    }

    public static function BitToHumano($size)
    {
        $filesizename = array(" Bytes", " KB", " MB", " GB", " TB", " PB", " EB", " ZB", " YB");
        return $size ? round($size / pow(1024, ($i = floor(log($size, 1024)))), 2) . $filesizename[$i] : '0 Bytes';
    }

    public static function DataToSQL($value)
    {
        $regexData = '/[0-9]{2}\/[0-9]{2}\/[0-9]{4}/';
        $ExistData = preg_match($regexData, $value, $matchesData);
        if ($ExistData) {
            return Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d');
        } else {
            return null;
        }
    }

    public static function RealToSQL($valor)
    {
        if (!is_float($valor)) {
            $fmt = new \NumberFormatter('pt_BR', \NumberFormatter::DECIMAL);
            return $fmt->parse($valor);
        } else {
            return $valor;
        }
    }
}
