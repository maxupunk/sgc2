<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class OrdenUpdateNotification extends Notification
{
    use Queueable;

    private $orden;

    private $notificaoNome = 'UpdateOrden';
    private $titulo = "Ordem de serviço atualizada";

    public function __construct($orden)
    {
        $this->orden = $orden;
    }

    public function via($usuario)
    {
        /* Exemplo de json esperado
            {
                "notificacoes": ["NovaOrden"],
                "notificacaoVia": ["mail", "database"]
            }
        */
        if (isset($usuario->config['notificacoes']) and in_array($this->notificaoNome, $usuario->config['notificacoes'])) {
            if (isset($usuario->config['notificacaoVia'])) {
                return $usuario->config['notificacaoVia'];
            }
        }
    }

    public function toMail($notifiable)
    {
        $url = url('/servicos/' . $this->orden->id);
        return (new MailMessage)
            ->subject($this->titulo)
            ->line("{$notifiable->nome}, nova ordem de serviço foi atualizada no nome de {$this->orden->nome}")
            ->action('Abrir a ordem', $url);
    }

    public function toArray($notifiable)
    {
        $url = '/servicos/' . $this->orden->id;
        return [
            'titulo' => "{$this->titulo}",
            'subtitulo' => "ID : {$this->orden->id} | {$this->orden->descricao}",
            'url' => $url,
        ];
    }
}
