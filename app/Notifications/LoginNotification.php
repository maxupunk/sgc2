<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class LoginNotification extends Notification
{
    use Queueable;

    private $Token;

    private $notificaoNome = 'Login';
    private $titulo = "Sua conta foi acessada de um novo dispositivo";

    public function __construct($token)
    {
        $this->Token = $token;
    }

    public function via($usuario)
    {
        /* Exemplo de json esperado
            {
                "notificacoes": ["NovaOrden", "Login"],
                "notificacaoVia": ["mail", "database"]
            }
        */
        if (isset($usuario->config['notificacoes']) and in_array($this->notificaoNome, $usuario->config['notificacoes'])) {
            if (isset($usuario->config['notificacaoVia'])) {
                return $usuario->config['notificacaoVia'];
            }
        }
    }

    public function toMail($notifiable)
    {
        $url = url('/usuarios/' . $notifiable->id);
        return (new MailMessage)
            ->subject($this->titulo)
            ->line("{$notifiable->nome}, sua conta foi acessada por um novo dispositivo, caso não seja vocé click no botão a baixo.")
            ->action('Abrir a ordem', $url)
            ->line("token gerado: {$this->Token}");
    }

    public function toArray($notifiable)
    {
        $url = '/usuarios/' . $notifiable->id;
        return [
            'titulo' => $this->titulo,
            'subtitulo' => "token gerado: {$this->Token}",
            'url' => $url,
        ];
    }
}
