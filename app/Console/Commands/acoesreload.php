<?php

namespace App\Console\Commands;

use App\Models\Acao;
use Database\Seeders\AcoesTableSeeder;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Schema;

class acoesreload extends Command
{

    protected $signature = 'acoes:reload';
    protected $description = 'Recria todas as acoes do banco de dados com base no seed de ações';
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $continuar = $this->choice('Ao recarregar as acoes o perfil podera ser modificado, deseja continuar?', ['sim','não']);
        if ($continuar == 'sim') {
            $this->comment('Limpando as permisões do banco de dados');
            Schema::disableForeignKeyConstraints();
            Acao::truncate();
            $this->info('concluido');
            $this->comment('Adicionando as ações');
            $this->call(AcoesTableSeeder::class);
            Schema::enableForeignKeyConstraints();
            $this->info('concluido');
        }
    }
}
