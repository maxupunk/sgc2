<?php

namespace App\Console\Commands;

use App\Models\Backup as ModelsBackup;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class backup extends Command
{
    protected $signature = 'backup';
    protected $description = 'Gerenciamento de backup';

    protected $ListaBackups = [];

    public function __construct()
    {
        parent::__construct();

        $files = Storage::disk('backup')->Files();
        foreach ($files as $file) {
            $this->ListaBackups[] = $file;
        }
    }

    public function handle()
    {
        $this->comment('++ Gerenciamento de backup SGC ++');

        $OqueFazer = $this->choice('O que deseja fazer?', ['Criar', 'Restaurar', 'Excluir', 'Sair'], 'Criar');

        if ($OqueFazer == 'Restaurar') {
            $this->Restaurar();
        } else if ($OqueFazer == 'Criar') {
            $this->Criar();
        } else if ($OqueFazer == 'Excluir') {
            $this->Excluir();
        } else if ($OqueFazer == 'Sair') {
            exit;
        }
    }

    protected function Criar()
    {
        $expotacao = ModelsBackup::criar();
        if ($expotacao) {
            $this->info("backup ($expotacao) criado com sucesso!");
        }
        $this->handle();
    }

    protected function Restaurar()
    {
        $backupSelecionado = $this->choice('Qual backup deseja restaurar?', $this->ListaBackups);

        if ($this->confirm("Confirma restauração do backup $backupSelecionado?")) {
            $this->info('Criando ponto de restauração');
            //$this->call('migrate:refresh');
            $restoure = ModelsBackup::restaure($backupSelecionado);
            if ($restoure) {
                $this->info("Concluido com sucesso");
            } else {
                $this->info($restoure);
            }
            $this->handle();
        }
    }

    private function Excluir()
    {
        $backupSelecionado = $this->choice('Qual backup deseja excluir?', $this->ListaBackups);

        if ($this->confirm("Confirma a exclusão definitiva do backup [$backupSelecionado]?")) {
            if (Storage::disk('backup')->delete($backupSelecionado)) {
                $this->info("Backup excluido com sucesso!");
            }
            $this->handle();
        }
    }
}
