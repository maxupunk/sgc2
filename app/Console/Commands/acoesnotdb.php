<?php

namespace App\Console\Commands;

use App\Models\Acao;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class acoesnotdb extends Command
{

    protected $signature = 'acoes:notdb';
    protected $description = 'Lista metodos não cadastrado em acoes no DB';
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Artisan::call('route:list --json');
        $rotasJson = Artisan::output();

        $json = json_decode($rotasJson);

        $saida = [];
        foreach ($json as $route) {
            if ($route->name !== null AND strpos($route->uri, 'admin') !== false AND $route->name != "login") {
                //dd(strpos($route->uri, 'maxuel') . $route->uri);
                $acaoQuery = Acao::where('metodo', 'like', "%$route->name%")->first();

                if (!$acaoQuery) {
                    $saida[] = [$route->uri, $route->name];
                }
            }
        }
        $this->info('Lista de metodos não encontrados');
        $this->table(
            ['uri', 'metodo'],
            $saida
        );
    }
}
