<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;
use App\Models\Usuario;
use App\Models\Perfil;

class CreateUser extends Command
{
    protected $signature = 'user:create';

    public function handle()
    {
        $nome = $this->ask('Digite o nome do usuário');
        $email = $this->ask('Digite o email do usuário');
        $senha = $this->secret('Digite a senha do usuário (pelomenos 6 digitos)');
        if (strlen($senha) < 6) {
            $this->error('Senha menor que 6 digitos');
            return;
        }
        $confirmSenha = $this->secret('Confirme a senha do usuário');
        if ($senha !== $confirmSenha) {
            $this->error('As senhas não coincidem');
            return;
        }
        
        $perfis = Perfil::all(['id', 'descricao'])->pluck('descricao', 'id')->toArray();
        $perfilDesc = $this->choice('Selecione um perfil:', $perfis);
        $perfil = Perfil::where('descricao', $perfilDesc)->select('id')->first();

        Usuario::create([
            'nome' => $nome,
            'email' => $email,
            'senha' => Hash::make($senha),
            'perfil_id' => $perfil->id,
            'ativo' => true
        ]);

        $this->info("Usuário {$nome} criado com sucesso!");
    }
}
