<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class install extends Command
{
    protected $signature = 'install';

    protected $description = 'Instalação do SGC';

    protected $env = [
        'APP_NAME' => 'SGC',
    ];

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $this->info('Iniciando a instalação do SGC');

        if (env('INSTALL')) {
            $this->info('O sistema já esta instalado!');
            exit();
        }

        $app_env = $this->choice('tipo de instalação?', ['local', 'production']);
        $this->env['APP_ENV'] = $app_env;
        if ($app_env == 'production') {
            $this->env['APP_DEBUG'] = 'false';
        } else {
            $this->env['APP_DEBUG'] = 'true';
        }

        $app_url = $this->ask('URL do app [http://sgc.meusite.com.br]');
        if ($app_url != null) {
            $this->env['APP_URL'] = $app_url;
        }

        $db_connection = $this->choice('Qual bando de dados vai usar?', ['mysql', 'pgsql']);
        $this->env['DB_CONNECTION'] = $db_connection;

        $db_host = $this->ask('IP [localhost]');
        if ($db_host != null) {
            $this->env['DB_HOST'] = $db_host;
        }

        $db_port = $this->ask('IP [3306]');
        if ($db_port != null) {
            $this->env['DB_PORT'] = $db_port;
        }

        $db_database = $this->ask('database');
        if ($db_database != null) {
            $this->env['DB_DATABASE'] = $db_database;
        }

        $db_username = $this->ask('Usuario');
        if ($db_username != null) {
            $this->env['DB_USERNAME'] = $db_username;
        }

        $db_password = $this->ask('Senha');
        if ($db_password != null) {
            $this->env['DB_PASSWORD'] = $db_password;
        }

        $redis_host = $this->ask('Redis host');
        if ($redis_host != null) {
            $this->env['REDIS_HOST'] = $db_password;
        }

        $redis_password = $this->ask('Redis senha');
        if ($redis_password != null) {
            $this->env['REDIS_PASSWORD'] = $redis_password;
        }

        $redis_port = $this->ask('Redis port [6379]');
        if ($redis_port != null) {
            $this->env['REDIS_PORT'] = $redis_port;
        }

        $this->info(' - Gerando chave de criptografia');
        $this->call('key:generate', ['--force' => true, '--show' => true]);

        if ($this->setEnvironmentValue($this->env)) {

            $this->info(' - Limpado caches');
            $this->call('config:clear');
            $this->call('route:clear');
            $this->call('view:clear');

            $this->info(' - Gerando chave da APP_KEY do laravel');
            $this->call('key:generate');

            $this->info(' - Discobrindo pacotes');
            $this->call('package:discover');

            $this->info(' - Criando a estrutura de Base de dados');
            $this->call('migrate');

            $this->info(' - Polulando a base de dados');
            $this->call('db:seed');

            if ($app_env == 'production') {
                $this->info(' - Criando os cache');
                $this->call('config:cache');
                $this->call('view:cache');
                // não pode roda o cache de rota por conta do sistema vue_capture
                //$this->call('route:cache');
            }

            $this->setEnvironmentValue(['INSTALL' => 'true']);

            $this->info('Instalação concluida');
        } else {
            $this->error('Opa, alguma coisa saio errado!');
        }
    }

    private function setEnvironmentValue(array $values)
    {
        $envFile = app()->environmentFilePath();
        $str = file_get_contents($envFile);
        if (count($values) > 0) {
            foreach ($values as $envKey => $envValue) {

                $keyPosition = strpos($str, "$envKey=");
                $endOfLinePosition = strpos($str, "\n", $keyPosition);
                $oldLine = substr($str, $keyPosition, $endOfLinePosition - $keyPosition);

                if (is_bool($keyPosition) && $keyPosition === false) {
                    // variable doesnot exist
                    $str .= "$envKey=$envValue";
                } else {
                    // variable exist
                    $str = str_replace($oldLine, "$envKey=$envValue", $str);
                }
            }
        }

        return file_put_contents($envFile, $str);
    }
}
