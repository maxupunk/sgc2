<?php

namespace Database\Factories;

use App\Models\Perfil;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;


class UsuarioFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'pessoa_id' => $this->faker->numberBetween(1, 100),
            'email' => $this->faker->unique()->safeEmail,
            'nome' => $this->faker->name,
            'senha' => Hash::make('password'),
            'perfil_id' => Perfil::factory(),
            'empresa_id' => null,
            'imagem' => null,
            'config' => null,
            'ativo' => true,
        ];
    }
}
