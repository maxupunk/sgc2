<?php

namespace Database\Factories;

use App\Models\Cidade;
use App\Models\Rua;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Carbon\Carbon;

class PessoaFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        //dd(Rua::factory());
        return [
            'nome' => Str::random(30),
            'fantasia' => Str::random(10),
            'documento' => Str::random(11),
            'mae' => Str::random(30),
            'pai' => Str::random(30),
            'dtnasc' => Carbon::now()->subYears(25)->format('d/m/Y'),
            'tipo' => 'f',
            'rua_id' => Rua::factory(),
            'numero' => rand(1, 100),
            'cep' => Str::random(8),
            'complemento' => Str::random(10),
            'senha' => Hash::make('secret'),
            'imagem' => null,
            'config' => null,
            'ativo' => true,
        ];
    }
}
