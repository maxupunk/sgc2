<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class PerfilFactory extends Factory
{
    public function definition(): array
    {
        return [
            'descricao' => $this->faker->unique()->word,
        ];;
    }
}
