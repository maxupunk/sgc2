<?php

namespace Database\Factories;

use App\Models\Bairro;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class RuaFactory extends Factory
{
    public function definition(): array
    {
        return [
            'nome' => Str::random(45),
            'bairro_id' => Bairro::factory(),
        ];
    }
}
