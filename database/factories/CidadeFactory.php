<?php

namespace Database\Factories;

use App\Models\Estado;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class CidadeFactory extends Factory
{
    public function definition(): array
    {
        return [
            'nome' => Str::random(45),
            'estado_id' => Estado::factory(),
        ];
    }
}
