<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class EstadoFactory extends Factory
{
    public function definition(): array
    {
        return [
            'nome' => Str::random(45),
            'uf' => Str::random(2)
        ];
    }
}
