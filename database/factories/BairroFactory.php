<?php

namespace Database\Factories;

use App\Models\Cidade;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class BairroFactory extends Factory
{
    public function definition(): array
    {
        return [
            'nome' => Str::random(45),
            'cidade_id' => Cidade::factory(),
        ];
    }
}
