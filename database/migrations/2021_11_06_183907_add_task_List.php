<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTaskList extends Migration
{
    public function up()
    {
        Schema::table('estoques', function (Blueprint $table) {
            $table->json('tasklist')->nullable()->after('alerta');
        });

        Schema::table('lista_itens', function (Blueprint $table) {
            $table->json('tasklist')->nullable()->after('comissao');
        });
    }

    public function down()
    {
        Schema::table('estoques', function (Blueprint $table) {
            if (Schema::hasColumn('estoques', 'tasklist')) {
                $table->dropColumn('tasklist');
            }
        });

        Schema::table('lista_itens', function (Blueprint $table) {
            if (Schema::hasColumn('lista_itens', 'tasklist')) {
                $table->dropColumn('tasklist');
            }
        });
    }
}
