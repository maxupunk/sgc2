<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPrestacaoFinanceiro extends Migration
{

    public function up()
    {
        Schema::table('financeiros', function (Blueprint $table) {
            $table->boolean('fechado')->nullable()->default(false)->after('arquivos');
        });
    }


    public function down()
    {
        Schema::table('financeiros', function (Blueprint $table) {
            if (Schema::hasColumn('financeiros', 'fechado')) {
                $table->dropColumn('fechado');
            }
        });
    }
}
