<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstoqueSerialsTable extends Migration
{
    private $tabela = 'estoque_serials';

    public function up()
    {
        Schema::create($this->tabela, function (Blueprint $table) {
            $table->integer('estoque_id')->unsigned();
            $table->string('serial', 60);
            $table->boolean('ativo');
            $table->timestamps();

            $table->foreign('estoque_id')
                ->references('id')->on('estoques')
                ->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists($this->tabela);
    }
}
