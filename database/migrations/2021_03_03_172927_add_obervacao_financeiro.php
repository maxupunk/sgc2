<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddObervacaoFinanceiro extends Migration
{
    public function up()
    {
        Schema::table('financeiros', function (Blueprint $table) {
            $table->string('observacao', 150)->nullable()->after('descricao');
        });
    }

    public function down()
    {
        Schema::table('financeiros', function (Blueprint $table) {
            $table->dropColumn('observacao');
        });
    }
}
