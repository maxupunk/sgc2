<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParcelasTable extends Migration
{

    public function up()
    {
        Schema::create('parcelas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('formapg_id')->unsigned();
            $table->integer('nparcela');
            $table->decimal('juros', 5, 2);

            $table->foreign('formapg_id')
                ->references('id')->on('formapgs')
                ->onDelete('cascade');

        });
    }


    public function down()
    {
        Schema::dropIfExists('parcelas');
    }
}
