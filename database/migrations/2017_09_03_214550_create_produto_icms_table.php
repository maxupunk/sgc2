<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdutoIcmsTable extends Migration
{

    public function up()
    {
        Schema::create('produto_icms', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->string('codigo', 10);
            $table->text('descricao');
        });
    }


    public function down()
    {
        Schema::dropIfExists('produto_icms');
    }
}
