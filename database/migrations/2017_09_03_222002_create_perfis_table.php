<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerfisTable extends Migration
{

    public function up()
    {
        Schema::create('perfis', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descricao', 60)->unique();;
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('perfis');
    }
}
