<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdutoCestTable extends Migration
{

    public function up()
    {
        Schema::create('produto_cest', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->char('codigo', 10)->unique();
            $table->text('descricao');
        });
    }


    public function down()
    {
        Schema::dropIfExists('produto_cest');
    }
}
