<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRuasTable extends Migration
{

    public function up()
    {
        Schema::create('ruas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bairro_id')->unsigned();
            $table->string('nome', 45);
            $table->timestamps();

            $table->foreign('bairro_id')
            ->references('id')->on('bairros')
            ->onDelete('cascade');

        });
    }


    public function down()
    {
        Schema::dropIfExists('ruas');
    }
}
