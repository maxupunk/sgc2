<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdutoCfopTable extends Migration
{

    public function up()
    {
        Schema::create('produto_cfop', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->text('descricao');
        });
    }


    public function down()
    {
        Schema::dropIfExists('produto_cfop');
    }
}
