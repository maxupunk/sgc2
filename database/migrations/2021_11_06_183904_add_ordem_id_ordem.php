<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOrdemIdOrdem extends Migration
{
    public function up()
    {
        Schema::table('ordens', function (Blueprint $table) {
            $table->integer('orden_id')->unsigned()->nullable()->after('pessoa_id');

            $table->foreign('orden_id')
            ->references('id')->on('ordens')
            ->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::table('ordens', function (Blueprint $table) {
            $table->dropForeign(['orden_id']);
            $table->dropColumn('orden_id');
        });
    }
}
