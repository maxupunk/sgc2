<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCaixaTable extends Migration
{
    public function up()
    {
        Schema::create('caixa', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('usuario_id')->unsigned();
            $table->decimal('abertura', 10, 2);
            $table->decimal('fechamento', 10, 2);
            $table->text('descricao');
            $table->boolean('aberto');

            $table->timestamps();

            $table->foreign('usuario_id')
                ->references('id')->on('usuarios')
                ->onDelete('restrict');
        });
    }

    public function down()
    {
        Schema::dropIfExists('caixa');
    }
}
