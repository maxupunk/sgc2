<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBairrosTable extends Migration
{

    public function up()
    {
        Schema::create('bairros', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cidade_id')->unsigned();
            $table->string('nome', 45);
            $table->timestamps();

            $table->foreign('cidade_id')
            ->references('id')->on('cidades')
            ->onDelete('cascade');

        });
    }


    public function down()
    {
        Schema::dropIfExists('bairros');
    }
}
