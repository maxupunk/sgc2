<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFinanceirosTable extends Migration
{

    public function up()
    {
        Schema::create('financeiros', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('orden_id')->unsigned()->nullable();
            // pessoa_id pode aceitar null, pois quando for venda avulsa o valor sera null
            $table->integer('pessoa_id')->unsigned()->nullable();
            $table->enum('nature', ['R', 'D'])
                ->comment('R - RECEITA | D - DESPESA');

            $table->string('descricao', 150)->nullable();
            $table->decimal('valor', 10, 2);
            $table->decimal('pago', 10, 2)->default(0);
            $table->date('vencimento')->nullable();
            $table->date('datapg')->nullable();

            $table->json('arquivos')->nullable();

            $table->enum('estatus', ['AB', 'PG', 'PP', 'CN', 'DV', 'DL'])->default('AB')
                ->comment(' AB = aberta
                        PG = Pago
                        PP = parcialmente pago
                        CN = cancelado
                        DV = devolução
                        DL = deletado');

            $table->timestamps();

            $table->foreign('orden_id')
                ->references('id')->on('ordens')
                ->onDelete('cascade');

            $table->foreign('pessoa_id')
                ->references('id')->on('pessoas')
                ->onDelete('restrict');
        });
    }


    public function down()
    {
        Schema::dropIfExists('financeiros');
    }
}
