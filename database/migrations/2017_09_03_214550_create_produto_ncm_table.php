<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdutoNcmTable extends Migration
{

    public function up()
    {
        Schema::create('produto_ncm', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->string('codigo',16);
            $table->text('descricao');
        });
    }


    public function down()
    {
        Schema::dropIfExists('produto_ncm');
    }
}
