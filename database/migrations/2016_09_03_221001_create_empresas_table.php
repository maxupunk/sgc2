<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresasTable extends Migration
{

    public function up()
    {
        Schema::create('empresas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('razaosocial', 60);
            $table->string('nomefantasia', 60);
            $table->char('cnpj', 14)->unique();
            $table->char('inscricaomunicipal', 14)->unique();
            $table->char('inscricaoestadual', 14)->unique();
            $table->string('telefone', 60);
            $table->string('email', 60)->nullable();
            $table->date('dtnasc')->nullable();
            $table->string('endereco', 100)->nullable();
            $table->string('cep', 25);
            $table->string('complemento', 100)->nullable();
            //configurações globais
            $table->json('config')->nullable();

            $table->boolean('ativo');
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('empresas');
    }
}
