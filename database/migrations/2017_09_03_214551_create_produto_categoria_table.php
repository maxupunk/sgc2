<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdutoCategoriaTable extends Migration
{
    private $tabela = 'produto_categoria';

    public function up()
    {
        Schema::create($this->tabela, function (Blueprint $table) {
            $table->integer('produto_id')->unsigned();
            $table->integer('categoria_id')->unsigned();

            $table->foreign('produto_id')
            ->references('id')->on('produtos')
            ->onDelete('cascade');

            $table->foreign('categoria_id')
                ->references('id')->on('categorias')
                ->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists($this->tabela);
    }
}
