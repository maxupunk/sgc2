<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormapgsTable extends Migration
{

    public function up()
    {
        Schema::create('formapgs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descricao', 45)->unique();;
            $table->decimal('custo', 5, 2);
            $table->boolean('ativo')->default(0);
        });
    }


    public function down()
    {
        Schema::dropIfExists('formapgs');
    }
}
