<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSenhaPessoas extends Migration
{
    public function up()
    {
        Schema::table('pessoas', function (Blueprint $table) {
            $table->string('fantasia', 30)->nullable()->after('nome');
            $table->string('senha', 60)->nullable()->after('ativo');
            $table->binary('imagem')->nullable()->after('senha');
            $table->json('config')->nullable()->after('imagem');
        });
    }

    public function down()
    {
        Schema::table('pessoas', function (Blueprint $table) {
            $table->dropColumn('fantasia');
            $table->dropColumn('senha');
            $table->dropColumn('imagem');
            $table->dropColumn('config');
        });
    }
}
