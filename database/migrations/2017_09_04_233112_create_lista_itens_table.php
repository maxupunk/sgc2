<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListaItensTable extends Migration
{

    public function up()
    {
        Schema::create('lista_itens', function (Blueprint $table) {
            $table->integer('orden_id')->unsigned();
            $table->integer('estoque_id')->unsigned()->nullable();
            // para colocar o usuario tecnico que fez o serviço
            $table->integer('usuario_id')->unsigned()->nullable();

            $table->string('observacao', 100)->nullable();

            $table->decimal('quant', 10, 3)->default(1);
            $table->decimal('valor', 10, 2);
            $table->decimal('desconto', 10, 2)->default(0);
            $table->decimal('acrescimo', 10, 2)->default(0);
            $table->tinyInteger('comissao')->nullable()->default(0);
            $table->smallInteger('estatus')->nullable()
                ->comment('1 = deve dá entrada/saida
                           2 = já foi dado entrada/saida
                           3 = não precisa mexer, item avulso');

            $table->foreign('orden_id')
            ->references('id')->on('ordens')
            ->onDelete('cascade');

            $table->foreign('estoque_id')
            ->references('id')->on('estoques')
            ->onDelete('cascade');

            $table->foreign('usuario_id')
                ->references('id')->on('usuarios')
                ->onDelete('restrict');

        });
    }


    public function down()
    {
        Schema::dropIfExists('lista_itens');
    }
}
