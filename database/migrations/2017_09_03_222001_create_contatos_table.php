<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContatosTable extends Migration
{

    public function up()
    {
        Schema::create('contatos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pessoa_id')->unsigned();
            $table->enum('tipo', ['TF','CL','EM','OT'])->default('TF')
                ->comment('TF = TELEFONE
                                    CL = CELULAR
                                    EM = EMAIL
                                    OT = OUTRO');
            $table->string('contato', 40);
            $table->timestamps();

            $table->foreign('pessoa_id')
            ->references('id')->on('pessoas')
            ->onDelete('cascade');
        });
    }


    public function down()
    {
        Schema::dropIfExists('contatos');
    }
}
