<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveTipoContato extends Migration
{
    public function up()
    {
        Schema::table('contatos', function (Blueprint $table) {
            $table->dropColumn('tipo');
        });
    }

    public function down()
    {
        Schema::table('contatos', function (Blueprint $table) {
            $table->string('tipo', 2);
        });
    }
}
