<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateprodutoIpiTable extends Migration
{

    public function up()
    {
        Schema::create('produto_ipi', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->string('codigo', 4);
            $table->text('descricao');
        });
    }


    public function down()
    {
        Schema::dropIfExists('produto_ipi');
    }
}
