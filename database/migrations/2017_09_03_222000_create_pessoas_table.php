<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePessoasTable extends Migration
{

    public function up()
    {
        Schema::create('pessoas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->enum('tipo', ['f','j'])->comment('f = fisica j = juridica');
            $table->string('documento', 18)->nullable();
            $table->text('mae')->nullable();
            $table->text('pai')->nullable();
            $table->date('dtnasc')->nullable();
            $table->enum('sexo', ['m','f', 'o'])->comment('m = masculia f = feminino o = outros')->nullable();;
            $table->integer('rua_id')->unsigned();
            $table->integer('numero')->nullable();
            $table->string('cep', 25);
            $table->string('complemento', 100)->nullable();
            $table->boolean('ativo')->nullable();
            $table->timestamps();

            $table->foreign('rua_id')
                ->references('id')->on('ruas')
                ->onDelete('restrict');
        });
    }


    public function down()
    {
        Schema::dropIfExists('pessoas');
    }
}
