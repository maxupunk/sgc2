<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAcoesTable extends Migration
{

    public function up()
    {
        Schema::create('acoes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome', 40);
            $table->text('metodo');
            $table->string('titulo');
        });
    }


    public function down()
    {
        Schema::dropIfExists('acoes');
    }
}
