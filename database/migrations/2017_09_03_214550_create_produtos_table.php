<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdutosTable extends Migration
{
    public function up()
    {
        Schema::create('produtos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descricao', 100);
            $table->text('caracteristica')->nullable();
            $table->enum('tipo', ['p','s','k'])->comment('p - produto | s - serviço | k - kit');

            $table->decimal('peso', 10, 3)->nullable();

            $table->integer('medida_id')->unsigned()->nullable();
            $table->foreign('medida_id')->references('id')->on('medidas')->onDelete('restrict');

            $table->integer('marca_id')->unsigned()->nullable();
            $table->foreign('marca_id')->references('id')->on('marcas')->onDelete('restrict');

            $table->integer('origem_id')->unsigned()->nullable();
            $table->foreign('origem_id')->references('id')->on('produto_origens')->onDelete('restrict');

            $table->integer('tipo_id')->unsigned()->nullable();
            $table->foreign('tipo_id')->references('id')->on('produto_tipos')->onDelete('restrict');

            $table->integer('ncm_id')->unsigned()->nullable();
            $table->foreign('ncm_id')->references('id')->on('produto_ncm')->onDelete('restrict');

            $table->integer('cest_id')->unsigned()->nullable();
            $table->foreign('cest_id')->references('id')->on('produto_cest')->onDelete('restrict');

            $table->integer('cfop_id')->unsigned()->nullable();
            $table->foreign('cfop_id')->references('id')->on('produto_cfop')->onDelete('restrict');

            $table->integer('icms_id')->unsigned()->nullable();
            $table->foreign('icms_id')->references('id')->on('produto_icms')->onDelete('restrict');
            $table->decimal('icms', 5, 2)->nullable();

            $table->integer('ipi_id')->unsigned()->nullable();
            $table->foreign('ipi_id')->references('id')->on('produto_ipi')->onDelete('restrict');
            $table->decimal('ipi', 5, 2)->nullable();


            $table->integer('pis_id')->unsigned()->nullable();
            $table->foreign('pis_id')->references('id')->on('produto_pis_cofins')->onDelete('restrict');
            $table->decimal('pis', 5, 2)->nullable();

            $table->integer('cofins_id')->unsigned()->nullable();
            $table->foreign('cofins_id')->references('id')->on('produto_pis_cofins')->onDelete('restrict');
            $table->decimal('cofins', 5, 2)->nullable();

            $table->timestamps();

        });
    }

    public function down()
    {
        Schema::dropIfExists('produtos');
    }
}
