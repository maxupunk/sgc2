<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstadosTable extends Migration
{

    public function up()
    {
        Schema::create('estados', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome', 45)->unique();
            $table->char('uf', 2)->unique();
        });
    }


    public function down()
    {
        Schema::dropIfExists('estados');
    }
}
