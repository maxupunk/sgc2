<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAvariasTable extends Migration
{

    public function up()
    {
        Schema::create('avarias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('usuario_id')->unsigned();
            $table->integer('estoque_id')->unsigned();
            $table->decimal('quant', 10, 3);
            $table->decimal('valor', 10, 2);
            $table->text('motivo');
            $table->timestamps();

            $table->foreign('usuario_id')
            ->references('id')->on('usuarios')
            ->onDelete('restrict');

            $table->foreign('estoque_id')
            ->references('id')->on('estoques')
            ->onDelete('cascade');

        });
    }


    public function down()
    {
        Schema::dropIfExists('avarias');
    }
}
