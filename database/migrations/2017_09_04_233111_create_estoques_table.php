<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstoquesTable extends Migration
{
    public function up()
    {
        Schema::create('estoques', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('produto_id')->unsigned();
            $table->integer('empresa_id')->unsigned()->nullable();
            $table->string('codigo', 20)->nullable();
            $table->string('variante', 50)->nullable();
            $table->decimal('quant', 10, 3)->default(0);
            $table->decimal('custo', 10, 2);
            $table->decimal('valor', 10, 2);
            $table->json('imagens')->nullable();
            $table->tinyInteger('comissao')->default(0);
            $table->string('encontra_se', 10)->nullable();
            $table->integer('alerta')->nullable();
            $table->boolean('ativo')->nullable();
            $table->timestamps();

            $table->foreign('produto_id')
            ->references('id')->on('produtos')
            ->onDelete('cascade');

            $table->foreign('empresa_id')
                ->references('id')->on('empresas')
                ->onDelete('cascade');

        });
    }

    public function down()
    {
        Schema::dropIfExists('estoques');
    }
}
