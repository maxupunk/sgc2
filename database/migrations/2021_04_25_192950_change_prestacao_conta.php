<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangePrestacaoConta extends Migration
{
    public function up()
    {
        Schema::table('financeiros', function (Blueprint $table) {
            $table->dropColumn('fechado');
        });

        Schema::table('lista_itens', function (Blueprint $table) {
            $table->increments('id')->first();
        });

        Schema::dropIfExists('prestacao_contas');
        Schema::create('prestacao_contas', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('usuario_id')->unsigned();
            $table->foreign('usuario_id')
                ->references('id')->on('usuarios')
                ->onDelete('restrict');

            $table->integer('financeiro_id')->unsigned();
            $table->foreign('financeiro_id')
                ->references('id')->on('financeiros')
                ->onDelete('restrict');

            $table->integer('lista_iten_id')->unsigned();
            $table->foreign('lista_iten_id')
                ->references('id')->on('lista_itens')
                ->onDelete('cascade');

            $table->integer('pagamento_id')->unsigned()->comment('esse é o ip do financeiro pago dessas ordens');
            $table->foreign('pagamento_id')
                ->references('id')->on('financeiros')
                ->onDelete('cascade');

            $table->decimal('comissao', 10, 2);

            $table->timestamp('created_at')->useCurrent();
        });
    }

    public function down()
    {
    }
}
