<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedidasTable extends Migration
{

    public function up()
    {
        Schema::create('medidas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome', 20)->unique();
            $table->char('sigla', 4)->unique();
        });
    }


    public function down()
    {
        Schema::dropIfExists('medidas');
    }
}
