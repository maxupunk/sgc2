<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdutoOrigensTable extends Migration
{

    public function up()
    {
        Schema::create('produto_origens', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->string('descricao', 100);
        });
    }


    public function down()
    {
        Schema::dropIfExists('produto_origens');
    }
}
