<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuariosTable extends Migration
{

    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome', 60);
            $table->string('email', 60)->unique();;
            $table->string('senha', 60);
            $table->integer('perfil_id')->unsigned();
            $table->integer('empresa_id')->unsigned()->nullable();
            $table->binary('imagem')->nullable();
            $table->json('config')->nullable();
            $table->boolean('ativo')->nullable();
            $table->timestamps();

            $table->foreign('empresa_id')->references('id')->on('empresas');
            $table->foreign('perfil_id')->references('id')->on('perfis');
        });
    }

    public function down()
    {
        Schema::dropIfExists('usuarios');
    }
}
