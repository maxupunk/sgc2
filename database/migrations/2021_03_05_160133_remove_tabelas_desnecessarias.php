<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveTabelasDesnecessarias extends Migration
{

    public function up()
    {
        Schema::dropIfExists('estoque_historico');
        Schema::dropIfExists('log_acesso');
        Schema::dropIfExists('financeiro_historico');
    }

    public function down()
    {

    }
}
