<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrestacaoContasTable extends Migration
{
    public function up()
    {
        Schema::create('prestacao_contas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('usuario_id')->unsigned();
            $table->integer('financeiro_id')->unsigned();
            $table->decimal('valor', 10, 2);
            $table->text('descricao')->nullable();

            $table->timestamps();

            $table->foreign('usuario_id')
                ->references('id')->on('usuarios')
                ->onDelete('restrict');

            $table->foreign('financeiro_id')
                ->references('id')->on('financeiros')
                ->onDelete('restrict');
        });
    }

    public function down()
    {
        Schema::dropIfExists('prestacao_contas');
    }
}
