<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdensTable extends Migration
{

    public function up()
    {
        Schema::create('ordens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pessoa_id')->unsigned()->nullable();

            $table->enum('tipo', ['v','c','s','d','t'])
            ->comment('V = venda | C = compra | S = servico | D = devolução | T = transferencia/troca');

            $table->text('descricao')->nullable()->comment('DADOS DO EQUIPAMENTO DO CLIENTE');
            $table->text('defeito')->nullable();

            $table->string('acessorios', 50)->nullable();

            $table->string('documento', 45)->nullable();
            $table->decimal('acrescimo', 10, 2)->nullable()->default('0');
            $table->decimal('desconto', 10, 2)->nullable()->default('0');
            $table->decimal('frete', 10, 2)->nullable()->default('0');
            $table->integer('transportadora_id')->unsigned()->nullable();
            $table->string('codigo_rastreio', 45)->nullable();

            $table->json('imagens')->nullable();

            $table->json('anotacoes')->nullable();

            $table->integer('parcela_id')->unsigned()->nullable();

            $table->decimal('total', 10, 2)->nullable()->default('0');

            $table->enum('estatus', ['EA', 'AA', 'PA','AP','EE','RP','RE','CC','CA'])->default('EA')
            ->comment(' EA = EM ABERTA
                        AA = AGUARDANDO APROVACAO
                        PA = PAGAMENTO EM ANALISE
                        AP = AGUARDANDO PAGAMENTO
                        EE = ENVIADO/ESPERANDO
                        RP = RECEB/ENTREG PARCIAL
                        RE = RECEBIDO/ENTREGUE
                        CC = CONCLUIDO
                        CA = CANCELADA');

            $table->date('entregue')->nullable();
            $table->timestamps();

            $table->foreign('pessoa_id')
            ->references('id')->on('pessoas')
            ->onDelete('restrict');

            $table->foreign('transportadora_id')
                ->references('id')->on('pessoas')
                ->onDelete('restrict');

            $table->foreign('parcela_id')
            ->references('id')->on('parcelas')
            ->onDelete('restrict');

        });
    }


    public function down()
    {
        Schema::dropIfExists('ordens');
    }
}
