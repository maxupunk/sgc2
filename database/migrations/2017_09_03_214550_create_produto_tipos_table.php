<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdutoTiposTable extends Migration
{

    public function up()
    {
        Schema::create('produto_tipos', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->string('descricao', 40);

        });
    }


    public function down()
    {
        Schema::dropIfExists('produto_tipos');
    }
}
