<?php

use App\Models\Orden;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeTableOrdensStatus extends Migration
{

    public function up()
    {
        Schema::table('ordens', function (Blueprint $table) {
            $table->string('status', 2)->default('EA')
                ->comment('EA = EM ABERTA
                        PD = PENDENTE
                        OR = ORÇAMENTO
                        AA = AGUARDANDO APROVACAO
                        PA = PAGAMENTO EM ANALISE
                        AP = AGUARDANDO PAGAMENTO
                        EE = ENVIADO/ESPERANDO
                        RP = RECEB/ENTREG PARCIAL
                        RE = RECEBIDO/ENTREGUE
                        CC = CONCLUIDO
                        CA = CANCELADA');
        });

        $ordens = Orden::get();
        foreach ($ordens as $orden) {
            Orden::find($orden->id)->update([
                'status' => $orden->estatus
            ]);
        }

        Schema::table('ordens', function (Blueprint $table) {
            $table->dropColumn('estatus');
        });
    }

    public function down()
    {
    }
}
