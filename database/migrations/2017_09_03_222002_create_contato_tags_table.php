<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContatoTagsTable extends Migration
{

    public function up()
    {
        Schema::create('contato_tag', function (Blueprint $table) {
            $table->integer('tag_id')->unsigned();
            $table->integer('contato_id')->unsigned();

            $table->foreign('contato_id')
            ->references('id')->on('contatos')
            ->onDelete('cascade');

            $table->foreign('tag_id')
                ->references('id')->on('tags')
                ->onDelete('cascade');
        });
    }


    public function down()
    {
        Schema::dropIfExists('contato_tag');
    }
}
