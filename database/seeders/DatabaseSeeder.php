<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // lista de seeds
        $this->call(EstadosTableSeeder::class);
        $this->call(CidadesTableSeeder::class);
        $this->call(BairrosTableSeeder::class);
        $this->call(RuasTableSeeder::class);

        $this->call(AcoesTableSeeder::class);
        $this->call(PerfisTableSeeder::class);
        $this->call(UsuariosTableSeeder::class);

        $this->call(ProdutoTiposTableSeeder::class);
        $this->call(ProdutoNCMSeeder::class);
        $this->call(ProdutoOrigensSeeder::class);
        $this->call(ProdutoCESTSeeder::class);
        $this->call(ProdutoCFOPSeeder::class);
        $this->call(ProdutoCMSSeeder::class);
        $this->call(ProdutoIPISeeder::class);
        $this->call(ProdutoPISCONFINSSeeder::class);
        $this->call(FormaPgSeeder::class);
        $this->call(ParcelaSeeder::class);

    }
}
