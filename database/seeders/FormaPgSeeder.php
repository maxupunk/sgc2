<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Formapg;

class FormaPgSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Formapg::create(array('id' => 1, 'descricao' => 'Avsita', 'custo' => 0, 'ativo' => 1));
    }
}
