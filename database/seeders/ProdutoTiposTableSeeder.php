<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProdutoTiposTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tipos = [
            1 =>  ['id' => '1', 'descricao' => 'MERCADORIA PARA REVENDA'],
            2 =>  ['id' => '2', 'descricao' => 'MATERIA-PRIMA'],
            3 =>  ['id' => '3', 'descricao' => 'EMBALAGEM'],
            4 =>  ['id' => '4', 'descricao' => 'PRODUTO EM PROCESSO'],
            5 =>  ['id' => '5', 'descricao' => 'PRODUTO ACABADO'],
            6 =>  ['id' => '6', 'descricao' => 'SUBPRODUTO'],
            7 =>  ['id' => '7', 'descricao' => 'PRODUTO INTERMEDIARIO'],
            8 =>  ['id' => '8', 'descricao' => 'MATERIAL DE USO E CONSUMO'],
            9 =>  ['id' => '9', 'descricao' => 'ATIVO IMOBILIZADO'],
            10 => ['id' => '10', 'descricao' => 'SERVICOS'],
            11 => ['id' => '11', 'descricao' => 'OUTROS INSUMOS'],
            12 => ['id' => '12', 'descricao' => 'OUTRAS'],
            13 => ['id' => '16', 'descricao' => 'GARRAFA']
        ];

        DB::table('produto_tipos')->insert($tipos);
    }
}
