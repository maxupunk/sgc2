<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Estado;

class EstadosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Estado::create(array('nome' => 'Acre', 'uf' => 'AC'));
        Estado::create(array('nome' => 'Alagoas', 'uf' => 'AL'));
        Estado::create(array('nome' => 'Amapá', 'uf' => 'AP'));
        Estado::create(array('nome' => 'Amazonas', 'uf' => 'AM'));
        Estado::create(array('nome' => 'Bahia', 'uf' => 'BA'));
        Estado::create(array('nome' => 'Ceará', 'uf' => 'CE'));
        Estado::create(array('nome' => 'Distrito Federal', 'uf' => 'DF'));
        Estado::create(array('nome' => 'Espírito Santo', 'uf' => 'ES'));
        Estado::create(array('nome' => 'Goiás', 'uf' => 'GO'));
        Estado::create(array('nome' => 'Maranhão', 'uf' => 'MA'));
        Estado::create(array('nome' => 'Mato Grosso', 'uf' => 'MT'));
        Estado::create(array('nome' => 'Mato Grosso do Sul', 'uf' => 'MS'));
        Estado::create(array('nome' => 'Minas Gerais', 'uf' => 'MG'));
        Estado::create(array('nome' => 'Pará', 'uf' => 'PA'));
        Estado::create(array('nome' => 'Paraíba', 'uf' => 'PB'));
        Estado::create(array('nome' => 'Paraná', 'uf' => 'PR'));
        Estado::create(array('nome' => 'Pernambuco', 'uf' => 'PE'));
        Estado::create(array('nome' => 'Piauí', 'uf' => 'PI'));
        Estado::create(array('nome' => 'Rio de Janeiro', 'uf' => 'RJ'));
        Estado::create(array('nome' => 'Rio Grande do Norte', 'uf' => 'RN'));
        Estado::create(array('nome' => 'Rio Grande do Sul', 'uf' => 'RS'));
        Estado::create(array('nome' => 'Rondônia', 'uf' => 'RO'));
        Estado::create(array('nome' => 'Roraima', 'uf' => 'RR'));
        Estado::create(array('nome' => 'Santa Catarina', 'uf' => 'SC'));
        Estado::create(array('nome' => 'São Paulo', 'uf' => 'SP'));
        Estado::create(array('nome' => 'Sergipe', 'uf' => 'SE'));
        Estado::create(array('nome' => 'Tocantins', 'uf' => 'TO'));

    }
}
