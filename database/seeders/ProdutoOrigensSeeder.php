<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProdutoOrigensSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $origens = [
            0 => ['id' => '1', 'descricao' => 'NACIONAL'],
            1 => ['id' => '2', 'descricao' => 'ESTRANGEIRA IMPORTACAO DIRETA'],
            2 => ['id' => '3', 'descricao' => 'ESTRANGEIRA ADQUIRIDA NO MERCADO INTERNO']
        ];

        DB::table('produto_origens')->insert($origens);
    }
}
