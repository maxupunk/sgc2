<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Parcela;
class ParcelaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Parcela::create(array('formapg_id' => 1, 'nparcela' => 1, 'juros' => 0));
    }
}
