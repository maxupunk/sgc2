<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProdutoCMSSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $icms = [
            ARRAY(
                'descricao' => '00: Tributada integralmente',
                'codigo' => '00'
            ),
            ARRAY(
                'descricao' => '10: Tributada com cobr. por subst. trib.',
                'codigo' => '10'
            ),
            ARRAY(
                'descricao' => '20: Com redução de base de cálculo',
                'codigo' => '20'
            ),
            ARRAY(
                'descricao' => '30: Isenta ou não trib com cobr por subst trib',
                'codigo' => '30'
            ),
            ARRAY(
                'descricao' => '40: Isenta',
                'codigo' => '40'
            ),
            ARRAY(
                'descricao' => '41: Não tributada',
                'codigo' => '41'
            ),
            ARRAY(
                'descricao' => '50: Suspesão',
                'codigo' => '50'
            ),
            ARRAY(
                'descricao' => '51: Diferimento',
                'codigo' => '51'
            ),
            ARRAY(
                'descricao' => '60: ICMS cobrado anteriormente por subst trib',
                'codigo' => '60'
            ),
            ARRAY(
                'descricao' => '70: Redução de Base Calc e cobr ICMS por subst trib',
                'codigo' => '70'
            ),
            ARRAY(
                'descricao' => '90: Outros',
                'codigo' => '90'
            ),
            ARRAY(
                'descricao' => 'Partilha 10: Entre UF origem e destino ou definida na legislação com Subst Trib',
                'codigo' => '10Part'
            ),
            ARRAY(
                'descricao' => 'Partilha 90: Entre UF origem e destino ou definida na legislação - outros',
                'codigo' => '90Part'
            ),
            ARRAY(
                'descricao' => 'Repasse 41: ICMS ST retido em operações interestaduais com repasses do Subst Trib',
                'codigo' => '41ST'
            ),
            ARRAY(
                'descricao' => 'Simples Nacional: 101: Com permissão de crédito',
                'codigo' => '101'
            ),
            ARRAY(
                'descricao' => 'Simples Nacional: 102: Sem permissão de crédito',
                'codigo' => '102'
            ),
            ARRAY(
                'descricao' => 'Simples Nacional: 103: Isenção do ICMS para faixa de receita bruta',
                'codigo' => '103'
            ),
            ARRAY(
                'descricao' => 'Simples Nacional: 201: Com permissão de crédito, com cobr ICMS por Subst Trib',
                'codigo' => '201'
            ),
            ARRAY(
                'descricao' => 'Simples Nacional: 202: Sem permissão de crédito, com cobr ICMS por Subst Trib',
                'codigo' => '202'
            ),
            ARRAY(
                'descricao' => 'Simples Nacional: 203: Isenção ICMS p\/ faixa de receita bruta e cobr do ICMS por ST',
                'codigo' => '203'
            ),
            ARRAY(
                'descricao' => 'Simples Nacional: 300: Imune',
                'codigo' => '300'
            ),
            ARRAY(
                'descricao' => 'Simples Nacional: 400: Não tributada',
                'codigo' => '400'
            ),
            ARRAY(
                'descricao' => 'Simples Nacional: 500: ICMS cobrado antes por subst trib ou antecipação',
                'codigo' => '500'
            ),
            ARRAY(
                'descricao' => 'Simples Nacional: 900: Outros',
                'codigo' => '900'
            )
        ];

        DB::table('produto_icms')->insert($icms);
    }
}
