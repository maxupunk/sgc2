<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Rua;

class RuasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Rua::create(array('nome' => 'Pedro carmo', 'bairro_id' => 1));
    }
}
