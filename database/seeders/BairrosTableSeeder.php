<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Bairro;

class BairrosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Bairro::create(array('nome' => 'centro', 'cidade_id' => 1));
    }
}
