<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Acao;

class AcoesTableSeeder extends Seeder
{

    var $ListaDeRotaBasicas = "categorias.listing|cidades.listing|estados.listing|formapgs.listing|logout|marcas.listing|medidas.listing|"
    ."parcelas.get|perfis.listing|pessoas.search|produtofiscal.index|produtofiscal.cest|produtofiscal.cfop|produtofiscal.icms|produtofiscal.ipi|"
    ."produtofiscal.ncm|produtofiscal.origem|produtofiscal.piscofins|produtofiscal.tipo|produtos.search|ruas.listing|bairros.listing|serials.index|".
    "serials.store|serials.listing|serials.show|serials.update|serials.destroy|tags.index|tags.store|tags.search|tags.show|tags.update|tags.destroy|".
    "usuarios.lista|notificacoes.index|notificacoes.marcaTodasComoLidas|notificacoes.deleteTodas|notificacoes.marcaComoLida|notificacoes.delete";

    public function run()
    {
        Acao::create(['nome' => 'Comandos Basicos', 'metodo' => $this->ListaDeRotaBasicas , 'titulo' => 'Basicos']);
        //
        Acao::create(['nome' => 'Acesso total ao sistema', 'metodo' => '*', 'titulo' => 'Administrador']);
        //
        Acao::create(['nome' => 'Dashboard', 'metodo' => 'dashboard.index', 'titulo' => 'Pagina principal']);

        Acao::create(['nome' => 'Listagem', 'metodo' => 'avarias.index', 'titulo' => 'Avarias']);
        Acao::create(['nome' => 'Adicionar', 'metodo' => 'avarias.store', 'titulo' => 'Avarias']);
        Acao::create(['nome' => 'Alterar', 'metodo' => 'avarias.show|avarias.update', 'titulo' => 'Avarias']);
        Acao::create(['nome' => 'Excluir', 'metodo' => 'avarias.destroy', 'titulo' => 'Avarias']);

        Acao::create(['nome' => 'Criar', 'metodo' => 'backups.index|backups.store', 'titulo' => 'Backups']);
        Acao::create(['nome' => 'Download', 'metodo' => 'backups.index|backups.show', 'titulo' => 'Backups']);
        Acao::create(['nome' => 'Restourar', 'metodo' => 'backups.index|backups.update', 'titulo' => 'Backups']);
        Acao::create(['nome' => 'Excluir', 'metodo' => 'backups.index|backups.destroy', 'titulo' => 'Backups']);

        Acao::create(['nome' => 'Listagem', 'metodo' => 'bairros.index', 'titulo' => 'Bairros']);
        Acao::create(['nome' => 'Adicionar', 'metodo' => 'bairros.store', 'titulo' => 'Bairros']);
        Acao::create(['nome' => 'Alterar', 'metodo' => 'bairros.index|bairros.show|bairros.update', 'titulo' => 'Bairros']);
        Acao::create(['nome' => 'Excluir', 'metodo' => 'bairros.index|bairros.destroy', 'titulo' => 'Bairros']);

        Acao::create(['nome' => 'listagem', 'metodo' => 'categorias.index', 'titulo' => 'Categorias']);
        Acao::create(['nome' => 'Adicionar', 'metodo' => 'categorias.store', 'titulo' => 'Categorias']);
        Acao::create(['nome' => 'Alterar', 'metodo' => 'categorias.show|categorias.update', 'titulo' => 'Categorias']);
        Acao::create(['nome' => 'Detalhar', 'metodo' => 'categorias.show', 'titulo' => 'Categorias']);
        Acao::create(['nome' => 'Excluir', 'metodo' => 'categorias.destroy', 'titulo' => 'Categorias']);

        Acao::create(['nome' => 'Listagem', 'metodo' => 'cidades.index', 'titulo' => 'Cidades']);
        Acao::create(['nome' => 'Adicionar', 'metodo' => 'cidades.store', 'titulo' => 'Cidades']);
        Acao::create(['nome' => 'Alterar', 'metodo' => 'cidades.index|cidades.show|cidades.update', 'titulo' => 'Cidades']);
        Acao::create(['nome' => 'Excluir', 'metodo' => 'cidades.destroy', 'titulo' => 'Cidades']);

        Acao::create(['nome' => 'Listagem', 'metodo' => 'compras.index', 'titulo' => 'Compras']);
        Acao::create(['nome' => 'Adicionar', 'metodo' => 'compras.store', 'titulo' => 'Compras']);
        Acao::create(['nome' => 'Alterar', 'metodo' => 'compras.update', 'titulo' => 'Compras']);
        Acao::create(['nome' => 'Detalhar', 'metodo' => 'ordens.show', 'titulo' => 'Compras']);
        Acao::create(['nome' => 'Excluir', 'metodo' => 'compras.destroy', 'titulo' => 'Compras']);
        Acao::create(['nome' => 'Faturar', 'metodo' => 'ordens.show|compras.faturar', 'titulo' => 'Compras']);

        Acao::create(['nome' => 'Listagem de ', 'metodo' => 'estoques.index', 'titulo' => 'Estoque']);
        Acao::create(['nome' => 'Alterar quant/valor', 'metodo' => 'estoques.update', 'titulo' => 'Estoque']);
        Acao::create(['nome' => 'Ver historico', 'metodo' => 'estoques.show', 'titulo' => 'Estoque']);

        Acao::create(['nome' => 'Listagem', 'metodo' => 'financeiros.index', 'titulo' => 'Receita/despesa']);
        Acao::create(['nome' => 'Adicionar', 'metodo' => 'financeiros.store', 'titulo' => 'Receita/despesa']);
        Acao::create(['nome' => 'Alterar', 'metodo' => 'financeiros.show|financeiros.pessoa|financeiros.update', 'titulo' => 'Receita/despesa']);
        Acao::create(['nome' => 'Excluir', 'metodo' => 'financeiros.destroy', 'titulo' => 'Receita/despesa']);
        Acao::create(['nome' => 'Baixar', 'metodo' => 'financeiros.show|financeiros.baixar|financeiros.pessoa|financeiros.pessoa.baixar', 'titulo' => 'Receita/despesa']);
        Acao::create(['nome' => 'Relatorio', 'metodo' => 'financeiros.relatorio', 'titulo' => 'Receita/despesa']);

        Acao::create(['nome' => 'Listagem', 'metodo' => 'formapgs.index', 'titulo' => 'Formas de pagamento']);
        Acao::create(['nome' => 'Adicionar', 'metodo' => 'formapgs.store', 'titulo' => 'Formas de pagamento']);
        Acao::create(['nome' => 'Alterar', 'metodo' => 'formapgs.show|formapgs.update', 'titulo' => 'Formas de pagamento']);
        Acao::create(['nome' => 'Excluir', 'metodo' => 'formapgs.destroy', 'titulo' => 'Formas de pagamento']);
        Acao::create(['nome' => 'Parcelamentos', 'metodo' => 'parcelas.index|parcelas.store|parcelas.show|parcelas.update|parcelas.destroy', 'titulo' => 'Formas de pagamento']);

        Acao::create(['nome' => 'Listagem', 'metodo' => 'marcas.index', 'titulo' => 'Marcas']);
        Acao::create(['nome' => 'Adicionar', 'metodo' => 'marcas.store', 'titulo' => 'Marcas']);
        Acao::create(['nome' => 'Alterar', 'metodo' => 'marcas.show|marcas.update', 'titulo' => 'Marcas']);
        Acao::create(['nome' => 'Excluir', 'metodo' => 'marcas.destroy', 'titulo' => 'Marcas']);

        Acao::create(['nome' => 'Listagem', 'metodo' => 'medidas.index', 'titulo' => 'Medidas']);
        Acao::create(['nome' => 'Adicionar', 'metodo' => 'medidas.store', 'titulo' => 'Medidas']);
        Acao::create(['nome' => 'Alterar', 'metodo' => 'medidas.show|medidas.update', 'titulo' => 'Medidas']);
        Acao::create(['nome' => 'Excluir', 'metodo' => 'medidas.destroy', 'titulo' => 'Medidas']);

        Acao::create(['nome' => 'Listagem', 'metodo' => 'perfis.index', 'titulo' => 'Perfis']);
        Acao::create(['nome' => 'Adicionar', 'metodo' => 'perfis.store|acoes.listing', 'titulo' => 'Perfis']);
        Acao::create(['nome' => 'Alterar', 'metodo' => 'perfis.show|perfis.update|acoes.listing', 'titulo' => 'Perfis']);
        Acao::create(['nome' => 'Excluir', 'metodo' => 'perfis.destroy', 'titulo' => 'Perfis']);

        Acao::create(['nome' => 'Listagem', 'metodo' => 'pessoas.index|pessoas.link', 'titulo' => 'Pessoas']);
        Acao::create(['nome' => 'Adicionar', 'metodo' => 'pessoas.store', 'titulo' => 'Pessoas']);
        Acao::create(['nome' => 'Alterar', 'metodo' => 'pessoas.show|pessoas.update|pessoas.deleteToken', 'titulo' => 'Pessoas']);
        Acao::create(['nome' => 'Excluir', 'metodo' => 'pessoas.destroy', 'titulo' => 'Pessoas']);

        Acao::create(['nome' => 'Listagem', 'metodo' => 'produtos.index', 'titulo' => 'Produtos']);
        Acao::create(['nome' => 'Adicionar', 'metodo' => 'produtos.store', 'titulo' => 'Produtos']);
        Acao::create(['nome' => 'Alterar', 'metodo' => 'produtos.show|produtos.update', 'titulo' => 'Produtos']);
        Acao::create(['nome' => 'Excluir', 'metodo' => 'produtos.destroy', 'titulo' => 'Produtos']);

        Acao::create(['nome' => 'Listagem', 'metodo' => 'ruas.index', 'titulo' => 'Ruas']);
        Acao::create(['nome' => 'Adicionar', 'metodo' => 'ruas.store', 'titulo' => 'Ruas']);
        Acao::create(['nome' => 'Alterar', 'metodo' => 'ruas.show|ruas.update', 'titulo' => 'Ruas']);
        Acao::create(['nome' => 'Excluir', 'metodo' => 'ruas.destroy', 'titulo' => 'Ruas']);

        Acao::create(['nome' => 'Listagem', 'metodo' => 'servicos.index', 'titulo' => 'Ordem servico']);
        Acao::create(['nome' => 'Adicionar', 'metodo' => 'servicos.store|servicos.estender', 'titulo' => 'Ordem servico']);
        Acao::create(['nome' => 'Alterar', 'metodo' => 'ordens.show|servicos.update|ordens.reabrir', 'titulo' => 'Ordem servico']);
        Acao::create(['nome' => 'Faturar', 'metodo' => 'servicos.faturar', 'titulo' => 'Ordem servico']);
        Acao::create(['nome' => 'Excluir', 'metodo' => 'servicos.destroy', 'titulo' => 'Ordem servico']);

        Acao::create(['nome' => 'Listagem', 'metodo' => 'usuarios.index', 'titulo' => 'Usuários']);
        Acao::create(['nome' => 'Adicionar', 'metodo' => 'usuarios.store', 'titulo' => 'Usuários']);
        Acao::create(['nome' => 'alterar senha/email', 'metodo' => 'usuarios.show|usuarios.update|usuarios.deleteToken', 'titulo' => 'Usuários']);
        Acao::create(['nome' => 'Usuários alterar perfil', 'metodo' => 'usuarios.show|usuarios.perfil|usuarios.deleteTokenOutros', 'titulo' => 'Usuários']);
        Acao::create(['nome' => 'Excluir', 'metodo' => 'usuarios.destroy', 'titulo' => 'Usuários']);

        Acao::create(['nome' => 'Listagem', 'metodo' => 'vendas.index', 'titulo' => 'Vendas']);
        Acao::create(['nome' => 'Adicionar/alterar', 'metodo' => 'vendas.store|ordens.show', 'titulo' => 'Vendas']);
        Acao::create(['nome' => 'Excluir', 'metodo' => 'vendas.destroy', 'titulo' => 'Vendas']);

        Acao::create(['nome' => 'Listagem', 'metodo' => 'prestacaocontas.index', 'titulo' => 'Prestação de conta']);
        Acao::create(['nome' => 'Adicionar', 'metodo' => 'prestacaocontas.store|prestacaocontas.show', 'titulo' => 'Prestação de conta']);
        Acao::create(['nome' => 'Excluir', 'metodo' => 'prestacaocontas.destroy', 'titulo' => 'Prestação de conta']);

        Acao::create(['nome' => 'Financeiro', 'metodo' => 'financeiros.relatorio', 'titulo' => 'Relatorios']);
        Acao::create(['nome' => 'Odens Serviço/venda/compra', 'metodo' => 'ordens.relatorio', 'titulo' => 'Relatorios']);
    }
}
