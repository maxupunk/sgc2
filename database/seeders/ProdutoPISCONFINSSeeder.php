<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProdutoPISCONFINSSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pisconfins = [
            ARRAY(
                'descricao' => '00: Entrada com recuperação de crédito',
                'codigo' => '00'
            ),
            ARRAY(
                'descricao' => '01: Entrada tributada com alíquota zero',
                'codigo' => '01'
            ),
            ARRAY(
                'descricao' => '02: Entrada isenta',
                'codigo' => '02'
            ),
            ARRAY(
                'descricao' => '03: Entrada não-tributada',
                'codigo' => '03'
            ),
            ARRAY(
                'descricao' => '04: Entrada imune',
                'codigo' => '04'
            ),
            ARRAY(
                'descricao' => '05: Entrada com suspensão',
                'codigo' => '05'
            ),
            ARRAY(
                'descricao' => '49: Outras entradas',
                'codigo' => '49'
            ),
            ARRAY(
                'descricao' => '50: Saída tributada',
                'codigo' => '50'
            ),
            ARRAY(
                'descricao' => '51: Saída tributada com alíquota zero',
                'codigo' => '51'
            ),
            ARRAY(
                'descricao' => '52: Saída isenta',
                'codigo' => '52'
            ),
            ARRAY(
                'descricao' => '53: Saída não-tributada',
                'codigo' => '53'
            ),
            ARRAY(
                'descricao' => '54: Saída imune',
                'codigo' => '54'
            ),
            ARRAY(
                'descricao' => '55: Saída com suspensão',
                'codigo' => '55'
            ),
            ARRAY(
                'descricao' => '99: Outras saídas',
                'codigo' => '99'
            )
        ];

        DB::table('produto_pis_cofins')->insert($pisconfins);
    }
}
