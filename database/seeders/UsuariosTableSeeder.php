<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Usuario;
class UsuariosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Usuario::create([
            'email' => 'admin@admin.com.br',
            'nome' => 'Administrador',
            'senha' => '123456',
            'perfil_id' => 1,
            'ativo' => 1
        ]);
    }
}
