<?php

namespace Tests\Feature\Admin;

use App\Models\Usuario;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Hash;
use Laravel\Sanctum\PersonalAccessToken;
use Tests\TestCase;
use Illuminate\Support\Str;

class AutenticacaoControllerTest extends TestCase
{
    use RefreshDatabase;

    private $token;
    private $email;
    private $senha;
    private $usuario;

    // faz cria a auteticação e aloca os dados globais
    public function setUp(): void
    {
        parent::setUp();

        $this->email = Str::random(10) . "@email.com";
        $this->senha = Str::random(10);

        // cria um usuário
        $this->usuario = Usuario::factory()->create([
            'email' => $this->email,
            'senha' => bcrypt($this->senha),
            'ativo' => true,
        ]);

        // autentica o usuário e obtém o token de acesso
        $this->token = $this->usuario->createToken('Test Token')->plainTextToken;
    }

    public function testLoginComSucesso()
    {
        // faz uma requisição POST para o método login com as credenciais corretas
        $response = $this->postJson('api/admin/login', [
            'device_name' => "Linux x86_64",
            'lembrar' => false,
            'email' => $this->email,
            'senha' => $this->senha,
        ]);

        // verifica se a resposta contém um token de acesso
        $response->assertJsonStructure([
            'usuario',
            'token',
            'acl',
            'config',
        ]);

        // verifica se o token de acesso existe no banco de dados
        $personalAccessToken = PersonalAccessToken::findToken($response['token']);
        $this->assertNotNull($personalAccessToken);
    }

    /**
     * Testa o login com credenciais inválidas.
     *
     * @return void
     */
    public function testLoginComCredenciaisInvalidas()
    {
        // faz uma requisição POST para o método login com credenciais inválidas
        $response = $this->postJson('api/admin/login', [
            'device_name' => "Linux x86_64",
            'lembrar' => false,
            'email' => $this->email,
            'senha' => 'senha_errada',
        ]);

        // verifica se a resposta contém uma mensagem de erro
        $response->assertJson([
            'success' => false
        ]);
    }

    /**
     * Testa o logout de um usuário autenticado.
     *
     * @return void
     */
    public function testLogout()
    {

        // faz uma requisição POST para o método logout
        $response = $this->postJson('api/admin/logout', [], [
            'Authorization' => 'Bearer ' . $this->token,
        ]);

        // verifica se a resposta contém uma mensagem de sucesso
        $response->assertJson([
            'success' => true
        ]);

        // verifica se o token de acesso foi revogado
        $this->assertNull($this->usuario->tokens()->first());
    }
}
