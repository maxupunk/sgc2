<?php

namespace Tests\Feature;

use App\Models\Pessoa;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Illuminate\Support\Str;
use App\Models\Usuario;

class PessoaControllerTest extends TestCase
{
    //use RefreshDatabase;

    private $token;
    private $pessoaData;

    public function setUp(): void
    {
        parent::setUp();

        $senha = Str::random(10);
        $email = Str::random(10) . "@email.com";

        // cria um usuário
        $usuario = Usuario::factory()->create([
            'email' => $email,
            'senha' => bcrypt($senha),
            'ativo' => true,
        ]);

        // autentica o usuário e obtém o token de acesso
        $this->token = $usuario->createToken('Test Token')->plainTextToken;

        $this->pessoaData = Pessoa::factory()->create();
    }

    public function testIndex()
    {
        // faz uma requisição 
        $response = $this->get('/api/admin/pessoas', [
            'Authorization' => 'Bearer ' . $this->token,
        ]);

        // Assert that the response is successful and contains the expected JSON data
        $response->assertStatus(200)
            ->assertJsonCount(1, 'data')
            ->assertJsonStructure([
                'total',
                'data',
                'perPage',
                'lastPage',
            ]);
    }

    public function TestCadastroPessoa()
    {

        $response = $this->json('POST', '/api/admin/pessoas', $this->pessoaData->toArray(), [
            'Authorization' => 'Bearer ' . $this->token,
        ]);

        $response->assertStatus(200)
            ->assertJson([
                'success' => true,
                'type' => 'success'
            ]);
    }

    public function testPegarPessoa()
    {
        $response = $this->json('GET', '/api/admin/pessoas/' . $this->pessoaData->id, [], [
            'Authorization' => 'Bearer ' . $this->token,
        ]);

        $response->assertStatus(200)
            ->assertJsonFragment([
                'id' => $this->pessoaData->id,
                'nome' => $this->pessoaData->nome
            ]);
    }

    public function testAtualizarPessoa()
    {
        $novaPessoa = Pessoa::factory()->make()->toArray();

        $novaPessoa['contatos'] = [
            [
                "contato" => "(88)8 8888-8888",
                "created_at" => "2021-05-11",
                "tags" => ["whatsapp"]
            ]
        ];

        $response = $this->json('PUT', '/api/admin/pessoas/' . $this->pessoaData->id, $novaPessoa, [
            'Authorization' => 'Bearer ' . $this->token,
        ]);

        $response->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);
    }

    public function testDeletaPessoa()
    {
        $response = $this->json('DELETE', '/api/admin/pessoas/' . $this->pessoaData->id, [], [
            'Authorization' => 'Bearer ' . $this->token,
        ]);
        $response->assertStatus(200)
            ->assertJson([
                'success' => true
            ]);
    }
}
