<b>Sistema de Gerenciamneto Comercial em laravel</b>

Esse sistema foi desenvolvido para uma pequena assistencia tecnica de manutenção de computadores e perifericos com controle de estoque, <br>
ordem de serviço, prestação de contas de funcinario, comissão de vendas, app para os clientes e etc esta em construção e temos bastantes<br>
melhorias a fazer, porem já esta em produção.<br>
O sistema é opensource e está aqui para quem quiser usar.

<b>- INSTALAÇÃO</b>
<ol>
    <li>configurar o .env com os dados da conexão (obs.: o sqlite ainda não é totalmente compativel)</li>
    <li>Em caso de usar o APP para o cliente:
        <li>Edite o arquivo resources/app/.env e altera o VUE_APP_URL_API com a URL da API.</li>
        <li>Coloque a pasta public/app em um subdominio.</li>
        <li>Edite o resource/admin/.env e coloque em VUE_APP_URL_APP o subdominio criado anteriomente.</li>
    </li>
</ol>

<b>- PENDENCIAS</b>
<ol>
<li>
    <li>Colocar todas imagens em uma tabela poliformica</li>
    <li>Reabrir venda e excluir dentro do cart</li>
    <li>Fazer o recibo de serviço com as observações</li>
    <li>Adiciona dentro da ordem de serviço uma "lista de a fazeres/feito"</li>
    <li>Criar uma forma de reabrir a ordem de venda</li>
    <li>Adicionar lista de clientes esperando produto novo ou usado</li>
    <li>Colocar menu de configuração para configurar notificações</li>
    <li>Concluir sistema de notificação</li>
    <li>Coloca no fechamento o quanto falta receber no nome de cada pessoa</li>
    <li>Refazer os relatorios</li>
    <li>Imagem do v-app-bar fazer o armazenamento no servidor</li>
    <li>Fazer recibo de serviço com obs (PDF)</li>
    <li>Exporta PDF de OS</li>
    <li>Sistema de login difinitivo ou não</li>
    <li>Sistema de kit auto relacionamento muitos para muitos (Tabela estoque > estque_estoque > estoque)</li>
    <li>Fechamento de caixa</li>
    <li>Fluxo de caixa</li>
    <li>Adicionar hashtag de defeito relatado</li>
    <li>QR CODE em tudo que for possivel e colocar para lê pelo app</li>
    <li>Criar o sistema de fluxograma</li>
    <li>Verificador de itens vendidos a baixo do preço de compra</li>
    <li>Relatorio de ordem de serviço por produto</li>
    <li>Cotação de produtos por fornecedor (podendo ser separado)</li>
    <li>Configurações</li>
    <li>Criar o processo de pagamento da comição em um "caixa" histórico de pagamento para quando ouvir a reabertura ou retorno o pagamento voltar</li>
</ol>

<b>- Bugs pendentes</b>
<ol>
    <li>Em pessoa form não grava a excluisã de contatos</li>
    <li>mascaras em cadastro de pessoas contato</li>
    <li>Leitor de código de barras em produtos>estoque só está funcionando QR code e o código de barras não funciona</li>
</ol>
