import Vue from 'vue'
import VueRouter from 'vue-router'

import Dashboard from '../components/home/dashboard'
//
import ProdutoIndex from '../components/produto/ProdutoIndex'
import ProdutoForm from '../components/produto/ProdutoForm'
//
import PessoaIndex from '../components/pessoa/PessoaIndex'
import PessoaForm from '../components/pessoa/PessoaForm'
//
import PrestacaoContaIndex from '../components/prestacaoconta/PrestacaoContaIndex'
import PrestacaoContaForm from '../components/prestacaoconta/PrestacaoContaForm'
//
import CategoriaIndex from '../components/categoria/CategoriaIndex'
//
import MedidaIndex from '../components/medida/MedidaIndex'
//
import MarcaIndex from '../components/marca/MarcaIndex'
//
import FormapgIndex from '../components/formapg/FormapgIndex'
//
import ParcelaIndex from '../components/parcela/ParcelaForm'
//
import VendaCart from '../components/venda/VendaCart'
import VendaFaturar from '../components/venda/VendaFaturar'
import VendaIndex from '../components/venda/VendaIndex'
//
import ServicoIndex from '../components/servico/ServicoIndex'
import ServicoFaturar from '../components/servico/ServicoFaturar'
import ServicoForm from '../components/servico/ServicoForm'
import ServicoNovo from '../components/servico/ServicoNovo'
import ServicoEstender from '../components/servico/ServicoEstender'
//
import CompraIndex from '../components/compra/CompraIndex'
import CompraCart from '../components/compra/CompraCart'
import CompraFaturar from '../components/compra/CompraFaturar'
//
import FinanceiroIndex from '../components/financeiro/FinanceiroIndex'
import FinanceiroForm from '../components/financeiro/FinanceiroForm'
import FinanceiroBaixa from '../components/financeiro/FinanceiroBaixa'
import FinanceiroPessoa from '../components/financeiro/FinanceiroPessoa.vue'
import FinanceiroRelatorio from '../components/financeiro/FinanceiroRelatorio'
//
import EstoqueForm from '../components/estoque/EstoqueIndex'
import EstoqueRelatorio from '../components/estoque/EstoqueRelatorio'
//
import AvariaIndex from '../components/avaria/AvariaIndex'
import AvariaForm from '../components/avaria/AvariaForm'
//
import PerfilIndex from '../components/perfil/PerfilIndex'
import PerfilForm from '../components/perfil/PerfilForm'
//
import UsuarioIndex from '../components/usuario/UsuarioIndex'
import UsuarioForm from '../components/usuario/UsuarioForm'
//
import BairroIndex from '../components/bairro/BairroIndex'
//
import RuaIndex from '../components/rua/RuaIndex'
//
import Configuracoes from '../components/config/ConfigIndex'
//
import OrdemRelatorio from '../components/ordem/OrdemRelatorio'
//
import NotificacaoIndex from '../components/notificacao/NotificacaoIndex'
//
import BackupIndex from '../components/backup/BackupIndex.vue'

Vue.use(VueRouter)

const routes = [
    { path: '/', component: Dashboard },
    { path: '/dashboard', component: Dashboard },
    ///// produto ////////
    { path: '/produtos', component: ProdutoIndex },
    { path: '/produtos/create', component: ProdutoForm },
    { path: '/produtos/:id', component: ProdutoForm },
    ///// pessoa ////////
    { path: '/pessoas', component: PessoaIndex },
    { path: '/pessoas/create', component: PessoaForm },
    { path: '/pessoas/:id', component: PessoaForm },
    ///// Prestacao de conta ////////
    { path: '/prestacaocontas', component: PrestacaoContaIndex },
    { path: '/prestacaocontas/create', component: PrestacaoContaForm },
    ///// categoria ////////
    { path: '/categorias', component: CategoriaIndex },
    ///// medidas ////////
    { path: '/medidas', component: MedidaIndex },
    ///// marcas ////////
    { path: '/marcas', component: MarcaIndex },
    ///// formapgs ////////
    { path: '/formapgs', component: FormapgIndex },
    ///// parcelas ////////
    { path: '/parcelas', component: ParcelaIndex },
    ///// Relatorio de ordens ////////
    { path: '/ordens/relatorio', component: OrdemRelatorio },
    ///// vendas ////////
    { path: '/vendas', component: VendaIndex },
    { path: '/vendas/cart', component: VendaCart },
    { path: '/vendas/cart/:id', component: VendaCart },
    { path: '/vendas/faturar', component: VendaFaturar },
    ///// ordens de servicos ////////
    { path: '/servicos', component: ServicoIndex },
    { path: '/servicos/create', component: ServicoNovo },
    { path: '/servicos/estender/:id', component: ServicoEstender },
    { path: '/servicos/faturar/:id', component: ServicoFaturar },
    { path: '/servicos/:id', component: ServicoForm },
    ///// compras ///////
    { path: '/compras', component: CompraIndex },
    { path: '/compras/cart', component: CompraCart },
    { path: '/compras/cart/:id', component: CompraCart },
    { path: '/compras/faturar', component: CompraFaturar },
    ///// Financeiras ///////
    { path: '/financeiros', component: FinanceiroIndex },
    { path: '/financeiros/create', component: FinanceiroForm },
    { path: '/financeiros/relatorio', component: FinanceiroRelatorio },
    { path: '/financeiros/:id', component: FinanceiroForm },
    { path: '/financeiros/baixa/:id', component: FinanceiroBaixa },
    { path: '/financeiros/pessoa/:id', component: FinanceiroPessoa },
    ///// Estoques ///////
    { path: '/estoques', component: EstoqueForm },
    { path: '/estoques/relatorio', component: EstoqueRelatorio },
    ///// Avaria ////////
    { path: '/avarias', component: AvariaIndex },
    { path: '/avarias/create', component: AvariaForm },
    { path: '/avarias/:id', component: AvariaForm },
    ///// Perfil ////////
    { path: '/perfis', component: PerfilIndex },
    { path: '/perfis/create', component: PerfilForm },
    { path: '/perfis/:id', component: PerfilForm },
    ///// Usuario ////////
    { path: '/usuarios', component: UsuarioIndex },
    { path: '/usuarios/create', component: UsuarioForm },
    { path: '/usuarios/:id', component: UsuarioForm },
    ///// bairro ////////
    { path: '/bairros', component: BairroIndex },
    ///// rua ////////
    { path: '/ruas', component: RuaIndex },
    ///// configurações ////////
    { path: '/configuracoes', component: Configuracoes },
    ///// configurações ////////
    { path: '/notificacoes', component: NotificacaoIndex },
    ///// Sistema de backup ////////
    { path: '/backups', component: BackupIndex },
]

const router = new VueRouter({
    mode: 'history',
    base: '/',
    routes
})

export default router
