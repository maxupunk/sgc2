/* eslint-disable no-console */

import { register } from 'register-service-worker'
import store from './store'

if (process.env.NODE_ENV === 'production') {
  register(`${process.env.BASE_URL}service-worker.js`, {
    ready() {
      console.log(
        'O aplicativo está sendo servido do cache pelo service worker.\n' +
        'Para mais detalhes, visite https://goo.gl/AFskqB'
      )
    },
    registered() {
      let message = 'O service worker foi registrado.'
      store.dispatch('AddSnackbar', { message: message });
      console.log(message )
    },
    cached() {
      let message = 'O conteúdo foi armazenado em cache para uso offline.'
      store.dispatch('AddSnackbar', { message: message });
      console.log(message )
    },
    updatefound() {
      let message = 'Novo conteúdo está sendo baixado.'
      store.dispatch('AddSnackbar', { message: message });
      console.log(message )
    },
    updated() {
      let message = 'Novo conteúdo está disponível; por favor, atualize.'
      store.dispatch('AddSnackbar', { message: message });
      console.log(message)
    },
    offline() {
      let message = "Nenhuma conexão com a internet encontrada. O aplicativo está sendo executado no modo offline."
      store.dispatch('AddSnackbar', { message: message });
      console.log(message)
    },
    error(error) {
      console.error('Erro durante o registro do service worker:', error)
    }
  })
}
