module.exports = (function (value) {
    let estatus = {
        'EA': 'Aberta',
        'PD': 'Pendente',
        'OR': 'Orçamento',
        'AA': 'Aguardando aprovação',
        'PA': 'Pagamento em análise',
        'AP': 'Aguardando pagamento',
        'EE': 'Enviada',
        'RP': 'Entregue parcialmente',
        'RE': 'Entregue',
        'CC': 'Concluída',
        'CA': 'Cancelada'
    }
    return estatus[value]
})
