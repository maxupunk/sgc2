module.exports = (function (value) {

    function unformat(input, precision = 2) {
        //var negative = input.indexOf('-') >= 0 ? -1 : 1
        var numbers = onlyNumbers(input)
        var currency = numbersToCurrency(numbers, precision)
        return parseFloat(currency) //* negative
    }

    function toStr(value) {
        return value ? value.toString() : ''
    }

    function onlyNumbers(input) {
        return toStr(input).replace(/\D+/g, '') || '0'
    }

    function numbersToCurrency(numbers, precision) {
        var exp = Math.pow(10, precision)
        var float = parseFloat(numbers) / exp
        return float.toFixed(fixed(precision))
    }

    function fixed(precision) {
        return between(0, precision, 20)
    }

    function between(min, n, max) {
        return Math.max(min, Math.min(n, max))
    }

    ValorRetorn = (typeof value === 'string') ? unformat(value) : value

    if (ValorRetorn == null || isNaN(ValorRetorn)) {
        return null
    } else {
        let RetornoMask = parseFloat(ValorRetorn).toLocaleString('pt-BR', {
            minimumFractionDigits: 2,
            maximumFractionDigits: 2
        })
        return RetornoMask
    }
})
