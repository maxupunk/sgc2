module.exports = (function(input) {
    let str = input+ '';
    str = str.replace(/\D/g,'');
    if(str.length === 11 ){
        str=str.replace(/^(\d{2})(\d{1})(\d{4})(\d{4})/,'($1) $2 $3-$4');
    }else{
        str=str.replace(/^(\d{2})(\d{4})(\d{4})/,'($1) $2-$3');
    }
    return str;
})