import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import VueResource from 'vue-resource'
import vuetify from './plugins/vuetify'
import VueChartkick from 'vue-chartkick'
import Chart from 'chart.js'
import VCurrencyField from './plugins/VuetifyCurrencyField';
import VueMoment from 'vue-moment';
import VueMask from 'v-mask';

Vue.use(VueMoment);
Vue.use(VueResource);
Vue.use(VueMask);

Vue.http.options.root = store.getters.GetUrlRoot
Vue.http.options.credentials = true;

Vue.use(VueChartkick, {
    adapter: Chart
})

Vue.filter('person', require('./filters/person'));
Vue.filter('currency', require('./filters/currency'));
//Vue.filter('documento', require('./filters/documento'));
Vue.filter('telefone', require('./filters/telefone'));
Vue.filter('ativo', require('./filters/ativo'));
Vue.filter('float', require('./filters/float'));
Vue.filter('statusOrdem', require('./filters/statusOrdem'));
Vue.filter('tipoOrdem', require('./filters/tipoOrdem'));
Vue.filter('statusFinanceiro', require('./filters/statusFinanceiro'));
Vue.filter('nature', require('./filters/nature'));
Vue.filter('bytes', require('./filters/bytes'));

Vue.http.interceptors.push((request, next) => {
    if (store.getters.GetToken) {
        Vue.http.headers.common['Authorization'] = `Bearer ${store.getters.GetToken}`
    }
    store.dispatch('SetLoadingStatus', true)
    next(response => {
        store.dispatch('SetLoadingStatus', false)
        switch (response.status) {
            case 0:
                store.dispatch('AddSnackbar', {
                    message: "Servidor fora de alcance", type: "error",
                })
                break;
            case 401:
                store.dispatch('AddSnackbar', response.body)
                if (response.body.message == "Unauthenticated.") {
                    store.dispatch('destroyToken')
                }
                break;
            case 419:
                store.dispatch('AddSnackbar', {
                    message: "Acabou o tempo de autenticação!",
                });
                break;
            case 500:
                let mensagem = response.body.message ? response.body.message : "O servidor encontrou uma situação com a qual não sabe lidar."
                let typo = response.body.type ? response.body.type : null
                store.dispatch('AddSnackbar', {
                    type: typo,
                    message: mensagem,
                });
                break;
            default:
                if (response.body.message) {
                    store.dispatch('AddSnackbar', response.body)
                }
                break;
        }
    })
});

store.dispatch('GetConfigImageBackgroud')

// registra o service worker
if ('serviceWorker' in navigator) {
    window.addEventListener('load', () => {
      navigator.serviceWorker.register('/service-worker.js').then(registration => {
        console.log('Service worker registrado: ', registration)
      }).catch(registrationError => {
        console.log('Registro do service worker falhou: ', registrationError)
      })
    })
  }

new Vue({
    router,
    store,
    vuetify,
    VCurrencyField,
    render: function (h) { return h(App) }
}).$mount('#app')
