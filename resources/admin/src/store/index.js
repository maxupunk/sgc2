import VuexPersistence from 'vuex-persist'
import dashboard from './modules/dashboard'
import produto from './modules/produto'
import categoria from './modules/categoria'
import medida from './modules/medida'
import marca from './modules/marca'
import pessoa from './modules/pessoa'
import prestacaoconta from './modules/prestacaoconta'
import estado from './modules/estado'
import cidade from './modules/cidade'
import bairro from './modules/bairro'
import rua from './modules/rua'
import formapg from './modules/formapg'
import parcela from './modules/parcela'
import venda from './modules/venda'
import servico from './modules/servico'
import compra from './modules/compra'
import ordem from './modules/ordem'
import financeiro from './modules/financeiro'
import estoque from './modules/estoque'
import avaria from './modules/avaria'
import usuario from './modules/usuario'
import perfil from './modules/perfil'
import acoes from './modules/acoes'
import autenticacao from './modules/autenticacao'
import tag from './modules/tag'
import fiscal from './modules/fiscal'
import config from './modules/config'
import geral from './modules/geral'
import notificacoes from './modules/notificacoes'
import backup from './modules/backup'

const vuexLocal = new VuexPersistence({
    key: 'sgc-admin',
    storage: window.localStorage,
})


var VuexStore = {
    modules: {
        dashboard,
        produto,
        categoria,
        medida,
        marca,
        pessoa,
        prestacaoconta,
        estado,
        cidade,
        bairro,
        rua,
        formapg,
        parcela,
        venda,
        servico,
        compra,
        ordem,
        financeiro,
        estoque,
        avaria,
        usuario,
        perfil,
        acoes,
        autenticacao,
        tag,
        fiscal,
        config,
        geral,
        notificacoes,
        backup
    },
    plugins: [vuexLocal.plugin]
}

import Vue from 'vue';
import Vuex from 'vuex'
Vue.use(Vuex)
const store = new Vuex.Store(VuexStore)

export default store
