import Vue from 'vue';

const urlAPI = 'api/admin/vendas';

// initial state
const state = {
    vendas: [],
    row: {
        acrescimo: 0,
        desconto: 0,
        frete: 0
    },
    itens: [],
    updateList: false
}

// getters
const getters = {
    GetVendas: state => state.vendas,
    GetVenda: state => state.row,
    GetVendasUpdateLista: state => state.updateList
}

// mutations
const mutations = {
    VENDAS_ADD(state, data) {
        state.vendas = data
    },
    VENDAS_UPDATE(state, data) {
        data.filter((item) => {
            if (state.vendas.data.findIndex(x => x.id === item.id) == -1) {
                state.vendas.data.push(item)
            }
        })
    },
    VENDA_UPDATE_LISTA(state, data) {
        state.updateList = data
    }
}

// actions
const actions = {
    async getVendas({ commit }, pagination) {
        const response = (await Vue.http.get(urlAPI, { params: pagination }))
        if (pagination.page == 1) {
            commit('VENDAS_ADD', response.body)
        } else {
            commit('VENDAS_UPDATE', response.body.data)
        }
        commit('VENDA_UPDATE_LISTA', false)
    },

    async saveVenda({ commit }, params) {
        commit('VENDA_UPDATE_LISTA', true)
        return await Vue.http.post(urlAPI, params)
    },

    async DeleteVenda({ commit }, id) {
        commit('VENDA_UPDATE_LISTA', true)
        return (await Vue.http.delete(`${urlAPI}/${id}`))
    },
}

export default {
    state,
    getters,
    actions,
    mutations
}
