import Vue from 'vue';

const urlAPI = 'api/admin/dashboard';

// initial state
const state = {
    dashboard: [],
}

// getters
const getters = {
    GetDashboard: state => state.dashboard,
}

// mutations
const mutations = {
    updateDashboard(state, data) {
        state.dashboard = data
    },
}

// actions
const actions = {
    async getDashboard({ commit }) {
        const result = (await Vue.http.get(`${urlAPI}`))
        commit('updateDashboard', result.data)
    }
}

export default {
    state,
    getters,
    actions,
    mutations
}
