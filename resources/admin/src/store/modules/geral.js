import Vuetify from '../../plugins/vuetify'

// initial state
const state = {
    snackbar: [],
    loading: false
}

// getters
const getters = {
    GetSnackbarMsg: state => state.snackbar,
    GetLoadingStatus: state => state.loading,
}

// mutations
const mutations = {

    LOG_ADD_SNACKBAR_LIST(state, data) {
        state.snackbar.push(data)
    },

    LOG_REMOVE_SNACKBAR_LIST(state, item) {
        let Index = state.snackbar.indexOf(item)
        state.snackbar.splice(Index, 1)
    },

    LOG_SET_LOADING_STATUS(state, status) {
        state.loading = status
    },
}

// actions
const actions = {
    AddSnackbar({ commit }, mensagem) {
        commit('LOG_ADD_SNACKBAR_LIST', mensagem)
    },

    RemoveSnackbar({ commit }, item) {
        commit('LOG_REMOVE_SNACKBAR_LIST', item)
    },

    SetLoadingStatus({ commit }, status) {
        commit('LOG_SET_LOADING_STATUS', status)
    },

    aplicaTema({ getters }, themesValor = null) {
        let themes = (themesValor == null) ? getters.GetUsuarioConfig.themes : themesValor;

        if (themes) {
            Object.entries(themes).forEach(([theme, cores]) => {
                if (Vuetify.framework.theme.themes[theme] !== undefined && cores.length !== 0) {
                    Object.entries(cores).forEach(([key, cor]) => {
                        Vuetify.framework.theme.themes[theme][key] = cor.hex
                    })
                }
            });

            Vuetify.framework.theme.isDark = themes.isDark

            var appleThemeColor = document.querySelector('meta[name="apple-mobile-web-app-status-bar-style"]');
            var ThemeColor = document.querySelector('meta[name="theme-color"]');

            if (themes.isDark) {
                appleThemeColor.setAttribute("content", themes.dark.primary.hex);
                ThemeColor.setAttribute("content", themes.dark.primary.hex);
            } else {
                appleThemeColor.setAttribute("content", themes.light.primary.hex);
                ThemeColor.setAttribute("content", themes.light.primary.hex);
            }
        }

    },

}

export default {
    state,
    getters,
    actions,
    mutations
}
