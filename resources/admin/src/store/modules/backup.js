import Vue from 'vue';

const urlAPI = 'api/admin/backups';

// initial state
const state = {
    backups: [],
    list: [],
    progress: 0
}

// getters
const getters = {
    GetBackups: state => state.backups,
    ListBackup: state => state.list,
    GetBackup: state => state.row,
    GetBackupProgress: state => state.progress,
}

// mutations
const mutations = {
    BACKUPS_ADD(state, data) {
        state.backups = data
    },
    BACKUPS_UPDATE(state, data) {
        data.filter((item) => {
            if (state.backups.data.findIndex(x => x.id === item.id) == -1) {
                state.backups.data.push(item)
            }
        })
    },
    BACKUP_LIST(state, data) {
        state.list = data
    },

    BACKUP_PROGRSS(state, data) {
        state.progress = data
    },
}

// actions
const actions = {
    async getBackups({ commit }, pagination) {
        const response = (await Vue.http.get(urlAPI, { params: pagination }))
        if (pagination.page == 1) {
            commit('BACKUPS_ADD', response.body)
        } else {
            commit('BACKUPS_UPDATE', response.body.data)
        }
    },

    downloadBackup({ commit }, id) {
        commit('BACKUP_PROGRSS', 0)
        return Vue.http.get(`${urlAPI}/${id}`, {
            progress(e) {
                if (e.lengthComputable) {
                    let porcentagem = Math.round(e.loaded / e.total * 100)
                    commit('BACKUP_PROGRSS', porcentagem)
                    if (porcentagem >= 100) {
                        commit('BACKUP_PROGRSS', 0)
                    }
                }
            }
        })
    },

    async makeBackup({ commit }) {
        return await Vue.http.post(urlAPI)
    },

    async restaureBackup({ commit }, id) {
        return await Vue.http.put(`${urlAPI}/${id}`)
    },

    async deleteBackup({ commit }, id) {
        return (await Vue.http.delete(`${urlAPI}/${id}`))
    },
}

export default {
    state,
    getters,
    actions,
    mutations
}
