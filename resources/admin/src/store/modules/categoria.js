import Vue from 'vue';

const urlAPI = 'api/admin/categorias';

// initial state
const state = {
    categorias: [],
    list: [],
    row: {}
}

// getters
const getters = {
    GetCategorias: state => state.categorias,
    ListCategoria: state => state.list,
    GetCategoria: state => state.row,
}

// mutations
const mutations = {
    CATEGORIAS_ADD(state, data) {
        state.categorias = data
    },
    CATEGORIAS_UPDATE(state, data) {
        data.filter((item) => {
            if (state.categorias.data.findIndex(x => x.id === item.id) == -1) {
                state.categorias.data.push(item)
            }
        })
    },
    CATEGORIA_LIST(state, data) {
        state.list = data
    },
    CATEGORIA_ROW(state, data) {
        state.row = data
    }
}

// actions
const actions = {
    async getCategorias({ commit }, pagination) {
        const response = (await Vue.http.get(urlAPI, { params: pagination }))
        if (pagination.page == 1) {
            commit('CATEGORIAS_ADD', response.body)
        } else {
            commit('CATEGORIAS_UPDATE', response.body.data)
        }
    },

    async getCategoria({ commit }, id) {
        const result = (await Vue.http.get(`${urlAPI}/${id}`))
        commit('CATEGORIA_ROW', result.data)
    },

    async saveCategoria({ commit }, params) {
        if (params.id) {
            return await Vue.http.put(`${urlAPI}/${params.id}`, params)
        } else {
            return await Vue.http.post(urlAPI, params)
        }
    },

    async deleteCategoria({ }, id) {
        return (await Vue.http.delete(`${urlAPI}/${id}`))
    },

    async cleanCategoria({ commit }) {
        commit('CATEGORIA_ROW', {})
    },

    async listCategoria({ commit }) {
        const result = (await Vue.http.get(`${urlAPI}/listing`))
        commit('CATEGORIA_LIST', result.data)
    }
}

export default {
    state,
    getters,
    actions,
    mutations
}
