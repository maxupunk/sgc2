import Vue from 'vue';

const urlAPI = 'api/admin/compras';

// initial state
const state = {
    compras: {},
    row: {
        acrescimo: 0,
        desconto: 0,
        frete: 0
    },
    pdateList: false
}

// getters
const getters = {
    GetCompras: state => state.compras,
    GetCompra: state => state.row,
    getUpdateList: state => state.updateList
}

// mutations
const mutations = {
    COMPRAS_ADD(state, data) {
        state.compras = data
    },
    COMPRAS_UPDATE(state, data) {
        data.filter((item) => {
            if (state.compras.data.findIndex(x => x.id === item.id) == -1) {
                state.compras.data.push(item)
            }
        })
    },
    COMPRA_ROW(state, data) {
        state.row = data
    },
    COMPRA_UPDATE_LIST(state, data) {
        state.updateList = data
    }
}

// actions
const actions = {
    async getCompras({ commit }, pagination) {
        const response = (await Vue.http.get(urlAPI, { params: pagination }))
        if (pagination.page == 1) {
            commit('COMPRAS_ADD', response.body)
        } else {
            commit('COMPRAS_UPDATE', response.body.data)
        }
        commit('COMPRA_UPDATE_LIST', false)
    },

    async saveCompra({ commit }, params) {
        commit('COMPRA_UPDATE_LIST', true)
        if (params.id) {
            return await Vue.http.put(`${urlAPI}/${params.id}`, params)
        } else {
            return await Vue.http.post(urlAPI, params)
        }
    },

    async FaturarCompra({ commit }, dados) {
        commit('COMPRA_UPDATE_LIST', true)
        return (await Vue.http.post(`${urlAPI}/faturar`, dados))
    },

    async ReabrirCompra({ commit }, id) {
        commit('COMPRA_UPDATE_LIST', true)
        return (await Vue.http.get(`${urlAPI}/reabrir/${id}`))
    },

    async DeleteCompra({ commit }, id) {
        commit('COMPRA_UPDATE_LIST', true)
        return (await Vue.http.delete(`${urlAPI}/${id}`))
    }
}

export default {
    state,
    getters,
    actions,
    mutations
}
