import Vue from 'vue';

const urlAPI = 'api/admin/produtos';

// initial state
const state = {
    produtos: {},
    list: [],
    row: {},
}

// getters
const getters = {
    GetProdutos: state => state.produtos,
    ListProduto: state => state.list,
    GetProduto: state => state.row,
}

// mutations
const mutations = {
    PRODUTOS_ADD(state, data) {
        state.produtos = data
    },
    PRODUTOS_UPDATE(state, data) {
        data.filter((item) => {
            if (state.produtos.data.findIndex(x => x.id === item.id) == -1) {
                state.produtos.data.push(item)
            }
        })
    },
    PRODUTO_LIST(state, data) {
        state.list = data
    },
    PRODUTO_ROW(state, data) {
        state.row = data
    },
    ///////////////////////////////////////
    PRODUTO_ESTOQUES_ADD(state, estoque) {
        state.row.estoques.push({
            'id': null,
            'variante': estoque.variante,
            'codigo': estoque.codigo,
            'alerta': estoque.alerta,
            'quant': estoque.quant,
            'custo': estoque.custo,
            'valor': estoque.valor,
            'comissao': estoque.comissao,
            'prateleira': estoque.prateleira,
            'imagens': estoque.imagens,
            'ativo': true,
            'delete': false
        })
    },
    PRODUTO_REMOVE_ESTOQUE(state, estoque) {
        if (estoque.id) {
            if (estoque.delete === true) {
                Vue.set(estoque, 'delete', false)
            } else {
                Vue.set(estoque, 'delete', true)
            }
        } else {
            // remove a linha
            let Index = state.row.estoques.findIndex(item => item.nome === estoque.nome)
            state.row.estoques.splice(Index, 1)
        }
    },
    ////////////////////////////////////////
}

// actions
const actions = {
    async getProdutos({ commit }, pagination) {
        const response = (await Vue.http.get(urlAPI, { params: pagination }))
        if (pagination.page == 1) {
            commit('PRODUTOS_ADD', response.body)
        } else {
            commit('PRODUTOS_UPDATE', response.body.data)
        }
    },

    async getProduto({ commit }, id) {
        const result = (await Vue.http.get(`${urlAPI}/${id}`))
        commit('PRODUTO_ROW', result.data)
    },

    async saveProduto({ commit }, params) {
        if (params.id) {
            return await Vue.http.put(`${urlAPI}/${params.id}`, params)
        } else {
            return await Vue.http.post(urlAPI, params)
        }
    },

    async cleanProduto({ commit }) {
        commit('PRODUTO_ROW', { estoques: [], imagens: [] })
    },

    async deleteProduto({ }, id) {
        return (await Vue.http.delete(`${urlAPI}/${id}`))
    },

    searchProduto({ commit }, search) {
        clearTimeout(state.searchProdutoTimeout);
        state.searchProdutoTimeout = setTimeout(async () => {
            if (search != '') {
                const result = (await Vue.http.get(`${urlAPI}/search/${search}`)).body
                commit('PRODUTO_LIST', result)
            }
        }, 500);
    },
    ////// actions de estoque /////////
    AddEstoqueProduto({ commit, state }, dados) {
        let curProd = state.row.estoques.find(estoque => estoque.variante === dados.variante)
        if (!curProd) {
            commit('PRODUTO_ESTOQUES_ADD', dados)
        }
    },
    RemoveEstoqueProduto({ commit }, estoque) {
        commit('PRODUTO_REMOVE_ESTOQUE', estoque)
    },
}

export default {
    state,
    getters,
    actions,
    mutations
}
