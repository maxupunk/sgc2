import Vue from 'vue';

const urlAPI = 'api/admin/usuarios';

// initial state
const state = {
    usuarios: {},
    list: [],
    row: {},
    configDefault: {
        notificacoes: [],
        notificacaoVia: [],
        themes: {
            dark: {
                primary: {
                    hex: "#1976D2",
                },
                secondary: {
                    hex: "#424242",
                },
                accent: {
                    hex: "#82B1FF",
                },
                error: {
                    hex: "#FF5252",
                },
                info: {
                    hex: "#2196F3",
                },
                success: {
                    hex: "#4CAF50",
                },
                warning: {
                    hex: "#FFC107",
                },
            },
            light: {
                primary: {
                    hex: "#1976D2",
                },
                secondary: {
                    hex: "#424242",
                },
                accent: {
                    hex: "#82B1FF",
                },
                error: {
                    hex: "#FF5252",
                },
                info: {
                    hex: "#2196F3",
                },
                success: {
                    hex: "#4CAF50",
                },
                warning: {
                    hex: "#FFC107",
                },
            },
            isDark: false,
        }
    },
}

// getters
const getters = {
    GetUsuarios: state => state.usuarios,
    ListUsuario: state => state.list,
    GetUsuario: state => state.row,
    GetUsuarioConfigDefaul: state => state.configDefault
}

// mutations
const mutations = {
    USUARIOS_ADD(state, data) {
        state.usuarios = data
    },
    USUARIOS_UPDATE(state, data) {
        data.filter((item) => {
            if (state.usuarios.data.findIndex(x => x.id === item.id) == -1) {
                state.usuarios.data.push(item)
            }
        })
    },
    USUARIO_LIST(state, data) {
        state.list = data
    },
    USUARIO_ROW(state, data) {
        if (!data.config || data.config == null) {
            data.config = state.configDefault
        }
        state.row = data
    },
}

// actions
const actions = {
    async getUsuarios({ commit }, pagination) {
        const response = (await Vue.http.get(urlAPI, { params: pagination }))
        if (pagination.page == 1) {
            commit('USUARIOS_ADD', response.body)
        } else {
            commit('USUARIOS_UPDATE', response.body.data)
        }
    },

    async getUsuario({ commit }, id) {
        const result = (await Vue.http.get(`${urlAPI}/${id}`))
        commit('USUARIO_ROW', result.data)
    },

    async saveUsuario({ commit, getters, dispatch }, params) {
        if (params.id) {
            if (params.id == getters.GetdataUser.id) {
                dispatch("aplicaTema")
            }
            if (this.getters.IsPermissionPerfil) {
                return await Vue.http.put(`${urlAPI}/perfil/${params.id}`, params)
            } else {
                return await Vue.http.put(`${urlAPI}/${params.id}`, params)
            }
        } else {
            return await Vue.http.post(urlAPI, params)
        }
    },

    async cleanUsuario({ commit }) {
        commit('USUARIO_ROW', {})
    },

    async deleteUsuario({ }, id) {
        return (await Vue.http.delete(`${urlAPI}/${id}`))
    },

    async listUsuario({ commit }) {
        const result = (await Vue.http.get(`${urlAPI}/listing`))
        commit('USUARIO_LIST', result.data)
    },

    async deleteToken({ state, dispatch }, token) {
        if (this.getters.IsPermissionPerfil) {
            await Vue.http.delete(`${urlAPI}/${token.tokenable_id}/token/${token.id}`)
        } else {
            await Vue.http.delete(`${urlAPI}/token/${token.id}`)
        }
        dispatch('getUsuario', state.row.id)
    },
}

export default {
    state,
    getters,
    actions,
    mutations
}
