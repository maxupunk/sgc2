import Vue from 'vue';

const urlAPI = 'api/admin/estoques';

// initial state
const state = {
    estoques: [],
    list: [],
    row: []
}

// getters
const getters = {
    GetEstoques: state => state.estoques,
    ListEstoque: state => state.list,
}

// mutations
const mutations = {
    ESTOQUES_ADD(state, data) {
        state.estoques = data
    },
    ESTOQUES_UPDATE(state, data) {
        data.filter((item) => {
            if (state.estoques.data.findIndex(x => x.id === item.id) == -1) {
                state.estoques.data.push(item)
            }
        })
    },
    ESTOQUE_LIST(state, data) {
        state.list = data
    },
    ESTOQUE_ROW(state, data) {
        state.row = data
    }
}

// actions
const actions = {
    async getEstoques({ commit }, pagination) {
        const response = (await Vue.http.get(urlAPI, { params: pagination }))
        if (pagination.page == 1) {
            commit('ESTOQUES_ADD', response.body)
        } else {
            commit('ESTOQUES_UPDATE', response.body.data)
        }
    },

    async getEstoque({ commit }, id) {
        commit('ESTOQUE_ROW', [])
        const result = (await Vue.http.get(`${urlAPI}/${id}`))
        commit('ESTOQUE_ROW', result.data)
    },

    async saveEstoque({ }, params) {
        const dados = {
            quant: params.quant,
            custo: params.custo,
            valor: params.valor
        }
        return await Vue.http.put(`${urlAPI}/${params.id}`, dados)
    },

    async cleanEstoque({ commit }) {
        commit('ESTOQUE_ROW', {})
    },

}

export default {
    state,
    getters,
    actions,
    mutations
}
