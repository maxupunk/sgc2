import Vue from 'vue';

const urlAPI = 'api/admin/avarias';

// initial state
const state = {
    avarias: [],
    list: [],
    row: {}
}

// getters
const getters = {
    GetAvarias: state => state.avarias,
    ListAvaria: state => state.list,
    GetAvaria: state => state.row,
}

// mutations
const mutations = {
    AVARIAS_ADD(state, data) {
        state.avarias = data
    },
    AVARIAS_UPDATE(state, data) {
        data.filter((item) => {
            if (state.avarias.data.findIndex(x => x.id === item.id) == -1) {
                state.avarias.data.push(item)
            }
        })
    },
    AVARIA_ROW(state, data) {
        state.row = data
    }
}

// actions
const actions = {
    async getAvarias({ commit, state }, pagination) {
        const response = (await Vue.http.get(urlAPI, { params: pagination }))
        if (pagination.page == 1) {
            commit('AVARIAS_ADD', response.body)
        } else {
            commit('AVARIAS_UPDATE', response.body.data)
        }
    },

    async getAvaria({ commit }, id) {
        const result = (await Vue.http.get(`${urlAPI}/${id}`))
        commit('AVARIA_ROW', result.data)
    },

    async saveAvaria({ }, params) {
        if (params.id) {
            return await Vue.http.put(`${urlAPI}/${params.id}`, params)
        } else {
            return await Vue.http.post(urlAPI, params)
        }
    },

    async deleteAvaria({ }, id) {
        return (await Vue.http.delete(`${urlAPI}/${id}`))
    },

    async cleanAvaria({ commit }) {
        commit('AVARIA_ROW', {})
    }
}

export default {
    state,
    getters,
    actions,
    mutations
}
