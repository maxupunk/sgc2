import Vue from 'vue';

const urlAPI = 'api/admin/prestacaocontas';

// initial state
const state = {
    prestacoes: [],
    comissoes: [],
}

// getters
const getters = {
    GetPrestacoes: state => state.prestacoes,
    GetComicoes: state => state.comissoes,
}

// mutations
const mutations = {
    PRESTACOES_ADD(state, data) {
        state.prestacoes = data
    },
    PRESTACOES_UPDATE(state, data) {
        data.filter((item) => {
            if (state.prestacoes.data.findIndex(x => x.id === item.id) == -1) {
                state.prestacoes.data.push(item)
            }
        })
    },
    PRESTACAO_COMISSOES(state, data) {
        state.comissoes = data
    },
    PRESTACAO_CLEAR(state) {
        state.comissoes = []
    }
}

// actions
const actions = {
    async getPrestacoes({ commit }, pagination) {
        const response = (await Vue.http.get(urlAPI, { params: pagination }))
        if (pagination.page == 1) {
            commit('PRESTACOES_ADD', response.body)
        } else {
            commit('PRESTACOES_UPDATE', response.body.data)
        }
    },

    async getComicoes({ commit }, id) {
        if (id !== undefined && id !== null) {
            const result = (await Vue.http.get(`${urlAPI}/${id}`))
            if (result.data) {
                commit('PRESTACAO_COMISSOES', result.data)
            } else {
                commit('PRESTACAO_CLEAR')
            }
        }
    },

    async savePrestacaoConta({ }, params) {
        return await Vue.http.post(urlAPI, params)
    },

    async deletePrestacaoConta({ }, id) {
        return (await Vue.http.delete(`${urlAPI}/${id}`))
    },

    async CleanPrestacaoConta({ commit }) {
        commit('PRESTACAO_CLEAR')
    },
}

export default {
    state,
    getters,
    actions,
    mutations
}
