import Vue from 'vue';

const urlAPI = 'api/admin/medidas';

// initial state
const state = {
    medidas: [],
    list: [],
    row: {}
}

// getters
const getters = {
    GetMedidas: state => state.medidas,
    ListMedida: state => state.list,
    GetMedida: state => state.row,
}

// mutations
const mutations = {
    MEDIDAS_ADD(state, data) {
        state.medidas = data
    },
    MEDIDAS_UPDATE(state, data) {
        data.filter((item) => {
            if (state.medidas.data.findIndex(x => x.id === item.id) == -1) {
                state.medidas.data.push(item)
            }
        })
    },
    MEDIDA_LIST_UPDATE(state, data) {
        state.list = data
    },
    MEDIDA_ROW_UPDATE(state, data) {
        state.row = data
    }
}

// actions
const actions = {
    async getMedidas({ commit }, pagination) {
        const response = (await Vue.http.get(urlAPI, { params: pagination }))
        if (pagination.page == 1) {
            commit('MEDIDAS_ADD', response.body)
        } else {
            commit('MEDIDAS_UPDATE', response.body.data)
        }
    },

    async getMedida({ commit }, id) {
        const result = (await Vue.http.get(`${urlAPI}/${id}`))
        commit('MEDIDA_ROW_UPDATE', result.data)
    },

    async saveMedida({ commit }, params) {
        if (params.id) {
            return await Vue.http.put(`${urlAPI}/${params.id}`, params)
        } else {
            return await Vue.http.post(urlAPI, params)
        }
    },

    async deleteMedida({ }, id) {
        return (await Vue.http.delete(`${urlAPI}/${id}`))
    },

    async cleanMedida({ commit }) {
        commit('MEDIDA_ROW_UPDATE', {})
    },

    async listMedida({ commit }) {
        const result = (await Vue.http.get(`${urlAPI}/listing`)).data
        commit('MEDIDA_LIST_UPDATE', result)
    }
}

export default {
    state,
    getters,
    actions,
    mutations
}
