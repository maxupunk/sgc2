import Vue from 'vue';

const urlAPI = 'api/admin/ruas';

// initial state
const state = {
    ruas: [],
    list: [],
    row: {}
}

// getters
const getters = {
    GetRuas: state => state.ruas,
    ListRua: state => state.list,
    GetRua: state => state.row,
}

// mutations
const mutations = {
    RUAS_ADD(state, data) {
        state.ruas = data
    },
    RUAS_UPDATE(state, data) {
        data.filter((item) => {
            if (state.ruas.data.findIndex(x => x.id === item.id) == -1) {
                state.ruas.data.push(item)
            }
        })
    },
    RUAS_LIST(state, data) {
        state.list = data
    },
    RUAS_ROW(state, data) {
        state.row = data
    }
}

// actions
const actions = {
    async getRuas({ commit }, pagination) {
        const response = (await Vue.http.get(urlAPI, { params: pagination }))
        if (pagination.page == 1) {
            commit('RUAS_ADD', response.body)
        } else {
            commit('RUAS_UPDATE', response.body.data)
        }
    },

    async getRua({ commit, store }, id) {
        if (id !== store.row.id) {
            const result = (await Vue.http.get(`${urlAPI}/${id}`))
            commit('RUAS_ROW', result.data)
        }
    },

    async saveRua({ commit }, params) {
        if (params.id) {
            return await Vue.http.put(`${urlAPI}/${params.id}`, params)
        } else {
            return await Vue.http.post(urlAPI, params)
        }
    },

    async deleteRua({ }, id) {
        return (await Vue.http.delete(`${urlAPI}/${id}`))
    },

    async cleanRua({ commit }) {
        commit('RUAS_ROW', {})
    },

    async listRua({ commit }, id) {
        if (id != undefined) {
            const result = (await Vue.http.get(`${urlAPI}/listing/${id}`)).data
            commit('RUAS_LIST', result)
        }
    }
}

export default {
    state,
    getters,
    actions,
    mutations
}
