import Vue from 'vue';

const urlAPI = 'api/admin/estados';

// initial state
const state = {
    list: []
}

// getters
const getters = {
    ListEstado: state => state.list,
}

// mutations
const mutations = {
    updateEstadoList(state, data) {
        state.list = data
    },
}

// actions
const actions = {
    async listEstado({ commit, state }) {
        if (!state.list.length) {
            const result = (await Vue.http.get(`${urlAPI}/listing`))
            commit('updateEstadoList', result.data)
        }
    }
}

export default {
    state,
    getters,
    actions,
    mutations
}
