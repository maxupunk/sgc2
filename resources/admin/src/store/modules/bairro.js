import Vue from 'vue';

const urlAPI = 'api/admin/bairros';

// initial state
const state = {
    bairros: [],
    getBairrosTimeout: null,
    list: [],
    row: {}
}

// getters
const getters = {
    GetBairros: state => state.bairros,
    ListBairro: state => state.list,
    GetBairro: state => state.row,
}

// mutations
const mutations = {
    BAIRROS_ADD(state, data) {
        state.bairros = data
    },
    BAIRROS_UPDATE(state, data) {
        data.filter((item) => {
            if (state.bairros.data.findIndex(x => x.id === item.id) == -1) {
                state.bairros.data.push(item)
            }
        })
    },
    BAIRRO_LIST(state, data) {
        state.list = data
    },
    BAIRRO_ROW(state, data) {
        state.row = data
    }
}

// actions
const actions = {
    async getBairros({ commit }, pagination) {
        const response = (await Vue.http.get(urlAPI, { params: pagination }))
        if (pagination.page == 1) {
            commit('BAIRROS_ADD', response.body)
        } else {
            commit('BAIRROS_UPDATE', response.body.data)
        }
    },

    async getBairro({ commit, store }, id) {
        if (id !== store.row.id) {
            const result = (await Vue.http.get(`${urlAPI}/${id}`))
            commit('BAIRRO_ROW', result.data)
        }
    },

    async saveBairro({ }, params) {
        if (params.id) {
            return await Vue.http.put(`${urlAPI}/${params.id}`, params)
        } else {
            return await Vue.http.post(urlAPI, params)
        }
    },

    async deleteBairro({ }, id) {
        return (await Vue.http.delete(`${urlAPI}/${id}`))
    },

    async cleanBairro({ commit }) {
        commit('BAIRRO_ROW', {})
    },

    async listBairro({ commit }, id) {
        if (id != undefined) {
            let result = (await Vue.http.get(`${urlAPI}/listing/${id}`)).data
            commit('BAIRRO_LIST', result)
        }
    }
}

export default {
    state,
    getters,
    actions,
    mutations
}
