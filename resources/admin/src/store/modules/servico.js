import Vue from 'vue';

const urlAPI = 'api/admin/servicos';

// initial state
const state = {
    // lista de serviços
    servicos: [],
    updateList: false
}

// getters
const getters = {
    GetServicos: state => state.servicos,
    GetServicosUpdateLista: state => state.updateList
}

// mutations
const mutations = {
    SERVICOS_ADD(state, data) {
        state.servicos = data
    },
    SERVICOS_UPDATE(state, data) {
        data.filter((item) => {
            if (state.servicos.data.findIndex(x => x.id === item.id) == -1) {
                state.servicos.data.push(item)
            }
        })
    },
    SERVICOS_UPDATE_LISTA(state, data) {
        state.updateList = data
    }
}

// actions
const actions = {
    async getServicos({ commit }, pagination) {
        const response = (await Vue.http.get(urlAPI, { params: pagination }))
        if (pagination.page == 1) {
            commit('SERVICOS_ADD', response.body)
        } else {
            commit('SERVICOS_UPDATE', response.body.data)
        }
        commit('SERVICOS_UPDATE_LISTA', false)
    },

    async SaveServico({ dispatch, commit }, params) {
        if (params.id) {
            await Vue.http.put(`${urlAPI}/${params.id}`, params)
            dispatch('GetOrdem', params.id)
        } else {
            return await Vue.http.post(urlAPI, params)
        }
        commit('SERVICOS_UPDATE_LISTA', true)
    },

    async deleteServico({ commit }, id) {
        commit('SERVICOS_UPDATE_LISTA', true)
        return (await Vue.http.delete(`${urlAPI}/${id}`))
    },

    async FaturarServico({ commit }, dados) {
        commit('SERVICOS_UPDATE_LISTA', true)
        return (await Vue.http.post(`${urlAPI}/faturar`, dados))
    },

    async EstenderServico({ commit }, dados) {
        commit('SERVICOS_UPDATE_LISTA', true)
        return (await Vue.http.post(`${urlAPI}/estender`, dados))
    },

    async DeleteServico({ commit }, id) {
        commit('SERVICOS_UPDATE_LISTA', true)
        return (await Vue.http.delete(`${urlAPI}/${id}`))
    }
}

export default {
    state,
    getters,
    actions,
    mutations
}
