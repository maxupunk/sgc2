import Vue from 'vue';
import menus from "../menu.json";

const urlAPI = '/api/admin'

// initial state
const state = {
    dataUser: {},
    token: '',
    url_root: window.location.protocol + '//' + window.location.hostname + ':' + window.location.port,
    manusPermitidos: '',
}

// getters
const getters = {
    GetUrlRoot: store => store.url_root,
    GetdataUser: state => state.dataUser,
    GetToken: state => state.token,
    GetMenusPermitidos: state => state.manusPermitidos,
    IsPermissionPerfil: state => {
        return state.acl.includes("usuarios.perfil") || state.acl.includes('*')
    },
    GetUsuarioConfig: (state, getters) => {
        if (state.dataUser.config != undefined || state.dataUser.config != null) {
            return state.dataUser.config
        } else {
            return getters.GetUsuarioConfigDefaul;
        }
    },
}

// mutations
const mutations = {
    SET_DATA_USER(state, user) {
        state.dataUser = user
    },

    SET_MENU_PERMISAO(state, menus) {
        state.manusPermitidos = menus
    },

    SET_TOKEN(state, token) {
        state.token = token
    },

    SET_ACL(state, acl) {
        state.acl = acl
    },

    SET_URL_ROOT(state, url_root) {
        Vue.http.options.root = url_root
        state.url_root = url_root
    }
}

// actions
const actions = {

    destroyToken({ commit }) {
        commit('SET_TOKEN', '')
        window.localStorage.clear()
    },

    setUrlRoot: ({ commit }, url_root) => {
        commit('SET_URL_ROOT', url_root)
    },

    async GoLogin({ dispatch, commit }, params) {
        params['device_name'] = navigator.platform //navigator?.userAgentData?.platform || navigator?.platform || 'unknown'
        Vue.http.post(`${urlAPI}/login`, params).then((data) => {
            if (data.body.token) {
                commit('SET_TOKEN', data.body.token)
                commit('SET_DATA_USER', data.body.usuario)
                commit('SET_ACL', data.body.acl)
                dispatch('SetMenuPermissao', data.body.acl)
                dispatch('aplicaTema')
            }
        })
    },

    async GoLogout({ getters, dispatch }) {
        Vue.http.post(`${urlAPI}/logout`, getters.GetdataUser)
        dispatch('destroyToken')
    },

    async SetMenuPermissao({ commit }, acl) {
        let menuPermitido = []
        menus.forEach((menu) => {
            let SubMenuNovo = Object.assign({}, menu);
            if (menu.children) {
                SubMenuNovo.children = [];
                menu.children.forEach((submenu) => {
                    let UrlFilter = submenu.to.split("/")[1];
                    let ContemAcl = acl.includes(UrlFilter + ".index") || acl.includes("*");
                    if (ContemAcl) {
                        SubMenuNovo.children.push(submenu);
                    }
                });
            }
            menuPermitido.push(SubMenuNovo);
        });
        commit('SET_MENU_PERMISAO', menuPermitido)
    },
}

export default {
    state,
    getters,
    actions,
    mutations
}
