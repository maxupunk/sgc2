import Vue from 'vue';

const urlAPI = 'api/admin/cidades';

// initial state
const state = {
    cidades: [],
    list: [],
    row: {}
}

// getters
const getters = {
    GetCidades: state => state.cidades,
    ListCidade: state => state.list,
    GetCidade: state => state.row,
}

// mutations
const mutations = {
    updateCidades(state, data) {
        state.cidades = data
    },
    updateCidadeList(state, data) {
        state.list = data
    },
    updateCidadeRow(state, data) {
        state.row = data
    }
}

// actions
const actions = {
    async getCidades({ commit }, pagination) {
        const result = (await Vue.http.get(urlAPI, { params: pagination }))
        commit('updateCidades', result.data)
    },

    async getCidade({ commit, store }, id) {
        if (id !== store.row.id) {
            const result = (await Vue.http.get(`${urlAPI}/${id}`))
            commit('updateCidadeRow', result.data)
        }
    },

    async saveCidade({ commit }, params) {
        if (params.id) {
            return await Vue.http.put(`${urlAPI}/${params.id}`, params)
        } else {
            return await Vue.http.post(urlAPI, params)
        }
    },

    async deleteCidade({ }, id) {
        return (await Vue.http.delete(`${urlAPI}/${id}`))
    },

    async cleanCidade({ commit }) {
        commit('updateCidadeRow', {})
    },

    async listCidade({ commit }, id) {
        if (id !== undefined) {
            const result = (await Vue.http.get(`${urlAPI}/listing/${id}`)).data
            commit('updateCidadeList', result)
        }
    }
}

export default {
    state,
    getters,
    actions,
    mutations
}
