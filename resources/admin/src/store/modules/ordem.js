import Vue from 'vue';

const urlAPI = 'api/admin/ordens';

// initial state
const state = {
    row: {},
    tipos: [{
        value: 'v',
        text: 'Venda'
    },
    {
        value: 'c',
        text: 'Compra'
    },
    {
        value: 's',
        text: 'Servico'
    },
    {
        value: 'd',
        text: 'Devolução'
    },
    {
        value: 't',
        text: 'Transferencia/troca'
    }
    ],
    status: [{
        value: 'EA',
        text: 'Em aberto',
        color: 'orange lighten-1'
    },
    {
        value: 'PD',
        text: 'Pendente',
        color: 'orange lighten-3'
    },
    {
        value: 'OR',
        text: 'Orçamento',
        color: 'purple lighten-2'
    },
    {
        value: 'AA',
        text: 'Aguardando aprovação'
    },
    {
        value: 'PA',
        text: 'Pagamento em análise',
        disabled: true,
    },
    {
        value: 'AP',
        text: 'Aguardando pagamento',
        disabled: true,
    },
    {
        value: 'EE',
        text: 'Enviado'
    },
    {
        value: 'RP',
        text: 'Entregue parcialmente'
    },
    {
        value: 'RE',
        text: 'Entregue',
        disabled: true,
        color: 'green lighten-1'
    },
    {
        value: 'CC',
        text: 'Concluído',
        color: 'blue lighten-1'
    },
    {
        value: 'CA',
        text: 'Cancelado',
        color: 'blue-grey lighten-1'
    }
    ],
    relatorio: [],
    GraficoPorDia: [],
    GraficoPorUsuario: [],
    GraficoPorTipo: []
}

// getters
const getters = {
    GetOrdem: state => state.row,
    GetOrdemTotal: state => {
        if (state.row.itens) {
            return state.row.itens.reduce((total, produto) => {
                return total + produto.quant * produto.valor
            }, 0)
        }
    },
    GetOrdemStatus: state => state.status,
    GetOrdemStatusRow: (state) => (value) => {
        var result = state.status.filter((obj) => {
            return obj.value === value;
        });
        return result[0]
    },
    GetOrdemTipos: state => state.tipos,
    GetOrdemRelatorio: state => state.relatorio,
    GetOrdemPorDia: state => state.GraficoPorDia,
    GetOrdemPorUsuario: state => state.GraficoPorUsuario,
    GetOrdemPorTipo: state => state.GraficoPorTipo,
}

// mutations
const mutations = {

    ORDEM_UPDATE_ROW(state, data) {
        //data.pgstatus = data.datapg != null
        state.row = data
    },
    // RELATORIOS
    ORDEM_RELATORIO_UPDATE(state, data) {

        let GraficoPorDia = []
        let GraficoPorUsuario = []
        let GraficoPorTipo = []

        if (Object.keys(data).length > 1) {
            data.forEach(function (item) {
                //if (item.entregue !== null) {
                // gera o array GraficoPorDia
                var Pordata = GraficoPorDia.find(row => row[0] === item.entregue)
                if (Pordata) {
                    Pordata[1] = parseFloat(Pordata[1]) + parseFloat(item.total)
                } else {
                    GraficoPorDia.push([item.entregue, item.total])
                }
                // Gera o array GraficoPorTipo
                var tipo = GraficoPorTipo.find(row => row[0] === item.tipo)
                if (tipo) {
                    tipo[1] = parseFloat(tipo[1]) + parseFloat(item.total)
                } else {
                    var tipos = state.tipos.find(row => row.value === item.tipo)
                    GraficoPorTipo.push([tipos.text, parseFloat(item.total)])
                }
                // Gera o array GraficoPorUsuario
                var user = GraficoPorUsuario.find(row => row[0] === item.usuario)
                if (user) {
                    user[1] = parseFloat(user[1]) + parseFloat(item.total)
                } else {
                    GraficoPorUsuario.push([item.usuario, parseFloat(item.total)])
                }
                //}
            });
            state.GraficoPorDia = GraficoPorDia
            state.GraficoPorUsuario = GraficoPorUsuario
            state.GraficoPorTipo = GraficoPorTipo
            state.relatorio = data
        }
    },
}

// actions
const actions = {

    async GetOrdem({ commit }, id) {
        const result = (await Vue.http.get(`${urlAPI}/${id}`))
        commit('ORDEM_UPDATE_ROW', result.data)
    },

    async ReabrirOrdem({ }, id) {
        return (await Vue.http.get(`${urlAPI}/reabrir/${id}`))
    },

    async cleanOrdem({ commit }) {
        commit('ORDEM_UPDATE_ROW', {})
    },
    // Funções de relatorios
    async getOrdemRelatorio({ commit }, dados) {
        //converte {__ob__: Observer} para array trodicional
        const params = Object.assign({}, dados)
        // faz o get com os paramentro
        const result = (await Vue.http.get(`${urlAPI}/relatorio`, { params: params }))
        commit('ORDEM_RELATORIO_UPDATE', result.data)
    },
}

export default {
    state,
    getters,
    actions,
    mutations
}
