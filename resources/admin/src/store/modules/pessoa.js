import Vue from 'vue';

const urlAPI = 'api/admin/pessoas';

// initial state
const state = {
    pessoas: {},
    list: [],
    row: {},
    tipo: [
        { value: 'f', text: 'Física' },
        { value: 'j', text: 'Jurídica' }
    ],
}

// getters
const getters = {
    GetPessoas: state => state.pessoas,
    ListPessoa: state => state.list,
    GetPessoa: state => state.row,
    GetPessoaTipo: state => state.tipo
}

// mutations
const mutations = {
    PESSOAS_ADD(state, data) {
        state.pessoas = data
    },
    PESSOAS_UPDATE(state, data) {
        data.filter((item) => {
            if (state.pessoas.data.findIndex(x => x.id === item.id) == -1) {
                state.pessoas.data.push(item)
            }
        })
    },
    PESSOA_LIST_UPDATE(state, data) {
        state.list = data
    },
    PESSOA_ROW_UPDATE(state, data) {
        state.row = data
    }
}

// actions
const actions = {
    async getPessoas({ commit, state }, pagination) {
        const response = (await Vue.http.get(urlAPI, { params: pagination }))
        if (pagination.page == 1) {
            commit('PESSOAS_ADD', response.body)
        } else {
            commit('PESSOAS_UPDATE', response.body.data)
        }
    },

    async getPessoa({ commit, state }, id) {
        const result = await Vue.http.get(`${urlAPI}/${id}`)
        commit('PESSOA_ROW_UPDATE', result.data)
    },

    async getPessoaLink({ }, id) {
        return await Vue.http.get(`${urlAPI}/link/${id}`)
    },

    async savePessoa({ commit }, params) {
        if (params.id) {
            return await Vue.http.put(`${urlAPI}/${params.id}`, params)
        } else {
            return await Vue.http.post(urlAPI, params)
        }
    },

    async cleanPessoa({ commit }) {
        commit('PESSOA_ROW_UPDATE', { 'contatos': [], 'ativo': true })
    },

    async deletePessoa({ }, id) {
        return (await Vue.http.delete(`${urlAPI}/${id}`))
    },

    async deleteTokenPessoa({ state, dispatch }, token) {
        await Vue.http.delete(`${urlAPI}/${token.tokenable_id}/token/${token.id}`)
        dispatch('getPessoa', state.row.id)
    },

    searchPessoa({ commit, state }, search) {
        if (search != null) {
            clearTimeout(state.searchTimeout);
            state.searchTimeout = setTimeout(async () => {
                if (search != '') {
                    const result = await Vue.http.get(`${urlAPI}/search/${search}`)
                    commit('PESSOA_LIST_UPDATE', result.data)
                }
            }, 500);
        }
    }
}

export default {
    state,
    getters,
    actions,
    mutations
}
