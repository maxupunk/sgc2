import Vue from 'vue';

const urlAPI = 'api/admin/financeiros';

// initial state
const state = {
    financeiros: [],
    row: {},
    relatorio: [],
    nature: [
        { value: 'R', text: 'Receita' },
        { value: 'D', text: 'Despesa' }
    ],
    estatus: [
        { value: 'AB', text: 'Aberto' },
        { value: 'PG', text: 'Pago' },
        { value: 'PP', text: 'Parcialmente pago' },
        { value: 'CN', text: 'Cancelado' },
        { value: 'DV', text: 'Devolução' }
    ],

    GraficoReceitaDespesa: [],
    GraficoPorUsuario: [],
    GraficoPorNatureza: [],
    GraficoResulmo: [],
    GraficoEstatus: [],
    updateList: false
}

// getters
const getters = {
    GetFinanceiros: state => state.financeiros,
    GetFinanceiro: state => state.row,
    GetFinanceiroNature: state => state.nature,
    GetFinanceiroEstatus: state => state.estatus,
    GetFinanceiroHistorico: state => state.historico,
    GetFinanceiroRelatorio: state => state.relatorio,
    GetFinanceiroGraficoRecDes: state => state.GraficoReceitaDespesa,
    GetFinanceiroGraficoPorUsuario: state => state.GraficoPorUsuario,
    GetFinanceiroGraficoPorNatureza: state => state.GraficoPorNatureza,
    GetFinanceiroGraficoResulmo: state => state.GraficoResulmo,
    GetFinanceiroGraficoEstatus: state => state.GraficoEstatus,
    GetFinanceiroUpdateLista: state => state.updateList
}

// mutations
const mutations = {
    FINANCEIROS_ADD(state, data) {
        state.financeiros = data
    },
    FINANCEIROS_UPDATE(state, data) {
        data.filter((item) => {
            if (state.financeiros.data.findIndex(x => x.id === item.id) == -1) {
                state.financeiros.data.push(item)
            }
        })
    },
    FINANCEIRO_ROW_UPDATE(state, data) {
        state.row = data
    },
    FINANCEIRO_RELATORIO_UPDATE(state, data) {
        let GraficoReceita = []
        let GraficoDespesa = []
        let GraficoPorUsuario = []
        let GraficoPorNatureza = []
        let GraficoPorEstatus = []

        data.forEach(function (item) {
            if (item.datapg !== null) {
                if (item.nature === 'R') {
                    let PordataReceita = GraficoReceita.find(row => row[0] === item.datapg)
                    if (PordataReceita) {
                        PordataReceita[1] = parseFloat(PordataReceita[1]) + parseFloat(item.pago)
                    } else {
                        GraficoReceita.push([item.datapg, item.pago])
                        GraficoDespesa.push([item.datapg, 0])
                    }
                } else {
                    let PordataDespesa = GraficoDespesa.find(row => row[0] === item.datapg)
                    if (PordataDespesa) {
                        PordataDespesa[1] = parseFloat(PordataDespesa[1]) + parseFloat(item.pago)
                    } else {
                        GraficoReceita.push([item.datapg, 0])
                        GraficoDespesa.push([item.datapg, item.pago])
                    }
                }
                // Gera o array GraficoPorUsuario
                var user = GraficoPorUsuario.find(row => row[0] === item.usuario)
                if (user) {
                    user[1] = parseFloat(user[1]) + parseFloat(item.pago)
                } else {
                    GraficoPorUsuario.push([item.usuario, parseFloat(item.pago)])
                }
                // Grafico por Naturesa
                var natureza = GraficoPorNatureza.find(row => row[0] === item.nature)
                if (natureza) {
                    natureza[1] = parseFloat(natureza[1]) + parseFloat(item.pago)
                } else {
                    var natureza = state.nature.find(row => row.value === item.nature)
                    GraficoPorNatureza.push([item.nature, parseFloat(item.pago)])
                }
            }
            // Grafico por Naturesa
            var estatus = GraficoPorEstatus.find(row => row[0] === item.estatus)
            if (estatus) {
                estatus[1]++
            } else {
                GraficoPorEstatus.push([item.estatus, 1])
            }
        });

        state.GraficoReceitaDespesa = [
            { name: 'Receita', data: GraficoReceita },
            { name: 'Despesa', data: GraficoDespesa }
        ]
        state.GraficoEstatus = GraficoPorEstatus
        state.GraficoPorUsuario = GraficoPorUsuario
        state.GraficoPorNatureza = GraficoPorNatureza
        state.relatorio = data
    },
    FINANCEIROS_UPDATE_LISTA(state, data) {
        state.updateList = data
    }
}

// actions
const actions = {
    async getFinanceiros({ commit }, pagination) {
        const response = (await Vue.http.get(urlAPI, { params: pagination }))
        if (pagination.page == 1) {
            commit('FINANCEIROS_ADD', response.body)
        } else {
            commit('FINANCEIROS_UPDATE', response.body.data)
        }
        commit('FINANCEIROS_UPDATE_LISTA', false)
    },

    async getFinanceiro({ commit }, id) {
        const result = (await Vue.http.get(`${urlAPI}/${id}`))
        commit('FINANCEIRO_ROW_UPDATE', result.data)
    },

    async saveFinanceiro({ commit }, params) {
        commit('FINANCEIROS_UPDATE_LISTA', true)
        if (params.id) {
            return await Vue.http.put(`${urlAPI}/${params.id}`, params)
        } else {
            return await Vue.http.post(urlAPI, params)
        }
    },

    async deleteFinanceiro({ commit }, id) {
        commit('FINANCEIROS_UPDATE_LISTA', true)
        return (await Vue.http.delete(`${urlAPI}/${id}`))
    },

    async cleanFinanceiro({ commit }) {
        commit('FINANCEIRO_ROW_UPDATE', {})
    },

    async BaixaFinanceiro({ commit }, dados) {
        commit('FINANCEIROS_UPDATE_LISTA', true)
        return (await Vue.http.post(`${urlAPI}/baixa`, dados))
    },

    async BaixaFinanceiroPessoa({ commit }, dados) {
        commit('FINANCEIROS_UPDATE_LISTA', true)
        return (await Vue.http.post(`${urlAPI}/baixa/pessoa`, dados))
    },

    async getFinanceiroRelatorio({ }, dados) {
        const result = (await Vue.http.get(`${urlAPI}/relatorio`, { params: dados }))
    },
}

export default {
    state,
    getters,
    actions,
    mutations
}
