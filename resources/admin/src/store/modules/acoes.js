import Vue from 'vue';

const urlAPI = 'api/admin/acoes';

// initial state
const state = {
    grupoAcoes: [],
    metodos: []
}

// getters
const getters = {
    grupoAcoes: state => state.grupoAcoes,
}

// mutations
const mutations = {
    updateAcoes(state, data) {
        state.grupoAcoes = data.reduce((grupos, raw) => {
            if (Object.keys(grupos).includes(raw.titulo)) return grupos;
            grupos[raw.titulo] = data.filter(acao => acao.titulo === raw.titulo);
            return grupos;
        }, {})
    },
    updateMetodos(state, data) {
        state.metodos = data
    },
}

// actions
const actions = {

    getAcoes({ commit }) {
        Vue.http.get(`${urlAPI}/listing`).then((result) => {
            commit('updateAcoes', result.data)
        })
    }
}

export default {
    state,
    getters,
    actions,
    mutations
}
