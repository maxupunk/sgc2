import Vue from 'vue';

const urlAPI = 'api/admin/perfis';

// initial state
const state = {
    perfis: [],
    list: [],
    row: {
        acoes_perfis: []
    }
}

// getters
const getters = {
    GetPerfils: state => state.perfis,
    ListPerfil: state => state.list,
    GetPerfil: state => state.row,
}

// mutations
const mutations = {
    PERFIS_ADD(state, data) {
        state.perfis = data
    },
    PERFIS_UPDATE(state, data) {
        data.filter((item) => {
            if (state.perfis.data.findIndex(x => x.id === item.id) == -1) {
                state.perfis.data.push(item)
            }
        })
    },
    PERFIL_LIST_UPDATE(state, data) {
        state.list = data
    },
    PERFIL_ROW_UPDATE(state, data) {
        state.row = data
    }
}

// actions
const actions = {
    async getPerfils({ commit }, pagination) {
        const response = (await Vue.http.get(urlAPI, { params: pagination }))
        if (pagination.page == 1) {
            commit('PERFIS_ADD', response.body)
        } else {
            commit('PERFIS_UPDATE', response.body.data)
        }
    },

    async getPerfil({ commit }, id) {
        const result = (await Vue.http.get(`${urlAPI}/${id}`))
        commit('PERFIL_ROW_UPDATE', result.data)
    },

    async savePerfil({ commit }, params) {
        if (params.id) {
            return await Vue.http.put(`${urlAPI}/${params.id}`, params)
        } else {
            return await Vue.http.post(urlAPI, params)
        }
    },

    async deletePerfil({ }, id) {
        return (await Vue.http.delete(`${urlAPI}/${id}`))
    },

    async cleanPerfil({ commit }) {
        commit('PERFIL_ROW_UPDATE', { acoes_perfis: [] })
    },

    async listPerfil({ commit }) {
        const result = (await Vue.http.get(`${urlAPI}/listing`))
        commit('PERFIL_LIST_UPDATE', result.data)
    }

}

export default {
    state,
    getters,
    actions,
    mutations
}
