import Vue from 'vue';

const urlAPI = 'api/admin/tags';

// initial state
const state = {
    tags: [],
    list: [],
    row: {}
}

// getters
const getters = {
    GetTags: state => state.tags,
    ListTag: state => state.list,
    GetTag: state => state.row,
}

// mutations
const mutations = {
    updateTags(state, data) {
        state.tags = data
    },
    updateTagList(state, data) {
        state.list = data
    },
    updateTagRow(state, data) {
        state.row = data
    }
}

// actions
const actions = {
    async getTags({ commit }, pagination) {
        const result = (await Vue.http.get(urlAPI, { params: pagination }))
        commit('updateTags', result.data)
    },

    async getTag({ commit }, id) {
        const result = (await Vue.http.get(`${urlAPI}/${id}`))
        commit('updateTagRow', result.data)
    },

    async saveTag({ commit }, params) {
        if (params.id) {
            return await Vue.http.put(`${urlAPI}/${params.id}`, params)
        } else {
            return await Vue.http.post(urlAPI, params)
        }
    },

    async deleteTag({ }, id) {
        return (await Vue.http.delete(`${urlAPI}/${id}`))
    },

    async cleanTag({ commit }) {
        commit('updateTagRow', {})
    },

    async searchTag({ commit }, search) {
        if (search !== null) {
            clearTimeout(state.searchTimeout);
            state.searchTimeout = setTimeout(async () => {
                const result = (await Vue.http.get(`${urlAPI}/search?tag=${search}`)).body
                commit('updateTagList', result)
            }, 500);
        }
    }
}

export default {
    state,
    getters,
    actions,
    mutations
}
