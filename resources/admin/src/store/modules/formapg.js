import Vue from 'vue';

const urlAPI = 'api/admin/formapgs';

// initial state
const state = {
    formapgs: [],
    list: [],
    row: {}
}

// getters
const getters = {
    GetFormapgs: state => state.formapgs,
    ListFormapg: state => state.list,
    GetFormapg: state => state.row,
}

// mutations
const mutations = {
    FORMAPGS_ADD(state, data) {
        state.formapgs = data
    },
    FORMAPGS_UPDATE(state, data) {
        data.filter((item) => {
            if (state.formapgs.data.findIndex(x => x.id === item.id) == -1) {
                state.formapgs.data.push(item)
            }
        })
    },
    FORMAPG_LIST_UPDATE(state, data) {
        state.list = data
    },
    FORMAPG_ROW_UPDATE(state, data) {
        state.row = data
    }
}

// actions
const actions = {
    async getFormapgs({ commit }, pagination) {
        const response = (await Vue.http.get(urlAPI, { params: pagination }))
        if (pagination.page == 1) {
            commit('FORMAPGS_ADD', response.body)
        } else {
            commit('FORMAPGS_UPDATE', response.body.data)
        }
    },

    async getFormapg({ commit }, id) {
        const result = (await Vue.http.get(`${urlAPI}/${id}`))
        commit('FORMAPG_ROW_UPDATE', result.data)
    },

    async saveFormapg({ commit }, params) {
        if (params.id) {
            return await Vue.http.put(`${urlAPI}/${params.id}`, params)
        } else {
            return await Vue.http.post(urlAPI, params)
        }
    },

    async deleteFormapg({ commit }, id) {
        return (await Vue.http.delete(`${urlAPI}/${id}`))
    },

    async cleanFormapg({ commit }) {
        commit('FORMAPG_ROW_UPDATE', {})
    },

    async listFormapg({ commit }) {
        const result = (await Vue.http.get(`${urlAPI}/listing`)).data
        //result.push({id: null, nome: ' - Novo formapg - '});
        commit('FORMAPG_LIST_UPDATE', result)
    }
}

export default {
    state,
    getters,
    actions,
    mutations
}
