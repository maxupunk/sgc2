import Vue from 'vue';

const urlAPI = 'api/admin/parcelas';

// initial state
const state = {
    parcelas: [],
    list: [],
    row: {}
}

// getters
const getters = {
    GetParcelas: state => state.parcelas,
    ListParcela: state => state.list,
    GetParcela: state => state.row,
}

// mutations
const mutations = {
    updateParcelas(state, data) {
        state.parcelas = data
    },
    updateParcelaList(state, data) {
        state.list = data
    },
    updateParcelaRow(state, data) {
        state.row = data
    }
}

// actions
const actions = {
    async getParcelas({ commit }, id) {
        const result = (await Vue.http.get(`${urlAPI}?id=${id}`))
        commit('updateParcelas', result.data)
    },

    async getParcela({ commit }, id) {
        const result = (await Vue.http.get(`${urlAPI}/${id}`))
        commit('updateParcelaRow', result.data)
    },

    async saveParcela({ commit }, params) {
        if (params.id) {
            return await Vue.http.put(`${urlAPI}/${params.id}`, params)
        } else {
            return await Vue.http.post(urlAPI, params)
        }
    },

    async deleteParcela({ }, id) {
        return (await Vue.http.delete(`${urlAPI}/${id}`))
    },

    async cleanParcela({ commit }) {
        commit('updateParcelaRow', {})
    },

    async listParcela({ commit }, id) {
        if (id != null) {
            const result = (await Vue.http.get(`${urlAPI}/get/${id}`))
            commit('updateParcelaList', result.data)
        }
    }
}

export default {
    state,
    getters,
    actions,
    mutations
}
