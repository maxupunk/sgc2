import Vue from 'vue';

const urlAPI = 'api/admin/produtofiscal';

// initial state
const state = {
    ficais: [],
}

// getters
const getters = {
    cestFiscalList: state => state.ficais.cest,
    cfopFiscalList: state => state.ficais.cfop,
    icmsFiscalList: state => state.ficais.icms,
    ipiFiscalList: state => state.ficais.ipi,
    ncmFiscalList: state => state.ficais.ncm,
    origemFiscalList: state => state.ficais.origem,
    piscofinsFiscalList: state => state.ficais.piscofins,
    tipoFiscalList: state => state.ficais.tipo,
}

// mutations
const mutations = {
    SET_FISCAL_UPDATE(state, data) {
        state.ficais = data
    }
}

// actions
const actions = {

    async allFiscalList({ state, commit }) {
        if (!state.ficais.length) {
            const result = (await Vue.http.get(urlAPI)).body
            commit('SET_FISCAL_UPDATE', result)
        }
    },
}

export default {
    state,
    getters,
    actions,
    mutations
}
