import Vue from 'vue';

const urlAPI = 'api/admin/configuracoes';

// initial state
const state = {
    global: {
        money: {
            decimal: ',',
            thousands: '.',
            //prefix: 'R$ ',
            //suffix: ' #',
            precision: 2,
            masked: false,
            allowBlank: false,
            min: Number.MIN_SAFE_INTEGER,
            max: Number.MAX_SAFE_INTEGER
        },
    },
    imageBackgroud: null,
}

// getters
const getters = {
    GetMoneyConfig: state => state.global.money,
    GetCrtMinConfig: state => state.global.CaractereMin,
    GetConfigGlobal: state => state.global.money,
    GetConfigImageBackgroud: state => state.imageBackgroud,
}

// mutations
const mutations = {
    updateConfigGlobal(state, data) {
        state.global = data
    },
    updateConfigImageBackgroud(state, img) {
        state.imageBackgroud = img
    }
}

// actions
const actions = {

    async saveConfig({ commit }, params) {
        return await Vue.http.post(urlAPI, params)
    },

    async GetConfigImageBackgroud({ commit }) {
        fetch('https://picsum.photos/1920/1080?random')
            .then(response => response.blob())
            .then(blob => new Promise(() => {
                const reader = new FileReader()
                reader.onloadend = () => {
                    commit('updateConfigImageBackgroud', reader.result)
                }
                reader.readAsDataURL(blob)
            }))
    },
}

export default {
    state,
    getters,
    actions,
    mutations
}
