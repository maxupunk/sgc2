import Vue from 'vue';

const urlAPI = 'api/admin/marcas';

// initial state
const state = {
    marcas: [],
    list: [],
    row: {}
}

// getters
const getters = {
    GetMarcas: state => state.marcas,
    ListMarca: state => state.list,
    GetMarca: state => state.row,
}

// mutations
const mutations = {
    MARCAS_ADD(state, data) {
        state.marcas = data
    },
    MARCAS_UPDATE(state, data) {
        data.filter((item) => {
            if (state.marcas.data.findIndex(x => x.id === item.id) == -1) {
                state.marcas.data.push(item)
            }
        })
    },
    MARCA_LIST_UPDATE(state, data) {
        state.list = data
    },
    MARCA_ROW_UPDATE(state, data) {
        state.row = data
    }
}

// actions
const actions = {
    async getMarcas({ commit }, pagination) {
        const response = (await Vue.http.get(urlAPI, { params: pagination }))
        if (pagination.page == 1) {
            commit('MARCAS_ADD', response.body)
        } else {
            commit('MARCAS_UPDATE', response.body.data)
        }
    },

    async getMarca({ commit }, id) {
        const result = (await Vue.http.get(`${urlAPI}/${id}`))
        commit('MARCA_ROW_UPDATE', result.data)
    },

    async saveMarca({ commit }, params) {
        if (params.id) {
            return await Vue.http.put(`${urlAPI}/${params.id}`, params)
        } else {
            return await Vue.http.post(urlAPI, params)
        }
    },

    async deleteMarca({ }, id) {
        return (await Vue.http.delete(`${urlAPI}/${id}`))
    },

    async cleanMarca({ commit }) {
        commit('MARCA_ROW_UPDATE', {})
    },

    async listMarca({ commit }) {
        const result = (await Vue.http.get(`${urlAPI}/listing`)).data
        commit('MARCA_LIST_UPDATE', result)
    }
}

export default {
    state,
    getters,
    actions,
    mutations
}
