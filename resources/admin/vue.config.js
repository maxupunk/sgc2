module.exports = {
    // proxy API requests to Valet during development
    devServer: {
        proxy: 'http://localhost'
    },

    // output built static files to Laravel's public dir.
    // note the "build" script in package.json needs to be modified as well.
    outputDir: '../../public',

    assetsDir: './assets',

    publicPath: '/',

    // modify the location of the generated HTML file.
    // make sure to do this only in production.
    indexPath: process.env.NODE_ENV === 'production'
        ? '../resources/views/admin.blade.php'
        : 'index.html',

    transpileDependencies: [
        'vuetify'
    ],

    productionSourceMap: false,
    chainWebpack: config => {
        config.optimization.delete('splitChunks')
    },

    pwa: {
        themeColor: '#ffffff',
        msTileColor: '#ffffff',
        appleMobileWebAppCapable: 'yes',
        appleMobileWebAppStatusBarStyle: '#ffffff',
        manifestOptions: {
            name: 'Sistema de Gerenciamento Comercial',
            short_name: 'S.G.C',
            start_url: './',
            display: "standalone",
            background_color: "#ffffff",
            icons: [
                {
                    "src": "icons/android-chrome-192x192.png",
                    "sizes": "192x192",
                    "type": "image/png"
                },
                {
                    "src": "icons/android-chrome-512x512.png",
                    "sizes": "512x512",
                    "type": "image/png"
                }
            ],
        },
        display_override: [
            "minimal-ui",
            "fullscreen"
        ],
        iconPaths: {
            favicon32: 'icons/favicon-32x32.png',
            favicon16: 'icons/favicon-16x16.png',
            appleTouchIcon: 'icons/apple-touch-icon.png',
            maskIcon: 'icons/safari-pinned-tab.svg',
            msTileImage: 'icons/mstile-150x150.png'
        },
        // configure the workbox plugin
        //workboxPluginMode: 'InjectManifest',
        workboxOptions: {
            //swSrc: 'public/service-worker.js',
            include: [
                /^.*js\/.*$/,
                /^.*css\/.*$/,
            ],
            exclude: [
                /^.*html\/.*$/,
            ]
        }
    },
}
