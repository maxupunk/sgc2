import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import VCurrencyField from './plugins/VuetifyCurrencyField';
import VueResource from 'vue-resource'
import VueMoment from 'vue-moment';

Vue.use(VueMoment);
Vue.use(VueResource);

Vue.http.options.root = (process.env.NODE_ENV === 'production') ? process.env.VUE_APP_URL_API : 'http://127.0.0.1:8000'
Vue.config.productionTip = false
Vue.http.options.credentials = true;

Vue.filter('currency', require('./filters/currency'));
Vue.filter('bytes', require('./filters/bytes'));
Vue.filter('statusOrdem', require('./filters/statusOrdem'));
Vue.filter('tipoOrdem', require('./filters/tipoOrdem'));
Vue.filter('statusFinanceiro', require('./filters/statusFianceiro'));
Vue.filter('nature', require('./filters/nature'));

Vue.http.interceptors.push((request, next) => {
    if (store.getters.GetToken) {
        Vue.http.headers.common['Authorization'] = `Bearer ${store.getters.GetToken}`
    }
    store.dispatch('SetLoadingStatus', true)
    next(response => {
        store.dispatch('SetLoadingStatus', false)
        switch (response.status) {
            case 0:
                store.dispatch('AddSnackbar', {
                    message: "Servidor fora de alcance", type: "error",
                })
                break;
            case 401:
                store.dispatch('AddSnackbar', response.body)
                if (response.body.message == "Unauthenticated.") {
                    store.dispatch('destroyToken')
                }
                break;
            case 419:
                store.dispatch('AddSnackbar', {
                    message: "Acabou o tempo de autenticação!",
                });
                break;
            case 500:
                let mensagem = response.body.message ? response.body.message : "O servidor encontrou uma situação com a qual não sabe lidar."
                let typo = response.body.type ? response.body.type : null
                store.dispatch('AddSnackbar', {
                    type: typo,
                    message: mensagem,
                });
                break;
            default:
                if (response.body.message) {
                    store.dispatch('AddSnackbar', response.body)
                }
                break;
        }
    })
});

new Vue({
    router,
    store,
    vuetify,
    VCurrencyField,
    render: function (h) { return h(App) }
}).$mount('#app')
