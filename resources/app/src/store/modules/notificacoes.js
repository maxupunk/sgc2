import Vue from 'vue';

const urlAPI = 'api/cliente/notificacoes';

// initial state
const state = {
    notificacoes: {},
}

// getters
const getters = {
    GetNotificacoes: state => state.notificacoes,
    GetNotificacaoNaoLida: (state) => {
        if (state.notificacoes.data) {
            return state.notificacoes.data.filter(notificacao => (notificacao.read_at == null));
        } else {
            return []
        }
    },
}

// mutations
const mutations = {
    NOTIFICACOES_SET(state, dados) {
        state.notificacoes = dados
    },
    NOTIFICACOES_DELETE(state, id) {
        let Index = state.notificacoes.data.findIndex(item => item.id === id)
        state.notificacoes.data.splice(Index, 1)
    },
    NOTIFICACOES_MARCA_LIDA(state, item) {
        Vue.set(item, 'read_at', true)
    },
    NOTIFICACOES_SET_QNT_NAO_LIDAS(state, qnt) {
        state.qntNaoLidas = qnt
    },
}

// actions
const actions = {
    getNotificacoes({ commit }) {
        /* Vue.http.get(urlAPI).then((result) => {
            commit('NOTIFICACOES_SET', result.data)
        }) */
    },

    async deleteNotificacao({ commit }, id) {
        Vue.http.delete(`${urlAPI}/${id}`).then(() => {
            commit('NOTIFICACOES_DELETE', id)
        })
    },

    async deleteTodasNotificacao({ dispatch }) {
        Vue.http.delete(urlAPI).then(() => {
            dispatch('getNotificacoes')
        })
    },

    marcaLidaNotificacao({ commit }, item) {
        return Vue.http.put(`${urlAPI}/${item.id}`).then(() => {
            commit("NOTIFICACOES_MARCA_LIDA", item);
        })
    },

    marcaTodasNotificacoesComoLidas({ dispatch }) {
        return Vue.http.put(urlAPI).then(() => {
            dispatch('getNotificacoes')
        })
    },

}

export default {
    state,
    getters,
    actions,
    mutations
}
