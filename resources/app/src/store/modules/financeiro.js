import Vue from 'vue';

const urlAPI = 'api/cliente/financeiros';

// initial state
const state = {
    financeiros: [],
    row: {},
    nature: [
        { value: 'R', text: 'Receita' },
        { value: 'D', text: 'Despesa' }
    ],
    estatus: [
        { value: 'AB', text: 'Aberto' },
        { value: 'PG', text: 'Pago' },
        { value: 'PP', text: 'Parcialmente pago' },
        { value: 'CN', text: 'Cancelado' },
        { value: 'DV', text: 'Devolução' }
    ]
}

// getters
const getters = {
    GetFinanceiros: state => state.financeiros,
    GetFinanceiro: state => state.row,
    GetFinanceiroNature: state => state.nature,
    GetFinanceiroEstatus: state => state.estatus,
    GetFinanceiroHistorico: state => state.historico,
}

// mutations
const mutations = {
    FINANCEIROS_ADD(state, data) {
        state.financeiros = data
    },
    FINANCEIROS_UPDATE(state, data) {
        data.filter((item) => {
            if (state.financeiros.data.findIndex(x => x.id === item.id) == -1) {
                state.financeiros.data.push(item)
            }
        })
    },
    FINANCEIRO_ROW_UPDATE(state, data) {
        state.row = data
    },

}

// actions
const actions = {
    async getFinanceiros({ commit }, pagination) {
        const response = (await Vue.http.get(urlAPI, { params: pagination }))
        if (pagination.page == 1) {
            commit('FINANCEIROS_ADD', response.body)
        } else {
            commit('FINANCEIROS_UPDATE', response.body.data)
        }
    },

    async getFinanceiro({ commit }, id) {
        const result = (await Vue.http.get(`${urlAPI}/${id}`))
        commit('FINANCEIRO_ROW_UPDATE', result.data)
    },
}

export default {
    state,
    getters,
    actions,
    mutations
}
