import Vue from 'vue';

const urlAPI = 'api/cliente/ordens';

// initial state
const state = {
    row: {},
    ordens: [],
    tipos: [{
        value: 'v',
        text: 'Venda'
    },
    {
        value: 'c',
        text: 'Compra'
    },
    {
        value: 's',
        text: 'Servico'
    },
    {
        value: 'd',
        text: 'Devolução'
    },
    {
        value: 't',
        text: 'Transferencia/troca'
    }
    ],
    status: [{
        value: 'EA',
        text: 'Em Aberto',
        color: 'orange lighten-1'
    },
    {
        value: 'PD',
        text: 'Pendente',
        color: 'orange lighten-3'
    },
    {
        value: 'OR',
        text: 'Orçamento',
        color: 'purple lighten-2'
    },
    {
        value: 'AA',
        text: 'Aguardando Aprovação'
    },
    {
        value: 'PA',
        text: 'Pagamento em analise',
        disabled: true,
    },
    {
        value: 'AP',
        text: 'Aguardando pagamento',
        disabled: true,
    },
    {
        value: 'EE',
        text: 'Enviado'
    },
    {
        value: 'RP',
        text: 'Entregue parcialmente'
    },
    {
        value: 'RE',
        text: 'Entregue',
        disabled: true,
        color: 'green lighten-1'
    },
    {
        value: 'CC',
        text: 'Concluido',
        color: 'blue lighten-1'
    },
    {
        value: 'CA',
        text: 'Cancelado',
        color: 'blue-grey lighten-1'
    }
    ],
}

// getters
const getters = {
    GetOrdens: state => state.ordens,
    GetOrdem: state => state.row,
    GetOrdemTotal: state => {
        if (state.row.itens) {
            return state.row.itens.reduce((total, produto) => {
                return total + produto.quant * produto.valor
            }, 0)
        }
    },
    GetOrdemStatus: state => state.status,
    GetOrdemStatusRow: (state) => (value) => {
        var result = state.status.filter((obj) => {
            return obj.value === value;
        });
        return result[0]
    },
    GetOrdemTipos: state => state.tipos,
}

// mutations
const mutations = {
    ORDENS_ADD(state, data) {
        state.ordens = data
    },
    ORDENS_UPDATE(state, data) {
        data.filter((item) => {
            if (state.ordens.data.findIndex(x => x.id === item.id) == -1) {
                state.ordens.data.push(item)
            }
        })
    },
    ORDEM_UPDATE_ROW(state, data) {
        state.row = data
    },
}

// actions
const actions = {
    async getOrdens({ commit }, pagination) {
        const response = (await Vue.http.get(urlAPI, { params: pagination }))
        if (pagination.page == 1) {
            commit('ORDENS_ADD', response.body)
        } else {
            commit('ORDENS_UPDATE', response.body.data)
        }
    },

    async GetOrdem({ commit }, id) {
        const result = (await Vue.http.get(`${urlAPI}/${id}`))
        commit('ORDEM_UPDATE_ROW', result.data)
    },
}

export default {
    state,
    getters,
    actions,
    mutations
}
