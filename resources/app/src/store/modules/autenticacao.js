import Vue from 'vue';

const urlAPI = 'api/cliente'

// initial state
const state = {
    dataUser: {},
    token: ''
}

// getters
const getters = {
    GetdataUser: state => state.dataUser,
    GetToken: state => state.token,
    GetUsuarioConfig: (state) => {
        if (state.dataUser.config != undefined) {
            return state.dataUser.config
        } else {
            return state.configDefault;
        }
    },
}

// mutations
const mutations = {
    SET_DATA_USER(state, user) {
        state.dataUser = user
    },

    SET_USER_CONFIG_THEMES(state, config) {
        state.dataUser.config = config
    },

    SET_MENU_PERMISAO(state, menus) {
        state.manusPermitidos = menus
    },

    SET_TOKEN(state, token) {
        state.token = token
    },
}

// actions
const actions = {
    destroyToken({ commit }) {
        commit('SET_TOKEN', '')
        window.localStorage.clear()
    },

    async GoLogin({ commit }, params) {
        params['device_name'] = navigator.userAgentData.platform;
        Vue.http.post(`${urlAPI}/login`, params).then((data) => {
            if (data.body.token) {
                commit('SET_TOKEN', data.body.token)
                commit('SET_DATA_USER', data.body.usuario)
            }
        })
    },

    async GoLoginChave({ getters, commit }, chave) {
        if (!getters.GetToken) {
            //Vue.http.get('sanctum/csrf-cookie').then(() => {
            let params = {}
            params['chave'] = chave
            params['device_name'] = navigator.userAgentData.platform;
            Vue.http.post(`${urlAPI}/login`, params).then((data) => {
                if (data.body.token) {
                    commit('SET_TOKEN', data.body.token)
                    commit('SET_DATA_USER', data.body.usuario)
                }
            })
            //});
        }
    },

    async GoLogout({ getters, dispatch }) {
        if (getters.GetToken) {
            Vue.http.post(`${urlAPI}/logout`, getters.GetdataUser)
            dispatch('destroyToken')
        }
    },
}

export default {
    state,
    getters,
    actions,
    mutations
}
