import VuexPersistence from 'vuex-persist'
import autenticacao from './modules/autenticacao'
import ordem from './modules/ordem'
import FinanceiroIndex from './modules/financeiro'
import notificacoes from './modules/notificacoes'
import geral from './modules/geral'

const vuexLocal = new VuexPersistence({
    key: 'sgc-cliente',
    storage: window.localStorage,
})

var VuexStore = {
    modules: {
        ordem,
        FinanceiroIndex,
        autenticacao,
        notificacoes,
        geral
    },
    plugins: [vuexLocal.plugin]
}

import Vue from 'vue';
import Vuex from 'vuex'
Vue.use(Vuex)
const store = new Vuex.Store(VuexStore)

export default store
