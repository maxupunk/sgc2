module.exports = (function (value) {
    let tipos = {
        'v': 'Venda',
        'c': 'Compra',
        's': 'Servico',
        'd': 'Devolução',
        't': 'Transferencia/Troca'
    }
    return tipos[value]
})