module.exports = (function (value) {
    let estatus = {
        'EA': 'Aberto',
        'PD': 'Pendente',
        'OR': 'Orçamento',
        'AA': 'Aguardando Aprovação',
        'PA': 'Pagamento em analise',
        'AP': 'Aguardando pagamento',
        'EE': 'Enviado',
        'RP': 'Entregue parcialmente',
        'RE': 'Entregue',
        'CC': 'Concluido',
        'CA': 'Cancelado'
    }
    return estatus[value]
})
