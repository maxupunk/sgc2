module.exports = (function (value) {
    estatus =
        {
            'AB': 'Aberta',
            'PG': 'Pago',
            'PP': 'Parcialmente pago',
            'CN': 'Cancelado',
            'DV': 'Devolução',
            'DL': 'Deletado'
        }
    return estatus[value]
})
