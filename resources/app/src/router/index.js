import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import OrdemIndex from '../views/ordem/OrdemIndex.vue'
import OrdemForm from '../views/ordem/OrdemForm.vue'
import FinanceiroIndex from '../views/financeiro/FinanceiroIndex.vue'
import FinanceiroForm from '../views/financeiro/FinanceiroForm.vue'

Vue.use(VueRouter)

const routes = [
    { path: '/', component: Home },
    { path: '/ordens', component: OrdemIndex },
    { path: '/ordens/:id', component: OrdemForm },
    { path: '/financeiros', component: FinanceiroIndex },
    { path: '/financeiros/:id', component: FinanceiroForm },
]

const router = new VueRouter({
    mode: 'history',
    base: '/',
    routes
})

export default router
