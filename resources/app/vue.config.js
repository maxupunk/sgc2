module.exports = {
    // proxy API requests to Valet during development
    devServer: {
        proxy: 'http://127.0.0.1:8000/app'
    },

    // output built static files to Laravel's public dir.
    // note the "build" script in package.json needs to be modified as well.
    outputDir: '../../public/app',

    assetsDir: './assets',

    publicPath: '/',

    // modify the location of the generated HTML file.
    // make sure to do this only in production.
    indexPath: 'index.html',

    transpileDependencies: [
        'vuetify'
    ],

    productionSourceMap: false,
    chainWebpack: config => {
        config.optimization.delete('splitChunks')
    },

    pwa: {
        themeColor: '#ffffff',
        msTileColor: '#ffffff',
        appleMobileWebAppCapable: 'yes',
        appleMobileWebAppStatusBarStyle: '#ffffff',
        manifestOptions: {
            name: 'Masternet Informatica',
            short_name: 'Masternet',
            id: "app",
            start_url: '/app/index.html',
            scope: "/app/",
            display: "standalone",
            background_color: "#ffffff",
            icons: [
                {
                    "src": "icons/android-chrome-192x192.png",
                    "sizes": "192x192",
                    "type": "image/png"
                },
                {
                    "src": "icons/android-chrome-512x512.png",
                    "sizes": "512x512",
                    "type": "image/png"
                }
            ],
        },
        display_override: [
            "minimal-ui",
            "fullscreen"
        ],
        iconPaths: {
            favicon32: 'icons/favicon-32x32.png',
            favicon16: 'icons/favicon-16x16.png',
            appleTouchIcon: 'icons/apple-touch-icon.png',
            maskIcon: 'icons/safari-pinned-tab.svg',
            msTileImage: 'icons/mstile-150x150.png'
        },
    },
}
